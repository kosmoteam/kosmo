<?php
//header("Access-Control-Allow-Origin: https://api.oplatapridostavke.ru");
header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
/*************************************
 * Страница роутер
 ************************************/

// Установка глобальных переменных и куки
require "config/config.php";
// подключение основных функций
require "pages/core/functions.php";
require "pages/core/variables.php";

// устанавливаем куки сеанса
if ( ! isset($_COOKIE['kosmosessid']) ){
	$sessID = generateId();
	setcookie("kosmosessid", $sessID, time() + 86400, '/');
} else {
	$sessID = $_COOKIE['kosmosessid'];
}

// Если запрошен любой URI, отличный от корня сайта.
if ($_SERVER['REQUEST_URI'] != '/') {
	try {
		$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

		// Разбиваем виртуальный URL по символу "/"
		$segments = explode('/', trim($url_path, ' /'));

		if (count($segments) == 1 AND ($segments[0] == 'cart' OR $segments[0] == 'checkorder' OR $segments[0] == 'order' OR $segments[0] == 'catalog' )) {
			$data['page'] = $segments[0];

			// помещено здесь чтобы успеть отработать до отрисовки меню
			if ($segments[0] == 'cart') {
				// обновляем данные, если была нажата кнопка обновления формы
				$fields = makeSafeArray($_POST);

				if ( count($fields) > 0) {
					$cartId = $fields['cartId'];
					$removeItems = $fields['removeItems'];

					// сначала удаляем из корзины элементы
					if ($removeItems != '') {
						$removeArray = explode(',', $removeItems);
						if (count($removeArray) > 0) {
							$where = '';
							$i=0;
							foreach($removeArray as $item) {
								$sem = ($i == 0) ? '' : ' OR ' ;
								$where .= ($item != '') ? $sem."id = '{$item}'" : '' ;
								$i++;
							}
						}
						$sql = "DELETE FROM cart WHERE {$where}";
						mysql_query($sql);
					}
					// меняем количество товаров в корзине
					if ( isset($fields['quan'])){
						$quans = $fields['quan'];
						if (count($quans) > 0) {
							foreach ($quans as $key => $val) {
								changeItemCountInCart($key, $val);
							}
						}
					}
				}
				// получаем списки товаров в корзине
				$items = getCartItems($sessID);
				$total = 0;
			}
		} elseif (count($segments) > 0  AND $segments[0] == 'admin') {
			/** СТРАНИЦЫ АДМИНКИ  */
			if (!isset($_SERVER['PHP_AUTH_USER'])){
				// авторизация
				header("WWW-Authenticate: Basic realm=\"Administartion\"");
				header("HTTP/1.0 401 Unauthorized");
			} elseif ($_SERVER['PHP_AUTH_USER']=='admin' && $_SERVER['PHP_AUTH_PW'] == 'fuckbabylon'){
				$data['pageType'] = 'admin';
				$data['page'] = $segments[0];
				$area = (!empty($segments[1])) ? $segments[1] : 'meta';
				$areaItem = (!empty($segments[2])) ? $segments[2] : '';

				// определяем старницы редактирования товаров
				if($area == 'goods'){
					if (!empty($segments[2]) AND ($segments[2] == 'editliq' OR $segments[2] == 'editdev' OR $segments[2] == 'editcat')){
						$area = $segments[2];
						$goodId = (!empty($segments[3])) ? $segments[3] : 'new';
					}
				}
				// определяем старницы редактирования товаров
				if($area == 'removeitem'){
					if (!empty($segments[2])){
						$area = 'removeitem';
						$type = $segments[2];
						$goodId = (!empty($segments[3])) ? $segments[3] : 'new';
					}
				}
			} else {
				header("WWW-Authenticate: Basic realm=\"Administartion\"");
				header("HTTP/1.0 401 Unauthorized");
				exit('Введен неверный логин или пароль');
			}
		} elseif (count($segments) >= 1 AND  $segments[0] == 'confirmorder') {
			$data['pageType'] = 'full';
			$data['page'] = $segments[0];
			$data['orid'] = $segments[1];
		} elseif (count($segments) == 1 AND ($segments[0] == 'add_cart' OR $segments[0] == 'add_subemail' OR $segments[0] == 'add_rate' OR $segments[0] == 'add_order' OR $segments[0] == 'send' OR $segments[0] == 'confirmorder')) {
			$data['pageType'] = 'clean';
			$data['page'] = $segments[0];
		} elseif (count($segments) > 1) {
			// обрабатываем фильтарцию каталога
			if ($segments[0] == 'catalog') {
				if (count($segments) == 2 AND in_array($segments[1], $alias)) {
					// фильтруем по типам продуктов
					$data['area'] = $segments[1];
					$data['filtredBy'] = 'area';
				}
				if (count($segments) == 3 AND in_array($segments[1], $alias)) {
					// фильтруем по категориям
					$data['area'] = $segments[1];
					$data['filtredBy'] = 'category';
					$data['category'] = $segments[2];
				}
				if (count($segments) == 4 AND in_array($segments[1], $alias)) {
					// страница карточки товара
					// страница карточки товара
					$data['area'] = $segments[1];

					if ($data['area'] == $alias['device']){
						$data['page'] = 'card_device';
						$data['itemUrlName'] = $segments[3];
						$data['currentItem'] = getDeviceByName($data['itemUrlName']);
						$data['category'] = $segments[2];
					}
					if ($data['area'] == $alias['liquid']){
						$data['page'] = 'card_liquid';
						$data['itemUrlName'] = $segments[3];
						$data['currentItem'] = getLiquidByName($data['itemUrlName']);
						$data['category'] = $segments[2];
					}
				}
			} elseif ($segments[0] == 'ajax') {
				$data['page'] = $segments[1];
			}
		} else {
			header("HTTP/1.0 404 Not Found");
			$data['page'] = '404';
		}
	} catch (Exception $e) {
		header("HTTP/1.0 404 Not Found");
		$data['page'] = '404';
	}
}

// количество товаров в корзине
$cartItemsCount = getCartCount($sessID);

// формируем массив сео-текстовок
$data['aliasBack'] = $aliasBack;
$data['alias'] = $alias;
$seoElements = getSeoElements($data);
/********************************************
 * отрисовываем страницу
 ********************************************/
//$data['pageType'] = ($data['page'] == 'home') ? 'home' : $data['pageType'];
//$data['pageType'] = 'full';

$data['url'] =  "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$data['pageident'] =  $_SERVER['REQUEST_URI'];

if ($data['pageType'] == 'full') {
	include "pages/tpl/head.php";
	// меню
	$data['activeMenuItem'] = $data['page'];
	include "pages/tpl/menu.php";
} elseif ($data['pageType'] == 'home') {
	include "pages/tpl/home_head.php";
} elseif ($data['pageType'] == 'admin') {
	include "pages/tpl/admin_head.php";
}
//pp($seoElements);
//
//if ($data['page'] == 'catalog' AND ! empty($data['area']) AND empty($data['category'])){
//	// заголовки area (all, device, liquid, nonicoliquid, producer)
//	echo 'main >> ' . $data['area'];
//}
//if ($data['page'] == 'catalog' AND ! empty($data['area']) AND ! empty($data['category'])){
//	// заголовки категории из БД
//	echo 'main >> '.$data['area'] . ' >> '.$data['category'];
//}
//if (($data['page'] == 'card_device' OR $data['page'] == 'card_liquid' )AND ! empty($data['area']) AND ! empty($data['itemUrlName'])){
//	// заголовки товара из БД
//	echo 'main >> '.$data['area'] . ' >> '.$data['category']. ' >> '.$data['itemUrlName'];
//}

include "pages/{$data['page']}.php";

if ($data['pageType'] == 'full') {
	include "pages/tpl/foot.php";
} elseif ($data['pageType'] == 'home') {
	include "pages/tpl/home_foot.php";
} elseif ($data['pageType'] == 'admin') {
	include "pages/tpl/admin_foot.php";
}