$(document).ready(function(){

	//$('#myModal').find('#myModalLabel').html('ВНИМАНИЕ!!');
	//$('#myModal').find('.modal-body').html('В данный момент магазин работает в тестовом режиме. Приносим свои извиниения');
	//$('#myModal').modal('show');

	// обработчик кнопки в биалоге подписки
	$('#subscribeBtn').click(function () {
		$('#subModal .email-block').removeClass('has-error');
		var email = $('#subEmail').val();
		if( ! validEmail(email)){
			$('#subModal .email-block').addClass('has-error');
		} else {
			// отправка мыла по ajax
			$.ajax({
				type: "GET",
				url: "/add_subemail",
				dataType: "json",
				data: {email:email},
				success: function(res) {
					// перед закрытием окна выставляем куку на необходимое время
					if(res.exp){
						var ExpDate = new Date ();
						ExpDate.setTime(ExpDate.getTime() + (res.exp  * 1000));
						SetCookie('hide_subs','1',ExpDate, "/");
					}
					$('#subModal').modal('hide');
				}
			});
		}
	});

	// обработчик закрытия диалога подписки
	$('#subModal .close').click(function () {
		// ставим куку на день
		var ExpDate = new Date ();
		ExpDate.setTime(ExpDate.getTime() + (24 * 60 * 60 * 1000));
		SetCookie('hide_subs','1',ExpDate, "/");
	});

	/**
	 * Добавление товара в корзину из карточки
	 */
	$('.add-item-to-cart').click(function () {
		var title ='';
		var message ='';
		$.ajax({
			type: "GET",
			url: "/add_cart",
			dataType: "json",
			data: $('#item-form').serialize(),
			success: function(res) {

				if (res.res) {
					var cartCount = parseInt($('.navbar-collapse #cartCount').html());
					var cartCountH = parseInt($('.navbar-header #cartCount').html());
					cartCount++;
					cartCountH++;
					title = 'Успешно!';
					message = res.msg;
				} else {
					title = 'Ошибка!';
					message = res.msg;
				}

				$('#myModal').find('#myModalLabel').html(title);
				$('#myModal').find('.modal-body').html(message);
				$('#myModal').modal('show');

				if (res.res) {
					$('#myModal').on('hidden.bs.modal', function (e) {
						$( "#done-arrow" ).fadeIn( 500 );
						$( "#done-arrow" ).fadeOut( 3000 );
						$('.navbar-collapse #cartCount').html(cartCount);
						$('.navbar-header #cartCount').html(cartCountH);
					});
				}
			}
		});
	});

	/**
	 * Обработчик кнопок количества товара на старнице корзины
	 */
	$('.cart-quantity-group .btn').click(function () {
		// получаем значения количества и цены

		var price = $(this).attr('data-price');
		var rowid = $(this).attr('data-id');
		var quantity = $('#cart-quan-'+rowid).val();

		if ($(this).attr('data-role') == 'btn-minus'){
			quantity = (quantity <= 1) ? 1 : quantity-1;
		}
		if ($(this).attr('data-role') == 'btn-plus'){
			quantity++;
		}

		//обновляем значения цены и количества
		$('#cart-price-'+rowid).html(price*quantity);
		$('#cart-quan-'+rowid).val(quantity);
	});

	/**
	 * Обработчик кнопок удаления овара на старнице корзины
	 */
	$('.row-remove').click(function () {
		var rowid = $(this).attr('data-id');
		$('#row-'+rowid).remove();
		$('#removeItems').val($('#removeItems').val()+rowid+',');
	});

	/**
	 * Обработчик кнопок количества товара на старнице карточки
	 */
	$('#quantity-group .btn').click(function () {
		// получаем значения количества и цены
		var quantity = $('#quantity-group #quantity').val();
		var price = $('#price').val();

		if ($(this).attr('data-role') == 'btn-minus'){
			quantity = (quantity <= 1) ? 1 : quantity-1;
		}
		if ($(this).attr('data-role') == 'btn-plus'){
			quantity++;
		}

		//обновляем значения цены и количества
		$('#price-block .price-val').html(price*quantity);
		$('#quantity-group #quantity').val(quantity);
	});

	/**
	 * Обработчик кнопок объёма на старнице карточки
	 */
	$('#volume-group .btn').click(function () {
		//получаем значения количества и цены
		var quantity = $('#quantity-group #quantity').val();
		var price;
		var price1 = $('#price1').val();
		var price2 = $('#price2').val();

		// получаем значения объёма
		var volume;
		var volume1 = $('#volume-group #volume1').val();
		var volume2 = $('#volume-group #volume2').val();

		if ($(this).attr('data-role') == 'btn-minus'){
			volume = volume1+' мл';
			price = price1 * quantity;
			$('#price').val(price1);
		}
		if ($(this).attr('data-role') == 'btn-plus'){
			volume = volume2+' мл';
			price = price2 * quantity;
			$('#price').val(price2);
		}
		$('#volume-group #volume').val(volume);

		$('#price-block .price-val').html(price);
	});

	/**
	 * Обработчик кнопок выбора цвета
	 */
	$('.product-colors li').click(function () {
		//отмечаем выбранный пункт
		$('.product-colors li').removeClass('active');
		$(this).addClass('active');

		// получаем изображение и применяем его к основному блоку

		// получаем значения количества и цены
		var image = $(this).find('img').attr('src');
		$('.product-image a img').prop('src',image);
		// запоминаем выбор пользователя
		$('#color').val($(this).attr('data-color'));
		$('#color').attr('data-src', image);
	});

	/**
	 * Обработчик ховера над кнопками выбора цвета
	 */
	$( ".product-colors li" )
		.mouseover(function() {
			$('.product-image a img').prop('src',$(this).find('img').attr('src'));
		})
		.mouseout(function() {
			$('.product-image a img').prop('src',$('#color').attr('data-src'));
		});

	$('.logo-header').click(function () {
		window.location.href = "/";
	});


	// предложение проголосовать
//	$('.can-rate').popover('show');

	$(".can-rate").hover(
		function() {
			// ничего не делаем
		}, function() {
			$(this).find(".fa").removeClass('fa-star-o');
			$(this).find(".fa.empty").removeClass('fa-star').addClass('fa-star-o');
			$(this).find(".fa.fill").removeClass('fa-star-o').addClass('fa-star');
		}
	);
	$(".can-rate .fa")
		.mouseenter(function() {
			if ( $(this).parents('.product-rating-stars').hasClass('can-rate') ) {

				$(".can-rate .fa").removeClass('fa-star-o').addClass('fa-star');
				$(this).addClass('fa-star').nextAll().removeClass('fa-star').addClass('fa-star-o');
			}
		})
		.mouseleave(function() {
			// ничего не делаем
		});

	// обработчик клика по звёздочке при голосовании
	$(".can-rate .fa").click(function () {
		if ( $(this).parents('.product-rating-stars').hasClass('can-rate') ){
			//$('.can-rate').popover('destroy');
			var rateCnt = $('.rating-count').data('count');
			var itemType = $(this).data('type');
			var itemId = $(this).data('id');
			var itemRate = $(this).data('weight');
			//  фиксируем выбор юзер
			$( ".product-rating-stars .fa").each(function() {
				// раскрашиваем звёзды до выбранного рейтинга
				if ( $(this).data('weight') <= itemRate ){
					$(this).removeClass('fa-star-o').removeClass('empty').addClass('fa-star').addClass('fill');
				} else {
					$(this).addClass('fa-star-o').addClass('empty').removeClass('fa-star').removeClass('fill');
				}

				$('.rating-count').html(rateCnt+1);
			});
			$.ajax({
				type: "GET",
				url: "/add_rate",
				dataType: "json",
				data: { type: itemType, id: itemId, rate: itemRate },
				success: function(res) {
					console.log(res);
					// отключаем возможность голосовать

					$(".product-rating-stars").removeClass('can-rate');
					$('.product-rating').addClass('rate-done');
					//	$('.rate-done').popover('show');
				}
			});
		}
	});
});

/**
 * проверка валидность введённого email
 */
function validEmail(email) {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	return reg.test(email);
}

/**
 * получение куки для Hi cpa
 * */
function getCookie(name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}

/**
 * Установка куки для Hi cpa
 * */
function SetCookie (name, value) {
	var argv = SetCookie.arguments;
	var argc = SetCookie.arguments.length;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	document.cookie = name + "=" + escape (value) +
		((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
		((path == null) ? "" : ("; path=" + path)) +
		((domain == null) ? "" : ("; domain=" + domain)) +
		((secure == true) ? "; secure" : "");
}