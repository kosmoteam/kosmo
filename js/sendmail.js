jQuery(document).ready(function(){

	$('#contactform').submit(function(){

		var action = $(this).attr('action');
		$("#message").slideUp(500,function() {
			$('#message').hide();
			$.post(action, {
				name: $('#name').val(),
				email: $('#email').val(),
				phone: $('#mobilephone').val(),
				comments: $('#comments').val(),
				cartId: $('#cartId').val(),
				cartTotal: $('#cartTotal').val(),
				cartCount: $('#cartItCount').val(),
				deliveryType: $("input[type='radio'][name='deliveryType']:checked").val()
			},
				function(data){
					document.getElementById('message').innerHTML = data;
					$('#message').slideDown('slow');
					$('#submit').removeAttr('disabled');
					if(data.match('success') != null) $('#contactform').slideUp('slow');
				}
			);
		});

		return false;

	});

});