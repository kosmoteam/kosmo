<?php
    // получаем списки товаров в корзине
    $items = getCartItems($sessID);
    $total = 0;
    $count = 0;
    foreach ($items as $item) {
        $total += $item['quantity']*$item['price'];
        $count += $item['quantity'];
    }
    $isCartEmpty  = (count($items ) == 0) ? TRUE : FALSE ;

    // инициализация checkout
    $tuCurl = curl_init();
        curl_setopt($tuCurl, CURLOPT_URL,
                "http://platform.checkout.ru/service/login/ticket/" . $checkoutKey);
        curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
        curl_setopt($tuCurl, CURLOPT_HEADER, 0);
        curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
        $tuData = curl_exec($tuCurl);

        if(!curl_errno($tuCurl)){
            $info = curl_getinfo($tuCurl);
        } else {
            //TODO  подумать что тут показать посетителю
            echo 'Curl error: ' . curl_error($tuCurl);
        }
    curl_close($tuCurl);
    $responce =  json_decode($tuData,true);
    $ticket = $responce["ticket"];
?>

<script type="text/javascript">
    var totalPrice = <?=$total?>;

    $(document).ready(function() {

        $("#cf-submit").click(function() {
            var delType= $("input[type='radio'][name='deliveryType']:checked").val();

            var mes = '';
            // очищаем сообщения об ошибках
            $(".form-group").removeClass('has-error');
            $("#message").html('').removeClass('alert').removeClass('alert-danger');

            // проверка имени и телефона
            if ( $("#fullname").val() == '' ){
                $(".fullname-group").addClass('has-error');
                mes += 'Поле <b>ФИО</b> не может быть пустым<br/>';
            }

            // проверка имени и телефона
            if ( $("#phone").val() == '' ){
                $(".phone-group").addClass('has-error');
                mes += 'Поле <b>ТЕЛЕФОН</b> не может быть пустым<br/>';
            }

            // проверка email
            if ( $("#email").val() == '' || !validEmail($("#email").val()) ){
                $(".email-group").addClass('has-error');
                mes += 'Поле <b>EMAIL</b> должно быть правильным email<br/>';
            }

            // показываем ошибку, елси поля не прошли проверку
            if (mes != ''){
                $("#message").html(mes).addClass('alert').addClass('alert-danger');
            } else {
	            <? if ($mode != 'dev') : ?>
	                // отправляем событие в ga
	                ga('send', {
	                    hitType: 'event',
	                    eventCategory: 'send_order',
	                    eventAction: 'click',
	                    eventLabel: 'send_order'
	                });
	            <? endif;  ?>

                //  сохранение ордера перед отправкой в чекаут
                $.ajax({
                    type: "post",
                    url: "/add_order",
                    dataType: "json",
                    data: $('#orderForm').serialize(),
                    success: function(res) {
                        //console.log(res);
                        // Если выбрана доставка курьером
                        //передаём управление checkout
                        if (delType == 'courier' ){
                            var callBUrl  = $('#callbackURL').data('value') + '/' + res.orid;
                            $('#callbackURL').val(callBUrl);
                            $('#orderForm').find('[type="submit"]').trigger('click');
                        }
                        // Если выбрана доставка самовывозом
                        //показываем сообщение об удачном создании
                        if (delType == 'self' ){
                            $('.hide-on-success').hide();
                            var m = '<h3>Заказ успешно сформирован и принят магазином</h3>';
                            m += '<h4>В самое ближайшее время с вами свяжется менеджер магазина Космос Вейп для обсуждения деталей заказа. Спасибо что выбрали нас!</h4>';
                            $('#message').html(m);
                        }
                    }
                });
            }
        });
    });
</script>
<!-- Start Contact 1 -->
<section class="content-block contact-1">
    <div class="container text-left">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="col-md-12 breadcrumbs">
                <h4>
                    <h4>
                        <ul class="filter">
                            <li><a href="/">Главная</a></li>
                            <li>Заказ и доставка</li>
                        </ul>
                    </h4>
                </h4>
            </div>
            <div class="editContent text-center">
                <h1>Заказ и доставка</h1>
            </div>
            <? if ($isCartEmpty) : ?>
                <div class="editContent text-center emptycart">
                    <img src="/images/emptycart.png" height="100px"/>
                </div>
                <h4 class="cart-h4 text-center">Вы пока еще не добавили товаров в корзину</h4>
                <div class="editContent text-center">
                    <a href="/catalog">Перейти к выбору товаров</a>
                </div>
            <? else :?>
                <div id="contact" class="form-container text-center">
                    <h4 class="hide-on-success">Заполните форму и отправьте нам. <br/>Наш менеджер свяжется с Вами в ближайшее время</h4>
                    <div class="row">
                        <div class="col-sm-10 col-md-offset-1" id="message"></div>
                    </div>
                    <form name="orderForm" id="orderForm" method="post" class="hide-on-success" action="//platform.checkout.ru/shop/checkout2" target="copIframe">
                    <!--<form name="orderForm" id="orderForm" method="post">-->
                        <input type='hidden' name='ticket' id='ticket' value='<?=$ticket?>'/>
                        <input type="hidden" id="cartId" name="cartId" value="<?=$sessID?>">
                        <input type='hidden' name='ver' value='1'/>
                        <input type='hidden' name='callbackURL' id='callbackURL' data-value="http://kosmosvape.ru/confirmorder" value='http://kosmosvape.ru/confirmorder' />

                        <? $i=0; ?>
                        <?php foreach ($items as $item) : ?>
                            <?php
                                if ($item['category'] == 'device') {
                                    $color = (!empty($colors[$item['color']])) ? $colors[$item['color']] : '-';
                                    $spec=" Цвет:{$color}";
                                } else {
                                    $spec=" Никотин:{$item['nicotine']}";
                                }
                            ?>

                            <input type='hidden' name='codes[<?=$i;?>]' value='<?=$item['id']?>'/>
                            <input type='hidden' name='names[<?=$i;?>]' value='<?=$item['name']?>'/>
                            <input type='hidden' name='settings[<?=$i;?>]' value='<?=$spec?>'/>
                            <input type='hidden' name='producers[<?=$i;?>]' value='<?=$item['producer']?>'/>
                            <input type='hidden' name='weights[<?=$i;?>]' value='<?=$item['weight']?>'/>
                            <input type='hidden' name='quantities[<?=$i;?>]' value='<?=$item['quantity']?>'/>
                            <input type='hidden' name='costs[<?=$i;?>]' value='<?=$item['price']?>'/>
                            <input type='hidden' name='paycosts[<?=$i;?>]' value='<?=$item['quantity']*$item['price']?>'/>

                            <? $i++; ?>
                        <?php endforeach;?>
                        <input type="hidden" id="cartItCount" name="cartItCount" value="<?=$count?>">
                        <input type="hidden" id="cartTotal" name="cartTotal" value="<?=$total?>">

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group fullname-group">
                                    <input name="fullname" id="fullname" placeholder="ФИО" class="form-control" type="text" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group phone-group">
                                    <input name="phone" id="phone" placeholder="Телефон" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group email-group">
                                    <input name="email" id="email" placeholder="Email" class="form-control" type="text" value="">
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group text-left">
                                    Другие способы связи с вами (вКонтакте, Telegram, WhatsApp и т.д.) и комментарии к заказу:
                                    <textarea name="comments" id="comments" class="form-control" rows="3" placeholder="Другие способы связи с вами (вКонтакте, Telegram, WhatsApp и т.д.) и комментарии к заказу:"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group text-left">
                                    <h6>Способ доставки</h6>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="deliveryType" id="deliveryType1" value="courier" checked>
                                            Доставка курьером <span class="small text-muted">(от 250 рублей)</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="deliveryType" id="deliveryType2" value="self">
                                            Самовывоз
                                            <span class="small text-muted">(бесплатно, по адресу: Пресненская набережная 6, стр. 2, Москва. +7 916 448 75 71)</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
	                        <!--ЕСЛИ включено в админке, показываем диалог с предложением подписки-->
	                        <? if ( ! empty($promos['subscriptions_enabled'])) : ?>
		                        <div class="col-sm-12">
			                        <div class="form-group checkbox text-left">
				                        <label>
					                        <input type="checkbox" id="getSubscription" name="getSubscription" type="checkbox" checked> подписаться на email-рассылку о новостях и акциях
				                        </label>
			                        </div>
		                        </div>
	                        <? endif;?>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <input class="hidden btn btn-info"  type='submit' value='Оригинальный submit'>
                                <button class="btn btn-primary" type="button" id="cf-submit" name="cf-submit">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.form-container -->
            <? endif; ?>
            <hr>
            <div class="editContent row">
                <div class="col-sm-5">
                    <h6>Оплата</h6>
                    <p>Оплата производится только наличными курьеру в момент получения Вашего заказа.</p>
                    <h6>Доставка</h6>
                    <p>
                        Доставка по Москве в пределах МКАД составляет от 250 рублей и осуществляется курьерской службой. Стоимость доставки оплачивается отдельно при получении. Заказы доставляются на следующий день после оформления. Доставка за МКАД обговаривается отдельно.
                        <br/>Мы не работаем с наложенным платежом.
                        <br/>Оплата заказа производится наличными при получении.
                        <br/>Мы отправляем заказы следующими транспортными компаниями:
                        <ul class="circle" >
                            <li>DPD</li>
                            <li>PickPoint</li>
                            <li>SPSR</li>
                            <li>Shop Logistics</li>
                            <li>Boxberry</li>
                            <li>Почта России</li>
                            <li>B2Cpl</li>
                        </ul>
                    <p>
                </div>
                <div class="col-sm-7 text-center">
                    <div class="text-left">
                        <h6>Самовывоз</h6>
                        <p>Самовывоз доступен в день заказа в случае, если товар в наличии. Вы можете самостоятельно забрать свой заказ по адресу Пресненская набережная 6, стр. 2, башня «Империя», Южное Лобби., с 10 утра до 19 вечера в будние рабочие дни. В выходные дни самовывоз обговаривается отдельно с менеджером по телефону или электронной почте.</p>
                    </div>
                    <!--Google Map-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.590468280113!2d37.53830451609125!3d55.748244299810935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54bdcf5c58c73%3A0x92d5b50611b605cc!2z0J_RgNC10YHQvdC10L3RgdC60LDRjyDQvdCw0LEuLCA2INGB0YLRgNC-0LXQvdC40LUgMiwg0JzQvtGB0LrQstCwLCAxMjMzMTc!5e0!3m2!1sru!2sru!4v1460043146666" width="450" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <ul class="contact-info">
                        <li><span class="fa fa-user"></span>ИП Антон Сергеевич Постюшков - Kosmos Vape,</li>
                        <li><span class="fa fa-map-marker"></span>Пресненская набережная 6, стр. 2, Москва.</li>
                        <li><span class="fa fa-phone"></span>+7 916 448 75 71 <a href="mailto:kosmosvape@gmail.com">kosmosvape@gmail.com</a> </li>
                    </ul>
                </div>
            </div>
        </div><!-- /.col-sm-10 -->

    </div><!-- /.container -->
</section><!-- /.content-block -->
<!--// END Contact 1 -->
