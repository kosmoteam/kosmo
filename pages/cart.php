<script type="text/javascript">
    $(document).ready(function() {
        // логирование и переход к форме оформления заказа
        $('.btn-primary').click(function (){
	        <? if ($mode != 'dev') : ?>
		        // отправляем событие в ga
	            ga('send', {
	                hitType: 'event',
	                eventCategory: 'go_to_order',
	                eventAction: 'click',
	                eventLabel: 'cart_form'
	            });
	        <? endif;  ?>
            // переход к оофрмлению
            setTimeout(function () {
                window.location.href = "/order";
            }, 100);
        });
    });
</script>

<section class="content-block contact-1">
    <div class="container text-center">
        <div class="col-md-12 breadcrumbs">
            <h4><ul class="filter">
                    <li><a href="/">Главная</a></li>
                    <li>Карточка заказа</li>
                </ul>
            </h4>
        </div>
        <div class="col-sm-12">
            <div class="underlined-title">
                <div class="editContent">
                    <h1>Ваша карточка заказа</h1>
                </div>
            </div>
            <div class="editContent">
            <?php if (count($items ) > 0) : ?>
                <form class="form-inline" id="cart-form" action="#" method="post">
                    <input type="hidden" id="removeItems" name="removeItems" value="">
                    <input type="hidden" id="cartId" name="cartId" value="<?=$sessID?>">
                    <!-- Start Shop 1-7 -->
                    <section id="shop-1-7" class="content-block shop-1-7">
                        <div class="container">
                            <div class="row">
                                <table class="table table-striped table-hover cart-table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th colspan="2"><span class='pull-left'>ТОВАР</span></th>
                                            <th>ЦВЕТ</th>
                                            <th>КРЕП.</th>
                                            <th>ЦЕНА</th>
                                            <th width="10px">КОЛИЧЕСТВО</th>
                                            <th>ВСЕГО</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($items as $item) : ?>
                                            <?php
                                            if ($item['category'] == 'liquid') {
                                                $image = safeFile("/images/liquids/{$item['url_name']}_small.jpg", 'small', 'liquid', $item['producer']);
                                                $iurl = "/catalog/{$alias['liquid']}/{$item['producer']}/{$item['url_name']}";
                                            } else {
                                                $color = (empty($item['color'])) ? '' : '_'.$item['color'];
                                                $image = safeFile("/images/devices/{$item['url_name']}{$color}_small.jpg", 'small', 'device', $item['producer']);
                                                $iurl = "/catalog/{$alias['device']}/{$item['producer']}/{$item['url_name']}";
                                            }
                                            ?>
                                            <tr id="row-<?=$item['id']?>">
                                                <td>
                                                    <a href="#" class="remove row-remove" title="Remove this item" data-toggle="tooltip" data-id="<?=$item['id']?>" data-placement="top"><i class="fa fa-trash"></i></a>
                                                </td>
                                                <td width="44px">
                                                    <a class="thumbnail" href="<?=$iurl?>"> <img src="<?=$image?>" style="width: 35px; height: auto;"/> </a>
                                                </td>
                                                <td>
                                                    <a class='pull-left' href="<?=$iurl?>"><?=$item['name']?></a>
                                                </td>
                                                <td>
                                                    <?php if ($item['category'] == 'device') : ?>
                                                        <?=(!empty($colors[$item['color']])) ? $colors[$item['color']] : '-';?>
                                                    <?php else:?>
                                                        -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                    <?php if ($item['category'] == 'liquid') : ?>
                                                        <?=$item['nicotine']?>
                                                    <?php else:?>
                                                        -
                                                    <?php endif;?>
                                                </td>
                                                <td><?=$item['price']?><span class="price-rub"></span></td>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputAmount">Quantity</label>
                                                        <div class="input-group cart-quantity-group">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" data-id='<?=$item['id']?>' data-price='<?=$item['price']?>' data-role="btn-minus" type="button">-</button>
                                                            </span>
                                                            <input class="form-control" id="cart-quan-<?=$item['id']?>" name="quan[<?=$item['id']?>]" placeholder="1" type="text" value="<?=$item['quantity']?>">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" data-id='<?=$item['id']?>' data-price='<?=$item['price']?>' data-role="btn-plus" type="button">+</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><strong><span id="cart-price-<?=$item['id']?>"><?=$item['quantity']*$item['price']?></span></strong><span class="price-rub"></span></td>
                                            </tr>
                                            <?php $total += $item['quantity']*$item['price'];?>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary full-pull-right">Обновить</button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                    </div>
                                    <div class="col-md-5 col-md-offset-2 col-sm-6 col-xs-12">
                                        <h3>Всего:</h3>
                                        <table class="table table-striped table-hover cart-table">
                                            <tbody>
                                            <tr class="big-total">
                                                <td>Всего (*без учёта доставки)</td>
                                                <td><?=$total?><span class="price-rub"></span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                    </div>
                                    <div class="col-md-5 col-md-offset-2 col-sm-6 col-xs-12">
                                        * Доставку и ее стоимость вы можете изменить на странице оформления заказ
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-4 col-md-8 col-md-offset-4 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <a href='/catalog' class="btn btn-default">
                                                    <span class="glyphicon glyphicon-shopping-cart"></span> Вернуться к выбору товаров
                                                </a>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="btn btn-primary">
                                                    Перейти к оформлению заказа <span class="glyphicon glyphicon-play"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--// End Shop 1-7 -->
                </form>
            <?php else:?>
                <div class="editContent text-center emptycart">
                    <img src="/images/emptycart.png" height="100px"/>
                </div>
                <h4 class="cart-h4">Вы пока еще не добавили товаров в корзину</h4>
                <div class="editContent">
                    <a href="/catalog">Перейти к выбору товаров</a>
                </div>
            <?php endif;?>
            </div>
        </div><!-- /.container -->
</section><!-- /.content-block -->
<!--// END Contact 1 -->

