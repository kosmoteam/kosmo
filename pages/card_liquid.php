<?
	$liquid = $data['currentItem'];
	$liquid['volume2'] = ( ! empty($liquid['volume2'])) ? $liquid['volume2'] : $liquid['volume1'] ;
	$liquid['price2'] = ( ! empty($liquid['price2'])) ? $liquid['price2'] : $liquid['price1'];
	$nicotines = explode(',',  $liquid['nicotine']);
	?>
<script type="text/javascript">
	$(document).ready(function() {
		/**
		 * сброс значений
		 */
		$('#quantity-group #quantity').val('1');
		$('#volume-group #volume').val('<?=$liquid['volume1']?> мл');
		$('#price').val('<?=$liquid['price1']?>');
		var nicstring = '<?=$liquid['nicotine']?>';
		var nicotines = nicstring.split(",");
		$('#nicotine').val(nicotines[0]+' мг').attr('placeholder', nicotines[0]+' мг');

        /**
         * Обработчик кнопок количества никотина на старнице карточки
         */
        $('#nicotine-group .btn').click(function () {
	        var index = $('#nicotine-group #nicotine').data('index');
	        var nicotine = nicotines[index];

            if ($(this).attr('data-role') == 'btn-minus') {
	            if (nicotines[index-1] !== undefined) {
		            nicotine = nicotines[index-1];
		            index--;
	            }
            }
            if ($(this).attr('data-role') == 'btn-plus') {
	            if (nicotines[index+1] !== undefined) {
		            nicotine = nicotines[index+1];
		            index++;
	            }
            }
	        $('#nicotine-group #nicotine').val(nicotine+' мг').data('index', index);
        });
    });
</script>
<!-- Start Shop 1-6 v1 -->
<section id="shop-1-6" class="content-block shop-1-6">
    <div class="container">
        <div class="row">
            <div class="col-md-9 pull-right" itemscope itemtype="http://schema.org/Product">
                <form class="form-inline col-md-12" id="item-form">
                    <input type="hidden" name="name" value="<?=$liquid['name']?>">
                    <input type="hidden" name="producer" value="<?=$liquid['producer']?>">
                    <input type="hidden" name="url_name" value="<?=$liquid['url_name']?>">
                    <input type="hidden" id="price1" value="<?=$liquid['price1']?>">
                    <input type="hidden" id="price2" value="<?=$liquid['price2']?>">
                    <input type="hidden" name="price" id="price" value="<?=$liquid['price1']?>">
                    <input type="hidden" name="category" id="category" value="liquid">
                    <input type="hidden" name="id" id="id" value="<?=$liquid['id']?>">
                    <input type="hidden" name="weight" value="<?=$liquid['weight']?>">
                    <input type="hidden" name="session_id" value="<?=$sessID?>">

                    <div class="row">
                        <div class="col-md-12 breadcrumbs">
                            <h4>
                                <ul class="filter">
                                    <li><a href="/">Главная</a></li>
                                    <li><a href="/catalog/<?=$alias['liquid']?>/"><?=$bAlias['liquid']?></a></li>
                                    <li><a href="/catalog/<?=$liquid['producer']?>"><?=$catList[$liquid['producer']]?></a></li>
                                    <li><?=$liquid['name']?></li>
                                </ul>
                            </h4>
                        </div>
                        <div class="col-md-6 col-xs-8 col-xs-offset-2 col-md-offset-0 text-center">
                            <div class="ribbon exclusive"></div><!-- Ribbon -->
                            <div class="product-image">
                                <? $img = safeFile("/images/liquids/{$liquid['url_name']}_big.jpg", 'big', 'liquid',$liquid['producer']);?>
                                <a href="#"><img itemprop="image" src="<?=$img?>" class="img-responsive" alt="<?=$liquid['img_alt']?>"/></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-desc">
                                <div class="editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
                                    <h1 itemprop="name">Жидкость <?=$liquid['name']?></h1>
                                </div>
                                <div class="editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
                                    <h2><?=$catList[$liquid['producer']]?></h2>
                                </div>

	                            <? printRating('liquid', $liquid['url_name'], $liquid['rating'], $liquid['ratingCnt']);?>

                                <div class="product-desc row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	                                <? if ($promos['show_old_price_liquid'] == 1):?>
		                                <div id="old-price-block" class="editContent col-md-12 old-price">
			                                <span class="price-val" itemprop="price"><?=oldLiqPrice($liquid['price1'])?></span>
				                            <span class="price-rub" itemprop="priceCurrency">RUB</span>
			                                <div class="del-line"></div>
		                                </div>
	                                <?endif;?>
									<div id="price-block" class="editContent col-md-12" data-price="<?=$liquid['price1']?>">
                                        <span class="price-val" itemprop="price"><?=$liquid['price1']?></span>
                                        <span class="price-rub" itemprop="priceCurrency">RUB</span>
                                    </div>

                                    <div class="form-group col-md-12 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center " id="quantity-group">
                                        <label class="sr-only" for="exampleInputAmount">Количество</label>
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-role="btn-minus" type="button">-</button>
                                        </span>
                                            <input class="form-control" id="quantity" name="quantity" placeholder="1" value='1' type="text">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-role="btn-plus" type="button">+</button>
                                        </span>
                                        </div>
                                    </div>

	                                <div class="form-group col-md-12 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center quantity-group">
                                        <i class=" fa fa-calculator"></i>В наличии: <span class='fat' itemprop="availability"><?=$liquid['quantity']?></span>
                                    </div>

                                    <div class="form-group col-md-12 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center">
                                        <button type="button" data-id="<?=$liquid['id']?>" data-category="liquid" class="btn btn-primary add-item-to-cart">В корзину</button>
                                    </div>
                                </div>

	                            <div class="form-group editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
		                            <h3 itemprop="description"><?=$liquid['remark']?></h3>
	                            </div>
	                            <div class="form-group editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
		                            <p><?=$liquid['description']?></p>
	                            </div>
	                            <div class="col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0 text-center">
		                            <ul class="fa-ul">
			                            <li>
				                            <div class="row" id="nicotine-row">
					                            <div class="col-md-3 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center ">
						                            <i class="fa-li fa fa-chevron-right"></i>Никотин:
					                            </div>
					                            <div class="col-md-4 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center ">
						                            <div class="form-group" id="nicotine-group">
							                            <label class="sr-only" for="exampleInputAmount">Никотин</label>
							                            <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" data-role="btn-minus" type="button">-</button>
                                                    </span>
								                            <input class="form-control" id="nicotine" name="nicotine" data-index='0' placeholder="<?=$nicotines[0]?> мг" value="<?=$liquid['nicotine'][0]?> мг" type="text">
                                                        <span class="input-group-btn">
                                                        <button class="btn btn-default" data-role="btn-plus" type="button">+</button>
                                                    </span>
							                            </div>
						                            </div>
					                            </div>
				                            </div>
			                            </li>
			                            <li>
				                            <div class="row" id="volume-row">
					                            <div class="col-md-3 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center " >
						                            <i class="fa-li fa fa-chevron-right"></i>Объём:
					                            </div>
					                            <div class="col-md-4 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center ">
						                            <div class="form-group" id="volume-group">
							                            <label class="sr-only" for="exampleInputAmount">Объём</label>
							                            <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" data-role="btn-minus" type="button">-</button>
                                                    </span>
			                                        <input class="form-control" name="volume" id="volume" placeholder="<?=$liquid['volume1']?> мл" value="<?=$liquid['volume1']?> мл" type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" data-role="btn-plus" type="button">+</button>
                                                    </span>
								                            <input type="hidden" id="volume1" value="<?=$liquid['volume1']?>">
								                            <input type="hidden" id="volume2" value="<?=$liquid['volume2']?>">
							                            </div>
						                            </div>
					                            </div>
				                            </div>
			                            </li>
		                            </ul>
	                            </div>
	                            <div class="col-md-12 col-xs-12">
	                                <hr>
	                            </div>
	                            <div class="col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0 text-center">
	                                <ul class="fa-ul" id="desc-row">
	                                    <li><i class="fa-li fa fa-th-list"></i>Категория: <a href="/catalog/<?=$alias['liquid']?>/<?=$liquid['category']?>"><?=$catList[$liquid['category']]?></a></li>
	                                    <?/*<li><i class="fa-li fa fa-tags"></i>Теги: <?=writeTagsLinks($liquid['tags'])?></li>*/?>
	                                </ul>
                                </div>
                            </div><!-- /.product-desc -->
                        </div>
                    </div><!-- /.row -->
                </form>
                <div class="col-md-12">
                    <?php include "tpl/commentblock.php";?>
                </div>
            </div><!-- /.main-col -->
            <div class="sidebar col-md-3 col-sm-12 col-xs-12 pull-left">
                <?php include "tpl/widgets.php"; ?>
            </div><!-- /.sidebar -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!--// End Shop 1-6 v1 -->

