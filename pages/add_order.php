<?php

	// сохранение ордера перед отправкой в чекаут
	$post = (!empty($_POST)) ? makeSafeArray($_POST) : FALSE;

	$res = array(
			'res'   => FALSE,
			'msg'   => 'Ошибка сохранения заказа в БД!'
	);

	//ЕСЛИ включено в админке, обрабатываем разрешение на участие в рассылке
	if ( ! empty($promos['subscriptions_enabled'])){
		if ( ! empty($post['getSubscription'])){
			$subemail = trim($post['email']);
			// добавляем запись в таблицу и создаём куку
			$exp = addEmailToSubs($subemail);
			setcookie("hide_subs", 1, time() + $exp);
		}
	}

	if ( ! empty($post) ) {
		$cartTotal = 0;
		$cartCount = 0;

		$name = stripslashes($post['fullname']);
		$email = trim($post['email']);

		// получаем id нового заказа
		$lastid = getLastOrderId()+1;

		/************* формируем сообщение для админов И БД */
		$message['admins'] = "<h1>Новый заказ с сайта kosmosvape.ru</h1>";
		$message['admins'] .= "Заказчик: {$name}<br/>";
		$message['admins'] .= "Контакты: {$post['phone']} {$email} <br/>";
		$message['admins'] .= "Вид доставки: {$delRus[$post['deliveryType']]}<br/><hr>";
		$message['admins'] .= "Сообщение: {$post['comments']}<br/>";
		$message['admins'] .= "<h2>Содержимое заказа:</h2>";
		$message['admins'] .= "<center><table border=1px cellspacing=0 cellpadding=4 style='padding:-1px;text-align:left'>";
		$message['admins'] .= "<thead><tr>";
		$message['admins'] .= "<th>ID</th>";
		$message['admins'] .= "<th>ТОВАР</th>";
		$message['admins'] .= "<th>ПРОИЗВОДИТЕЛЬ</th>";
		$message['admins'] .= "<th>ОСОБЕННОСТИ</th>";
		$message['admins'] .= "<th>ЦЕНА</th>";
		$message['admins'] .= "<th>КОЛИЧЕСТВО</th>";
		$message['admins'] .= "<th>ВСЕГО</th>";
		$message['admins'] .= "</tr></thead>";

		for ($i=0; $i<count($post['names']); $i++){
			$message['admins'] .= "<tr>";
			$message['admins'] .= "<td>{$post['codes'][$i]}</td>";
			$message['admins'] .= "<td>{$post['names'][$i]}</td>";
			$message['admins'] .= "<td>{$catList[$post['producers'][$i]]}</td>";
			$message['admins'] .= "<td>{$post['settings'][$i]}</td>";
			$message['admins'] .= "<td>{$post['costs'][$i]}р.</td>";
			$message['admins'] .= "<td>{$post['quantities'][$i]} шт.</td>";
			$message['admins'] .= "<td>".$post['paycosts'][$i]."р.</td>";
			$message['admins'] .= "</tr>";
			$cartCount += $post['quantities'][$i];
			$cartTotal += $post['paycosts'][$i];
		}
		$message['admins'] .= "<tr><td colspan=6>общее кол-во товаров:</td><td align=center colspan=2><b>".$cartCount.' шт.</b></td></tr>';
		$message['admins'] .= "<tr><td colspan=6>общая сумма заказа:</td><td align=center colspan=2><b>".$cartTotal.' р.</b></td></tr>';
		$message['admins'] .= "</table></center>";

		// сохраняем информацию о заказе в БД
		$mes = htmlspecialchars($message['admins'], ENT_QUOTES);
		$sql = "INSERT INTO orders (ticket, cartid, date, fullname, phone, email, comment, textform, state, delivery) VALUES ";
		$sql .= "('{$post['ticket']}', '{$post['cartId']}', NOW(),'{$name}', '{$post['phone']}', '{$email}', '{$post['comments']}', '{$mes}', 'created', '{$post['deliveryType']}')";
		$result = mysql_query($sql);

		if ($result) {
			// в случае успешного сохранения вычищаем из БД содержимое корзины
			deleteOrderFromCart($post['cartId']);

			// отправляем письма админам
			$subject = $name . " - новый заказ с kosmosvape.ru";
			$Reply = $email;
			$from = $name;

			$headers = "from: $from <$Reply>\nReply-To: $Reply \nContent-type: text/html";
			$to = 'kemosabhay@gmail.com, medvedev095@gmail.com, kosmosvape@gmail.com';
			//$to = 'kemosabhay@gmail.com';
			$mail = mail($to, $subject, $message['admins'], $headers);

			/************* формируем сообщение для заказчика с самовывозом */
			if ($post['deliveryType'] == 'self') {
				$message['user'] = "<h1>Новый заказ с сайта kosmosvape.ru</h1>";
				$message['user'] .= "Ваш заказ успешно сформирован и принят магазином<br/>";
				$message['user'] .= "В самое ближайшее время с вами свяжется менеджер магазина Космос Вейп для обсуждения деталей заказа. Номер Вашего заказа <b>{$lastid}</b><br/><hr>";

				$message['user'] .= "<h2>Содержимое заказа:</h2>";
				$message['user'] .= "<center><table border=1px cellspacing=0 cellpadding=4 style='padding:-1px;text-align:left'>";
				$message['user'] .= "<thead><tr>";
				$message['user'] .= "<th>ID</th>";
				$message['user'] .= "<th>ТОВАР</th>";
				$message['user'] .= "<th>ПРОИЗВОДИТЕЛЬ</th>";
				$message['user'] .= "<th>ОСОБЕННОСТИ</th>";
				$message['user'] .= "<th>ЦЕНА</th>";
				$message['user'] .= "<th>КОЛИЧЕСТВО</th>";
				$message['user'] .= "<th>ВСЕГО</th>";
				$message['user'] .= "</tr></thead>";
				for ($i = 0; $i < count($post['names']); $i++) {
					$message['user'] .= "<tr>";
					$message['user'] .= "<td>{$post['codes'][$i]}</td>";
					$message['user'] .= "<td>{$post['names'][$i]}</td>";
					$message['user'] .= "<td>{$catList[$post['producers'][$i]]}</td>";
					$message['user'] .= "<td>{$post['settings'][$i]}</td>";
					$message['user'] .= "<td>{$post['costs'][$i]}р.</td>";
					$message['user'] .= "<td>{$post['quantities'][$i]} шт.</td>";
					$message['user'] .= "<td>" . $post['paycosts'][$i] . "р.</td>";
					$message['user'] .= "</tr>";
					$cartCount += $post['quantities'][$i];
					$cartTotal += $post['paycosts'][$i];
				}
				$message['user'] .= "<tr><td colspan=6>общее кол-во товаров:</td><td align=center colspan=2><b>" . $cartCount . ' шт.</b></td></tr>';
				$message['user'] .= "<tr><td colspan=6>общая сумма заказа:</td><td align=center colspan=2><b>" . $cartTotal . ' р.</b></td></tr>';
				$message['user'] .= "</table></center>";
				$message['user'] .= "Спасибо что выбрали нас!<br/>";

				$subject = $name . " - новый заказ с kosmosvape.ru";
				$Reply = 'kosmosvape@gmail.com';
				$from = 'kosmosvape@gmail.com';

				// отправляем письмо юзеру
				$headers = "from: $from <$Reply>\nReply-To: $Reply \nContent-type: text/html";
				$to = $email;
				$mail = mail($to, $subject, $message['user'], $headers);
			}

			// формируем ответ
			$res = array(
						'res'   => TRUE,
						'msg'   => 'Ваш заказ успешно отправлен',
						'orid'   => $lastid
			);
		}
	}

	echo json_encode($res);