<?
    // сохраняем изменения
    $post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
    if ($post) {
        if (saveRow($post,'devices')) {
            echo '<div class="alert alert-success" role="alert">Изменения сохранены</div>';
            $goodId == $post['url_name'];
        } else {
            echo '<div class="alert alert-danger" role="alert">При сохранении возникли ошибки</div>';
        }
    }
?>
<div class="row">
    <div class="underlined-title">
        <div class="editContent">
            <? if ($goodId == 'new') : ?>
                <h2>Создание нового устройства</h2>
                <?
                $dev = array(
                    'id' => '',
                    'name' => '',
                    'img_alt' => '',
                    'url_name' => '',
                    'producer' => '',
                    'category' => '',
                    'color' => array(),
                    'description' => '',
                    'volume' => '',
                    'price' => '',
                    'quantity' => '',
                    'meta_title' => '',
                    'meta_description' => '',
                    'meta_keywords' => ''
                );
                ?>
            <? else :?>
                <h2>Редактирование устройства #<?=$goodId?></h2>
                <? $dev = getDeviceByName($goodId); ?>
            <? endif; ?>
        </div>
        <hr>
    </div>
</div>
<div class="editContent">
    <form method="post" action="#" name="devform" id="devform">
        <input type="hidden" class="form-control" id="id" name="id"  value="<?=$dev['id']?>">
        <div class="row">
            <div class="col-md-2 text-center">
                <div class="product-image text-center">
                    <? if ($goodId != 'new') : ?>
                        <? $color = (empty($dev['color'][0])) ? '' : '_'.$dev['color'][0] ;?>
                        <? $img = safeFile("/images/devices/{$dev['url_name']}{$color}_big.jpg", 'big', 'device',$dev['producer']);?>
                        <img src="<?=$img?>" class="img-thumbnail img-small" alt="<?=$dev['name']?>"/>
                    <? else: ?>
                        <p> Картинка не задана</p>
                    <? endif; ?>
                </div>

            </div>
            <div class="form-group col-md-6">
                В имени картинки товара должно использоваться значение, указанное в поле "АЛИАС".
                Для каждого девайса (а так же для каждого цвета , если у устройства етсь несколько цветов) создаются по 2 картинки (если у устройства нет цветов, блок "[COLOR]_" опускаем ):
                <ul class="circle">
                    <li>маленькая (263*350) <b>[ALIAS]_[COLOR]_small.jpg</b></li>
                    <li>большая(440*586) <b>[ALIAS]_[COLOR]_big.jpg</b></li>
                </ul>
                <div class="alert alert-info admin-alert">
                    Добавление картинок в данный момент недоступно, поэтому  готовые картинки следует загрузить на сервер по FTP а папку <i>www/images/devices</i>
                </div>
            </div>
            <div class="form-group col-md-4">
                <label>Цвет</label>
                <? foreach ($colors as $key => $val) : ?>
                    <? $checked = (in_array($key, $dev['color'])) ? 'checked' : '';?>
                    <div class="checkbox">
                        <label for="color_<?=$key?>">
                            <input type="checkbox" id="color_<?=$key?>" name="color[<?=$key?>]" <?=$checked?> placeholder="Цвет"><b><?=$val?></b> (<?=$key?>)
                        </label>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="name">Название (отображается на странице)</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="<?=$dev['name']?>">
            </div>
            <div class="form-group col-md-6">
                <label for="img_alt">Аlt для картинки</label>
                <input type="text" class="form-control" id="img_alt" name="img_alt" placeholder="Название" value="<?=$dev['img_alt']?>">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8">
                <label for="url_name">Алиас (используется в URL и названиях картинок - латинскими буквами без пробелов) </label>
                <input type="text" class="form-control" id="url_name" name="url_name" placeholder="Алиас" value="<?=$dev['url_name']?>">
            </div>

            <div class="form-group text-left col-md-4">
                <label for="weight">Вес (г.)</label>
                <input type="text" class="form-control" id="weight" name="weight" placeholder="Вес" value="<?=$dev['weight']?>">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-10">
                <label for="remark">Ремарка -  краткое описание товара(&lsaquo;h3&rsaquo;&lsaquo;/h3&rsaquo;)</label>
                <textarea class="form-control" id="remark>" name="remark" placeholder="Ремарка"  rows="1"><?=$dev['remark']?></textarea>
            </div>
            <div class="form-group col-md-2">
                <label for="quantity">В наличии (шт.)</label>
                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Количество в наличии" value="<?=$dev['quantity']?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="volume">Мощность</label>
                    <input type="text" class="form-control" id="volume" name="volume" placeholder="Мощность" value="<?=$dev['volume']?>">
                </div>
                <div class="form-group">
                    <label for="price">Цена (руб.)</label>
                    <input type="text" class="form-control" id="price" name="price" placeholder="Цена" value="<?=$dev['price']?>">
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="producer">Производитель</label>
                    <select class="form-control" id="producer" name="producer" placeholder="Производитель">
                        <? foreach ($categories['producer'] as $key => $val ) : ?>
                            <option <?=($key==$dev['producer'])?'selected':'';?> value="<?=$key?>"><?=$val?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="category">Категория</label>
                    <select class="form-control" id="category" name="category" placeholder="Категория">
                        <? foreach ($categories['device'] as $key => $val ) : ?>
                            <option <?=($key==$dev['category'])?'selected':'';?> value="<?=$key?>"><?=$val?></option>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="description">Описание(&lsaquo;p&rsaquo;&lsaquo;/p&rsaquo;)</label>
                    <textarea class="form-control" id="description>" name="description" placeholder="Описание"  rows="5"><?=$dev['description']?></textarea>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="meta_title"><u>meta title</u></label>
                <textarea class="form-control" id="meta_title" name="meta_title" rows="2"><?=$dev['meta_title']?></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="meta_keywords"><u>meta keywords</u></label>
                <textarea class="form-control" id="meta_keywords" name="meta_keywords" rows="2"><?=$dev['meta_keywords']?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="meta_description"><u>meta description</u></label>
                <textarea class="form-control" id="meta_description" name="meta_description" rows="1"><?=$dev['meta_description']?></textarea>
            </div>
        </div>
        <div class="editContent text-center">
            <button type="submit" class="btn btn-lg btn-success">Сохранить</button>
        </div>
    </form>
</div>