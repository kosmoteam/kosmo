<?
    $categories = getCategories();
?>

<div class="row">
    <div class="underlined-title">
        <div class="editContent">
            <h2>Категории</h2>
        </div>
        <hr>
    </div>
</div>

<!-- Start Content Block 1-8 -->
<section class="content-block">
    <div class="container">
        <ul class="nav nav-tabs text-center" role="tablist" id="myTab">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Производители</a></li>
            <li><a href="#tab2" role="tab" data-toggle="tab">Категории Устройств</a></li>
            <li><a href="#tab3" role="tab" data-toggle="tab">Категории Жидкостей </a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
                <div class="editContent">
                    <h4>Производители <a href="/admin/goods/editcat/new"><i class="fa fa-plus-square-o"></i></a></h4>
                </div>
                <div class="editContent table-grid">
                    <table class="table table-hover table-admin">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Алиас</th>
                            <th>Название</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($categories['producer'] as $key => $val ) : ?>
                            <tr>
                                <th scope="row">
                                    <a href="/admin/removeitem/producer/<?=$key?>" class="remove row-remove" title="Remove this item" data-toggle="tooltip" data-id="1" data-placement="top"><i class="fa fa-trash"></i></a>
                                    &nbsp;
                                    <a href="/admin/goods/editcat/<?=$key?>"><i class="fa fa-pencil-square-o"></i></a>
                                </th>
                                <td><?=$key?></td>
                                <td><?=$val?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /#tab1 -->
            <div class="tab-pane fade" id="tab2">
                <div class="editContent">
                    <h4>Категории Устройств <a href="/admin/goods/editcat/new"><i class="fa fa-plus-square-o"></i></a></h4>
                </div>
                <div class="editContent table-grid">
                    <table class="table table-hover table-admin">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Алиас</th>
                            <th>Название</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($categories['device'] as $key => $val ) : ?>
                            <tr>
                                <th scope="row">
                                    <a href="/admin/removeitem/categoryd/<?=$key?>" class="remove row-remove" title="Remove this item" data-toggle="tooltip" data-id="1" data-placement="top"><i class="fa fa-trash"></i></a>
                                    &nbsp;
                                    <a href="/admin/goods/editcat/<?=$key?>"><i class="fa fa-pencil-square-o"></i></a>
                                </th>
                                <td><?=$key?></td>
                                <td><?=$val?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /#tab2 -->
            <div class="tab-pane fade" id="tab3">
                <div class="editContent">
                    <h4>Категории Жидкостей <a href="/admin/goods/editcat/new"><i class="fa fa-plus-square-o"></i></a></h4>
                </div>
                <div class="editContent table-grid">
                    <table class="table table-hover table-admin">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Алиас</th>
                            <th>Название</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($categories['liquid'] as $key => $val ) : ?>
                            <tr>
                                <th scope="row">
                                    <a href="/admin/removeitem/categoryl/<?=$key?>" class="remove row-remove" title="Remove this item" data-toggle="tooltip" data-id="1" data-placement="top"><i class="fa fa-trash"></i></a>
                                    &nbsp;
                                    <a href="/admin/goods/editcat/<?=$key?>"><i class="fa fa-pencil-square-o"></i></a>
                                </th>
                                <td><?=$key?></td>
                                <td><?=$val?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /#tab3 -->
        </div><!-- /.tab-content -->
    </div><!-- /.container -->
</section>
<!--// End Content Block 1-8 -->