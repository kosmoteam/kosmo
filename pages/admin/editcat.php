<?
    // сохраняем изменения
    $post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
    if ($post) {
        if (saveRow($post,'category')) {
            echo '<div class="alert alert-success" role="alert">Изменения сохранены</div>';
           $goodId == $post['url_name'];
        } else {
            echo '<div class="alert alert-danger" role="alert">При сохранении возникли ошибки</div>';
        }
    }
?>
<div class="row">
    <div class="underlined-title">
        <div class="editContent">
            <? if ($goodId == 'new') : ?>
                <h2>Создание новой категории</h2>
                <?
                $cat = array(
                        'id' => '',
                        'name' => '',
                        'url_name' => '',
                        'parent' => '',
                        'meta_title' => '',
                        'meta_description' => '',
                        'meta_keywords' => ''
                );
                ?>
            <? else :?>
                <h2>Редактирование категории #<?=$goodId?></h2>
                <? $cat = getCategoryByName($goodId); ?>
            <? endif; ?>
        </div>
        <hr>
    </div>
</div>
<div class="editContent">
    <form method="post" action="#" name="catform" id="catform">
        <input type="hidden" class="form-control" id="id" name="id"  value="<?=$cat['id']?>">
        <div class="row">
            <div class="form-group col-md-12">
                <label for="url_name">Алиас (используется в URL и названиях картинок - латинскими буквами без пробелов)</label>
                <input type="text" class="form-control" id="url_name" name="url_name" placeholder="Алиас" value="<?=$cat['url_name']?>">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="name">Название</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="<?=$cat['name']?>">
            </div>
            <div class="form-group col-md-6">
                <label for="producer">Тип категории</label>
                <select class="form-control" id="parent" name="parent">
                    <option <?=($cat['parent']=='liquid')?'selected':'';?> value="liquid">Категория Жидкостей</option>
                    <option <?=($cat['parent']=='device')?'selected':'';?> value="device">Категория Устройств</option>
                    <option <?=($cat['parent']=='producer')?'selected':'';?> value="producer">Производитель</option>
                </select>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="meta_title"><u>meta title</u></label>
                <textarea class="form-control" id="meta_title" name="meta_title" rows="2"><?=$cat['meta_title']?></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="meta_keywords"><u>meta keywords</u></label>
                <textarea class="form-control" id="meta_keywords" name="meta_keywords" rows="2"><?=$cat['meta_keywords']?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="meta_description"><u>meta description</u></label>
                <textarea class="form-control" id="meta_description" name="meta_description" rows="1"><?=$cat['meta_description']?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="catalog_text"><u>текст при фильтрации в каталоге</u></label>
                <textarea class="form-control" id="catalog_text" name="catalog_text" rows="1"><?=$cat['catalog_text']?></textarea>
            </div>
        </div>

        <div class="editContent text-center">
            <button type="submit" class="btn btn-lg btn-success">Сохранить</button>
        </div>
    </form>
</div>