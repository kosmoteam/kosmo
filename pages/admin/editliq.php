<?
    // сохраняем изменения
    $post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
    if ($post) {
        if (saveRow($post,'liquids')) {
            echo '<div class="alert alert-success" role="alert">Изменения сохранены</div>';
            $goodId == $post['url_name'];
        } else {
            echo '<div class="alert alert-danger" role="alert">При сохранении возникли ошибки</div>';
        }
    }
?>
<div class="row">
    <div class="underlined-title">
        <div class="editContent">
            <? if ($goodId == 'new') : ?>
                <h2>Создание новой жидкости</h2>
                <?
                $liq = array(
                        'id' => '',
                        'name' => '',
                        'url_name' => '',
                        'img_alt' => '',
                        'producer' => '',
                        'category' => '',
                        'nicotine' => '',
                        'volume1' => '',
                        'price1' => '',
                        'volume2' => '',
                        'price2' => '',
                        'description' => '',
                        'tags' => '',
                        'quantity' => '',
                        'meta_title' => '',
                        'meta_description' => '',
                        'meta_keywords' => ''
                );
                ?>
            <? else :?>
                <h2>Редактирование жидкости #<?=$goodId?></h2>
                <? $liq = getLiquidByName($goodId); ?>
            <? endif; ?>
        </div>
        <hr>
    </div>
</div>
<div class="editContent">
    <form method="post" action="#" name="liqform" id="liqform">
        <input type="hidden" class="form-control" id="id" name="id"  value="<?=$liq['id']?>">
        <div class="row">
            <div class="col-md-7 text-center">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="product-image text-center">
                            <? if ($goodId != 'new') : ?>
                                <? $img = safeFile("/images/liquids/{$liq['url_name']}_big.jpg", 'big', 'liquid',$liq['producer']);?>
                                <img src="<?=$img?>" class="img-thumbnail img-small" alt="<?=$liq['name']?>"/>
                            <? else: ?>
                                <p> Картинка не задана</p>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="col-md-8 text-left">
                        В имени картинки товара должно использоваться значение, указанное в поле "АЛИАС". Для каждой жидкости создаются по 2 картинки:
                        <ul class="circle">
                            <li>маленькая (263*350) <b>[ALIAS]_small.jpg</b></li>
                            <li>большая(440*586) <b>[ALIAS]_big.jpg</b></li>
                        </ul>
                        <div class="alert alert-info admin-alert">
                            Добавление картинок в данный момент недоступно, поэтому  готовые картинки следует загрузить на сервер по FTP а папку <i>www/images/liquids</i>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <label >Крепость(никотин)</label>
                        <? $nicotines = explode(',', $liq['nicotine']);?>
                        <div class="checkbox">
                            <label for="nicotine0">
                                <input type="checkbox" id="nicotine0" name="nicotine[0]" <?=(in_array('0',$nicotines))?'checked':'';?>>0
                            </label>
                        </div>
                        <div class="checkbox">
                            <label for="nicotine3">
                                <input type="checkbox" id="nicotine3" name="nicotine[3]" <?=(in_array('3',$nicotines))?'checked':'';?>>3
                            </label>
                        </div>
                        <div class="checkbox">
                            <label for="nicotine6">
                                <input type="checkbox" id="nicotine6" name="nicotine[6]" <?=(in_array('6',$nicotines))?'checked':'';?>>6
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8 text-center">
                        <div class="form-group col-md-6">
                            <label for="quantity">Кол-во(шт.)</label>
                            <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Количество в наличии" value="<?=$liq['quantity']?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="weight">Вес (г.)</label>
                            <input type="text" class="form-control" id="weight" name="weight" placeholder="Вес" value="<?=$liq['weight']?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="name">Название (отображается на странице)</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="<?=$liq['name']?>">
                </div>
                <div class="form-group">
                    <label for="url_name">Алиас (используется в URL и названиях картинок - латинскими буквами без пробелов)</label>
                    <input type="text" class="form-control" id="url_name" name="url_name" placeholder="Алиас" value="<?=$liq['url_name']?>">
                </div>
                <div class="form-group">
                    <label for="img_alt">Аlt для картинки</label>
                    <input type="text" class="form-control" id="img_alt" name="img_alt" placeholder="Название" value="<?=$liq['img_alt']?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="producer">Производитель</label>
                <select class="form-control" id="producer" name="producer" placeholder="Производитель">
                    <? foreach ($categories['producer'] as $key => $val ) : ?>
                        <option <?=($key==$liq['producer'])?'selected':'';?> value="<?=$key?>"><?=$val?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="category">Категория</label>
                <select class="form-control" id="category" name="category" placeholder="Категория">
                    <? foreach ($categories['liquid'] as $key => $val ) : ?>
                        <option <?=($key==$liq['category'])?'selected':'';?> value="<?=$key?>"><?=$val?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="volume1">Объём_1</label>
                <input type="text" class="form-control" id="volume" name="volume1" placeholder="Объём1" value="<?=$liq['volume1']?>">
            </div>
            <div class="form-group col-md-3">
                <label for="price1">Цена_1 (руб.)</label>
                <input type="text" class="form-control" id="price1" name="price1" placeholder="Цена1" value="<?=$liq['price1']?>">
            </div>
            <div class="form-group col-md-3">
                <label for="volume2">Объём_2</label>
                <input type="text" class="form-control" id="volume2" name="volume2" placeholder="Объём2" value="<?=$liq['volume2']?>">
            </div>
            <div class="form-group col-md-3">
                <label for="price2">Цена_2 (руб.)</label>
                <input type="text" class="form-control" id="price2" name="price2" placeholder="Цена2" value="<?=$liq['price2']?>">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="tags">Теги (вводятся через запятую)</label>
                <textarea class="form-control" id="tags" name="tags" placeholder="Теги(вводятся через запятую)"  rows="2"><?=$liq['tags']?></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="remark">Ремарка -  краткое описание товара(&lsaquo;h3&rsaquo;&lsaquo;/h3&rsaquo;)</label>
                <textarea class="form-control" id="remark>" name="remark" placeholder="Описание" rows="2"><?=$liq['remark']?></textarea>
            </div>

            <div class="form-group col-md-12">
                <label for="description">Описание(&lsaquo;p&rsaquo;&lsaquo;/p&rsaquo;)</label>
                <textarea class="form-control" id="description" name="description" placeholder="Описание"  rows="2"><?=$liq['description']?></textarea>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="meta_title"><u>meta title</u></label>
                <textarea class="form-control" id="meta_title" name="meta_title" rows="1"><?=$liq['meta_title']?></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="meta_keywords"><u>meta keywords</u></label>
                <textarea class="form-control" id="meta_keywords" name="meta_keywords" rows="1"><?=$liq['meta_keywords']?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="meta_description"><u>meta description</u></label>
                <textarea class="form-control" id="meta_description" name="meta_description" rows="1"><?=$liq['meta_description']?></textarea>
            </div>
        </div>
        <div class="editContent text-center">
            <button type="submit" class="btn btn-lg btn-success">Сохранить</button>
        </div>
    </form>
</div>