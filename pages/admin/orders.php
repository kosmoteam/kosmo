<? if (empty($areaItem)) : ?>
    <!-- выводим список заказов-->
    <? $orders = getOrders(); ?>
    <div class="row">
        <div class="underlined-title">
            <div class="editContent">
                <h2>Заказы</h2>
            </div>
            <hr>
        </div>
    </div>
    <div class="editContent">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-center">#</th>
                <th>ID</th>
                <th>Дата</th>
                <th>Заказчик</th>
                <th>Телефон</th>
                <th>Email</th>
                <th>Доставка</th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($orders as $order) : ?>
                <tr>
                    <th scope="row"><a href="/admin/orders/<?=$order['id']?>"><i class="fa fa-info-circle"></i></a></th>
                    <td><?=$order['id']?></td>
                    <td><?=$order['date']?></td>
                    <td><?=$order['fullname']?></td>
                    <td><?=$order['phone']?></td>
                    <td><?=$order['email']?></td>
                    <td>
                        <? if ($order['delivery'] == 'self' ) :?>
                            Самовывоз
                        <? else :?>
                            <?="<a target='_blank' href='http://platform.checkout.ru/?show=orders/{$order['ch_order']}'>смотреть на checkout</a>"?>
                        <? endif; ?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
<? else : ?>
    <!-- отображаем данные о заказе-->
    <? $order = getOrder($areaItem); ?>
    <? if(!empty($order)):?>
        <div class="row">
            <div class="underlined-title">
                <div class="editContent">
                    <h2>Заказ #<?=$order['id']?></h2>
                    <h5><a href="/admin/orders">Вернуться к списку заказов</a></h5>
                </div>
                <hr>
            </div>
        </div>
        <div class="row">
            <table class="table table-hover">
                <tr>
                    <th scope="row">ID</th>
                    <td><?=$order['id']?></td>
                </tr><tr>
                    <th scope="row">Дата</th>
                    <td><?=$order['date']?></td>
                </tr><tr>
                    <th scope="row">Заказчик</th>
                    <td><?=$order['fullname']?></td>
                </tr><tr>
                    <th scope="row">Контакты</th>
                    <td><?=$order['phone']?> <?=$order['email']?></td>
                </tr><tr>
                    <th scope="row">Способ доставки</th>
                    <td><?=$delRus[$order['delivery']]?></td>
                </tr><tr>
                    <th scope="row">Состояние</th>
                    <td><?=$stateRus[$order['state']]?></td>
                </tr><tr>
                    <th scope="row">Доп. комментарии</th>
                    <td><?=htmlspecialchars_decode($order['comment'])?></td>
                </tr><tr>
                    <th scope="row">№ заказа на Checkout.ru</th>
                    <td>
                        <? if ($order['delivery'] == 'self' ) :?>
                        <? else :?>
                            <a target="_blank" href="http://platform.checkout.ru/?show=orders/<?=$order['ch_order']?>">#<?=$order['ch_order']?></a>
                        <? endif; ?>
                    </td>
                </tr><tr>
                    <th scope="row">Информация с Checkout.ru</th>
                    <td colspan=2 class="order-text"><?=htmlspecialchars_decode($order['ch_answer'])?></td>
                </tr><tr>
                    <td colspan=2 class="order-text"><?=htmlspecialchars_decode($order['textform'])?></td>
                </tr>
            </table>
        </div><!-- /.row -->
    <? else : ?>
        <div class="row">
            <div class="underlined-title">
                <div class="editContent">
                    <h2>Неправильный урл</h2>
                    <h5><a href="/admin/orders">Вернуться к списку заказов</a></h5>
                </div>
                <hr>
            </div>
        </div>
    <? endif; ?>
<? endif; ?>

