<?php
$category = FALSE;
$devices = getDevices($category);
$liquids = getLiquids($category);

//сохраняем изменения
$post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
//pp($post);

if ($post) {
	$voices = $post['voiceCnt'];
	$rate = $post['rate'];

	if ($voices > 0 AND $rate > 0 AND $rate <= 5){
		// накидываем рейтинги Жидкостям
		foreach ($liquids as $liq){
			$item = array(
				'user' => 'user',
				'type' => 'liquid',
				'id' =>  $liq['url_name'],
				'rate' => $rate
			);

			$i = 1;
			while ($i <= $voices) {
				saveRate($item);
				$i++;
			}
		}
		// накидываем рейтинги девайсам
		foreach ($devices as $dev){
			$item = array(
				'user' => 'user',
				'type' => 'device',
				'id' =>  $dev['url_name'],
				'rate' => $rate
			);
			$i = 1;
			while ($i <= $voices) {
				saveRate($item);
				$i++;
			}
		}
		echo '<div class="alert alert-success" role="alert">Товарам приписано <b><u>'.$voices.'</b></u> голос(a)(ов) с оценкой <b><u>'.$rate.'</u></b></div>';
	} else {
		echo '<div class="alert alert-danger" role="alert">Голичество головос болжно быть положительным. Оценка должны быть от 1 до 5</div>';
	}
}
?>


<div class="row">
<div class="underlined-title">
    <div class="editContent">
        <h2>Товары</h2>
    </div>
    <hr>
</div>
</div>

<!-- Start Content Block 1-8 -->
<section class="content-block">
<div class="container">
    <ul class="nav nav-tabs text-center" role="tablist" id="myTab">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Жидкости</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Устройства</a></li>
        <li><a href="#tab3" role="tab" data-toggle="tab">Накрутка рейтинга</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
            <? if (!empty($liquids)): ?>
                <div class="editContent text-center">
                    <h4><a href="/admin/goods/editliq/new"><i class="fa fa-plus-square-o"></i> создать новую жидкость</a></h4>
                    <hr>
                </div>
                <div class="editContent table-grid">
                    <table class="table table-hover table-admin">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th colspan=2>Товар</th>
                            <th>Алиас</th>
                            <th>Производитель</th>
                            <th>Категория</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($liquids as $liquid) :  ?>
                            <?$image = safeFile("/images/liquids/{$liquid['url_name']}_small.jpg", 'small', 'liquid', $liquid['producer']);?>
                            <tr>
                                <th scope="row">
                                    <a href="/admin/removeitem/liquid/<?=$liquid['url_name']?>" class="remove row-remove" title="Remove this item" data-toggle="tooltip" data-id="1" data-placement="top"><i class="fa fa-trash"></i></a>
                                    &nbsp;
                                    <a href="/admin/goods/editliq/<?=$liquid['url_name']?>" title="Edit this item" data-toggle="tooltip" ><i class="fa fa-pencil-square-o"></i></a>
                                </th>
                                <td><div class="thumbnail" ><img src="<?=$image?>" style="width: 20px; height: auto;"/> </div></td>
                                <td><?=$liquid['name']?></td>
                                <td><?=$liquid['url_name']?></td>
                                <td><?=$catList[$liquid['producer']]?></td>
                                <td><?=$catList[$liquid['category']]?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?endif;?>

        </div><!-- /#tab1 -->
        <div class="tab-pane fade" id="tab2">
            <?  if (!empty($devices)): ?>
                <div class="editContent text-center">
                    <h4><a href="/admin/goods/editdev/new"><i class="fa fa-plus-square-o"></i> создать новое устройство</a></h4>
                    <hr>
                </div>
                <div class="editContent table-grid">
                    <table class="table table-hover table-admin">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th colspan=2>Товар</th>
                            <th>Алиас</th>
                            <th>Производитель</th>
                            <th>Категория</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($devices as $device) :  ?>
                            <?
                            $color = (empty($device['color'][0])) ? '' : '_'.$device['color'][0];
                            $image = safeFile("/images/devices/{$device['url_name']}{$color}_small.jpg", 'small', 'device', $device['producer']);
                            ?>
                            <tr>
                                <th scope="row">
                                    <a href="/admin/removeitem/device/<?=$device['url_name']?>" class="remove row-remove" title="Remove this item" data-toggle="tooltip" data-id="1" data-placement="top"><i class="fa fa-trash"></i></a>
                                    &nbsp;
                                    <a href="/admin/goods/editdev/<?=$device['url_name']?>"><i class="fa fa-pencil-square-o"></i></a>
                                </th>
                                <td><div class="thumbnail" ><img src="<?=$image?>" style="width: 20px; height: auto;"/> </div></td>
                                <td><?=$device['name']?></td>
                                <td><?=$device['url_name']?></td>
                                <td><?=$catList[$device['producer']]?></td>
                                <td><?=$catList[$device['category']]?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <? endif; ?>
        </div><!-- /#tab2 -->
        <div class="tab-pane fade" id="tab3">
	        <div class="editContent">
		        <form class="form-inline" method="post" action="#" name="rateform" id="rateform" enctype="multipart/form-data">
			        <div class="form-group">
				        <label for="voiceCnt">Добавить количество голосов </label>
				        <input type="text" class="form-control" id="voiceCnt" name="voiceCnt" placeholder="голосов" >
			        </div>
			        <div class="form-group">
				        <label for="rate">с оценкой (1-5)</label>
			            <input type="text" class="form-control" id="rate" name="rate" placeholder="оценка">
			        </div>
			        <button type="submit" class="btn btn-primary">Добавить</button>
		        </form>
	        </div>
        </div><!-- /#tab3 -->
    </div><!-- /.tab-content -->
</div><!-- /.container -->
</section>
<!--// End Content Block 1-8 -->