<?
    $post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
    $header = '<h2><a href="/admin/goods">Вернуться к списку товаров</a></h2>';
    $removed = FALSE;

    if ($post) {
        if ( removeItem($post) ) {
            echo '<div class="alert alert-success" role="alert">ЗАпись успешно удалена</div>';
            $removed = TRUE;
        } else {
            echo '<div class="alert alert-danger" role="alert">При удалении возникли ошибки</div>';
        }
    }

    // елси товар ещё не удалён -  отобразим его
    if ( ! $removed) {
        switch ($type) {
            case 'liquid':
                $header = '<h2>Удаление жидкости</h2>';
                $item = getLiquidByName($goodId);
                $itemName = "жидкость {$item['name']} ({$catList[$item['producer']]})";
                $pic = safeFile("/images/liquids/{$item['url_name']}_small.jpg", 'small', 'liquid',$item['producer']);
                $img = "<img src='{$pic}' class='img-thumbnail img-small' alt='{$item['name']}'/>";
                break;
            case 'device':
                $header = '<h2>Удаление устройства</h2>';
                $item = getDeviceByName($goodId);
                $itemName = "устройство {$item['name']} ({$catList[$item['producer']]})" ;
                $color = (empty($item['color'][0])) ? '' : '_'.$item['color'][0];
                $pic = safeFile("/images/devices/{$item['url_name']}{$color}_big.jpg", 'big', 'device',$item['producer']);
                $img = "<img src='{$pic}' class='img-thumbnail img-small' alt='{$item['name']}'/>";
                break;
            case 'categoryl':
                $item = getCategoryByName($goodId);
                $header = '<h2>Удаление категории</h2>';
                $itemName = "категорию {$item['name']} (жидкости)";
                $img = '';
                break;
            case 'categoryd':
                $item = getCategoryByName($goodId);
                $header = '<h2>Удаление категории</h2>';
                $itemName = "категорию {$item['name']} (устройства)";
                $img = '';
                break;
            case 'producer':
                $item = getCategoryByName($goodId);
                $header = '<h2>Удаление производителя</h2>';
                $itemName = "производителя {$item['name']}";
                $img = '';
                break;
        }
    }
?>

<div class="row">
    <div class="underlined-title">
        <div class="editContent">
            <?=$header;?>
        </div>
        <hr>
    </div>
</div>
<? if ( ! $removed) :?>
    <div class="editContent">
        <form method="post" action="#" name="delform" id="delform">
            <input type="hidden" id="id" name="id"  value="<?=$goodId?>">
            <input type="hidden" id="itemType" name="itemType"  value="<?=$type?>">

            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <?=$img?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p class="">Вы собираетесь удалить <b><?=$itemName?></b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="col-md-6 text-center">
                        <a href="/admin/goods" class="btn btn-danger">Отмена</a>
                    </div>
                    <div class="col-md-6 text-center">
                        <button type="submit" class="btn btn-success">Удалить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<? endif; ?>