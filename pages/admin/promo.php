<div class="row">
	<div class="underlined-title">
		<div class="editContent">
			<h2>Промо</h2>
		</div>
		<hr>
	</div>
</div>
<?
	 //сохраняем изменения
	$post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
	//pp($post);
	//pp($_FILES);

	if ( !empty($_FILES) AND !empty($_FILES['newBanner']['name']) ){
		$uploaddir = $_SERVER["DOCUMENT_ROOT"].'/img/ban/';
		//chmod($uploaddir, 1777);
		$uploadfile = $uploaddir . basename($_FILES['newBanner']['name']);
		if (move_uploaded_file($_FILES['newBanner']['tmp_name'], $uploadfile)) {
			//chmod($uploadfile, 0777);
			$post['cat_head_banner_img'] = basename($_FILES['newBanner']['name']);
		} else {
			echo '<div class="alert alert-danger" role="alert">При аплоаде возникли ошибки</div>';
		}
	}

	if ($post) {
		unset($post['cat_head_saved']);
		//чекбоксы
		if( ! isset($post['cat_head_banner_enabled'])){
			$post['cat_head_banner_enabled'] = 0;
		} elseif (($post['cat_head_banner_enabled'] == 'on')){
			$post['cat_head_banner_enabled'] =  1;
		}
		if( ! isset($post['subscriptions_enabled'])){
			$post['subscriptions_enabled'] = 0;
		} elseif (($post['subscriptions_enabled'] == 'on')){
			$post['subscriptions_enabled'] =  1;
		}
		if( ! isset($post['show_old_price_device'])){
			$post['show_old_price_device'] = 0;
		} elseif (($post['show_old_price_device'] == 'on')){
			$post['show_old_price_device'] =  1;
		}
		if( ! isset($post['show_old_price_liquid'])){
			$post['show_old_price_liquid'] = 0;
		} elseif (($post['show_old_price_liquid'] == 'on')){
			$post['show_old_price_liquid'] =  1;
		}




		if (savePromos($post)) {
			echo '<div class="alert alert-success" role="alert">Изменения сохранены</div>';
		} else {
			echo '<div class="alert alert-danger" role="alert">При сохранении возникли ошибки</div>';
		}
	}
	// получаем промо-настройки
	$promos = getSitePromos();

	// получаем список баннеров
	$path    =  $_SERVER["DOCUMENT_ROOT"].'/img/ban';
	$files = scandir($path);
	$banners = array();
	foreach ($files as $file){
		if ($file !='.' AND  $file !='..'){
			$banners[] = $file;
		}
	}
	// дисаблим форму елси галка отжата
	$banDisabled = ($promos['cat_head_banner_enabled'] == 1) ? '' : 'disabled';

	$subscribers = getSubscribers();
?>
<div class="container">
	<form method="post" action="#" name="promoform" id="promoform" enctype="multipart/form-data">
		<ul class="nav nav-tabs text-center" role="tablist" id="myTab">
			<li class="active"><a href="#tab1" role="tab" data-toggle="tab">Баннер в каталоге</a></li>
			<li><a href="#tab2" role="tab" data-toggle="tab">Подписка</a></li>
			<li><a href="#tab3" role="tab" data-toggle="tab">"Старая цена"</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1">
				<div class="row">
					<div class="form-group col-md-12 checkbox">
						<label>
							<input type="checkbox" id="cat_head_banner_enabled" name="cat_head_banner_enabled" type="checkbox" <?=($promos['cat_head_banner_enabled'] == 1) ? 'checked':'';?>> показывать баннер в шапке каталога
						</label>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6 text-center">
						<div class="form-group text-center">
							<img src="/img/ban/<?=$promos['cat_head_banner_img']?>" class="img-thumbnail ban-preview col-md-12 <?=$banDisabled?>" alt=""/>
						</div>
					</div>
					<div class="form-group col-md-6">
						<label for="newBanner">Загрузить новый</label>
						<input type="file" id="newBanner" name="newBanner" <?=$banDisabled?>>
						<p class="help-block">выберите новое изображение и нажмите кнопку <b>СОХРАНИТЬ</b></p>
					</div>
				</div>

				<? if (count($banners) > 0):?>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="url_name">Имя баннера</label>
							<select  id="cat_head_banner_img" name="cat_head_banner_img" class="form-control" <?=$banDisabled?>>
								<?foreach($banners as $banner){
									$selected = ($banner == $promos['cat_head_banner_img']) ? "selected='selected'" : '';
									echo "<option {$selected} value='{$banner}'>{$banner}</option>";
								}?>
							</select>
						</div>
						<div class="form-group col-md-6">
							<label for="cat_head_banner_url">URL баннера</label>
							<input type="text" class="form-control" id="cat_head_banner_url" name="cat_head_banner_url" placeholder="URL баннера" value="<?=$promos['cat_head_banner_url']?>" <?=$banDisabled?>>
							<input type="hidden" name="cat_head_saved" id="cat_head_saved" value="1">
						</div>
					</div>
				<?else:?>
					папка баннеров пуста
				<?endif;?>
			</div>
			<div class="tab-pane fade in" id="tab2">
				<div class="row">
					<div class="form-group col-md-12 checkbox">
						<label>
							<input type="checkbox" id="subscriptions_enabled" name="subscriptions_enabled" type="checkbox" <?=($promos['subscriptions_enabled'] == 1) ? 'checked':'';?>> собирать emails для подписки
						</label>
					</div>
				</div>
				<hr>
				<div class="editContent text-center col-md-12">
					<h5>Подписчики (<?=count($subscribers)?>)</h5>
				</div>
				<div class="editContent col-md-6  col-md-offset-3">
					<table class="table table-hover">
						<thead>
						<tr>
							<th class="text-center">#</th>
							<th>Email</th>
						</tr>
						</thead>
						<tbody>
						<? foreach ($subscribers as $subscriber) : ?>
							<tr>
								<td class="text-center"><?=$subscriber['id']?></td>
								<td><?=$subscriber['email']?></td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="tab-pane fade in" id="tab3">
				<div class="row">
					<div class="col-md-6">
						<h5>"Старая цена" для жидкостей</h5>
						<div class="form-group col-md-12 checkbox">
							<label>
								<input type="checkbox" id="show_old_price_liquid" name="show_old_price_liquid" type="checkbox" <?=($promos['show_old_price_liquid'] == 1) ? 'checked':'';?>> отображать "Старую цену" для жидкостей
							</label>
						</div>
						<div class="form-group col-md-12">
							<label for="old_price_liquid_percent"><u>Размер накрутки "старой цены в процентах"</u></label>
							<div class="input-group">
								<input type="text" class="form-control" id="old_price_liquid_percent" name="old_price_liquid_percent" placeholder="Накрутка" value="<?=$promos['old_price_liquid_percent']?>">
								<div class="input-group-addon">%</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<h5>"Старая цена" для устройств</h5>
						<div class="row">
							<div class="form-group col-md-12 checkbox">
								<label>
									<input type="checkbox" id="show_old_price_device" name="show_old_price_device" type="checkbox" <?=($promos['show_old_price_device'] == 1) ? 'checked':'';?>> отображать "Старую цену" для устройств
								</label>
							</div>
							<div class="form-group col-md-12">
								<label class ='' for="old_price_device_percent"><u>Размер накрутки "старой цены в процентах"</u></label>
								<div class="input-group col-md-12">
									<input   type="text" class="form-control" id="old_price_device_percent" name="old_price_device_percent" placeholder="Накрутка" value="<?=$promos['old_price_device_percent']?>">
									<div class="input-group-addon">%</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?/* foreach ($texts as $item) : */?><!--
            <div class="form-group">
                <label for="<?/*=$item['name']*/?>"><?/*=$textList[$item['name']]*/?></label>
                <textarea class="form-control" id="<?/*=$item['name']*/?>" name="<?/*=$item['name']*/?>" rows="2"><?/*=$item['text']*/?></textarea>
            </div>
        --><?/* endforeach;*/?>
		<div class="editContent text-center col-md-12">
			<button type="submit" class="btn btn-lg btn-success">Сохранить</button>
		</div>
	<!--</form>-->
</div>
<script>
$(document).ready(function() {
	$('#cat_head_banner_enabled').click(function() {
		if (!$(this).is(':checked')) {
			$('#newBanner').attr('disabled','disabled');
			$('#cat_head_banner_img').attr('disabled','disabled');
			$('#cat_head_banner_url').attr('disabled','disabled');
			$('.ban-preview').addClass('disabled');
		} else {
			$('#newBanner').removeAttr('disabled');
			$('#cat_head_banner_img').removeAttr('disabled');
			$('#cat_head_banner_url').removeAttr('disabled');
			$('.ban-preview').removeClass('disabled');
		}
	});
	$('#cat_head_banner_img').change(function() {
		$('.ban-preview').prop('src', '/img/ban/'+$(this).val());
	});

	//$("#file-0a").fileinput({
	//		uploadUrl: 'http://kosmo.test/img/ban',
	//		allowedFileExtensions : ['jpg', 'png','gif'],
	//		overwriteInitial: false,
	//		//allowedFileTypes: ['image', 'video', 'flash'],
	//		slugCallback: function(filename) {
	//			return filename.replace('(', '_').replace(']', '_');
	//		}
	//});
	//$("#file-0a").on('fileloaded', function(event, file, previewId, index) {
	//	alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
	//});
});
</script>