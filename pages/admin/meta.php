<div class="row">
    <div class="underlined-title">
        <div class="editContent">
            <h2>Метаданные</h2>
        </div>
        <hr>
    </div>
</div>
<?
    // сохраняем изменения
    $post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
    if ($post) {
        if (saveSiteTexts($post)) {
            echo '<div class="alert alert-success" role="alert">Изменения сохранены</div>';
        } else {
            echo '<div class="alert alert-danger" role="alert">При сохранении возникли ошибки</div>';
        }
    }
    // получаем текстовки
    $texts = getSiteTextsPairs();
?>
<div class="container">
    <form method="post" action="#" name="metaform" id="metaform">
        <ul class="nav nav-tabs text-center" role="tablist" id="myTab">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab">OG метатеги</a></li>
            <li ><a href="#tab2" role="tab" data-toggle="tab">Дефолтные метатеги</a></li>
            <li><a href="#tab3" role="tab" data-toggle="tab">КАТАЛОГ: метатеги</a></li>
            <li><a href="#tab4" role="tab" data-toggle="tab">КАТАЛОГ: текстовки</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
                <div class="editContent">
                    <p class="alert-info text-center">
                        Информация , которая отображается при вставке ссылку в социальные сети<br/>
                        Используются по умолчанию на ВСЕХ страницах, если не заданы особые метатеги
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-3 text-center">
                        <div class="form-group text-center">
                            <img src="<?=$texts['og_image']?>" class="img-thumbnail img-small" alt=""/>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="og_image"><u>og:image</u></label>
                            <textarea class="form-control" id="og_image" name="og_image" rows="1"><?=$texts['og_image']?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="og_title"><u>og:title (ВРЕМЕННО НЕ ИСПОЛЬЗУЕТСЯ)</u></label>
                            <textarea disabled class="form-control disabled" id="og_title" name="og_title" rows="1"><?=$texts['og_title']?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="og_description"><u>og:description (ВРЕМЕННО НЕ ИСПОЛЬЗУЕТСЯ)</u></label>
                            <textarea disabled class="form-control" id="og_description" name="og_description" rows="2"><?=$texts['og_description']?></textarea>
                        </div>
                    </div>

                </div>
            </div>
             <div class="tab-pane fade in" id="tab2">
                <div class="editContent">
                    <p class="alert-info text-center">Используются по умолчанию на ВСЕХ страницах, если не заданы особые метатеги</p>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="meta_title"><u>meta title</u></label>
                        <textarea class="form-control" id="meta_title" name="meta_title" rows="2"><?=$texts['meta_title']?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="meta_keywords"><u>meta keywords</u></label>
                        <textarea class="form-control" id="meta_keywords" name="meta_keywords" rows="2"><?=$texts['meta_keywords']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="meta_description"><u>meta description</u></label>
                        <textarea class="form-control" id="meta_description" name="meta_description" rows="1"><?=$texts['meta_description']?></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in " id="tab3">
                <div class="editContent">
                    <h4 class="text-center">Метатеги каталога по умолчанию</h4>
                    <p class="alert-info text-center">Используются по умолчанию на странице каталога, если не заданы фильтры</p>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="catalog_all_meta_title"><u>meta title</u></label>
                        <textarea class="form-control" id="catalog_all_meta_title" name="catalog_all_meta_title" rows="2"><?=$texts['catalog_all_meta_title']?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="catalog_all_meta_keywords"><u>meta keywords</u></label>
                        <textarea class="form-control" id="catalog_all_meta_keywords" name="catalog_all_meta_keywords" rows="2"><?=$texts['catalog_all_meta_keywords']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="catalog_all_meta_description"><u>meta description</u></label>
                        <textarea class="form-control" id="catalog_all_meta_description" name="catalog_all_meta_description" rows="1"><?=$texts['catalog_all_meta_description']?></textarea>
                    </div>
                </div>
                <hr> <div class="editContent">
                    <h4 class="text-center">Метатеги каталога при отображении девайсов</h4>
                    <p class="alert-info text-center">Метатеги страницы при выборе в панели фильтров Категории >> Устройства и акссесуары</p>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="catalog_device_meta_title"><u>meta title</u></label>
                        <textarea class="form-control" id="catalog_device_meta_title" name="catalog_device_meta_title" rows="2"><?=$texts['catalog_device_meta_title']?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="catalog_device_meta_keywords"><u>meta keywords</u></label>
                        <textarea class="form-control" id="catalog_device_meta_keywords" name="catalog_device_meta_keywords" rows="2"><?=$texts['catalog_device_meta_keywords']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="catalog_device_meta_description"><u>meta description</u></label>
                        <textarea class="form-control" id="catalog_device_meta_description" name="catalog_device_meta_description" rows="1"><?=$texts['catalog_device_meta_description']?></textarea>
                    </div>
                </div>
                <hr> <div class="editContent">
                    <h4 class="text-center">Метатеги каталога при отображении жидкостей</h4>
                    <p class="alert-info text-center">Метатеги страницы при выборе в панели фильтров Категории >> Жидкости</p>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="catalog_liquid_meta_title"><u>meta title</u></label>
                        <textarea class="form-control" id="catalog_liquid_meta_title" name="catalog_liquid_meta_title" rows="2"><?=$texts['catalog_liquid_meta_title']?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="catalog_liquid_meta_keywords"><u>meta keywords</u></label>
                        <textarea class="form-control" id="catalog_liquid_meta_keywords" name="catalog_liquid_meta_keywords" rows="2"><?=$texts['catalog_liquid_meta_keywords']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="catalog_liquid_meta_description"><u>meta description</u></label>
                        <textarea class="form-control" id="catalog_liquid_meta_description" name="catalog_liquid_meta_description" rows="1"><?=$texts['catalog_liquid_meta_description']?></textarea>
                    </div>
                </div>
                <hr> <div class="editContent">
                    <h4 class="text-center">Метатеги каталога при отображении <u>БЕЗНИКОТИНОВЫХ</u> жидкостей</h4>
                    <p class="alert-info text-center">Метатеги страницы при выборе в панели фильтров Категории >> Безникотиновые жидкости</p>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="catalog_nonicoliquid_meta_title"><u>meta title</u></label>
                        <textarea class="form-control" id="catalog_nonicoliquid_meta_title" name="catalog_nonicoliquid_meta_title" rows="2"><?=$texts['catalog_nonicoliquid_meta_title']?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="catalog_nonicoliquid_meta_keywords"><u>meta keywords</u></label>
                        <textarea class="form-control" id="catalog_nonicoliquid_meta_keywords" name="catalog_nonicoliquid_meta_keywords" rows="2"><?=$texts['catalog_nonicoliquid_meta_keywords']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="catalog_nonicoliquid_meta_description"><u>meta description</u></label>
                        <textarea class="form-control" id="catalog_nonicoliquid_meta_description" name="catalog_nonicoliquid_meta_description" rows="1"><?=$texts['catalog_nonicoliquid_meta_description']?></textarea>
                    </div>
                </div>
                <hr> <div class="editContent">
                    <h4 class="text-center">Метатеги каталога при отображении производителей</h4>
                    <p class="alert-info text-center">Метатеги страницы при выборе в панели фильтров Категории >> производители</p>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="catalog_producer_meta_title"><u>meta title</u></label>
                        <textarea class="form-control" id="catalog_producer_meta_title" name="catalog_producer_meta_title" rows="2"><?=$texts['catalog_producer_meta_title']?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="catalog_producer_meta_keywords"><u>meta keywords</u></label>
                        <textarea class="form-control" id="catalog_producer_meta_keywords" name="catalog_producer_meta_keywords" rows="2"><?=$texts['catalog_producer_meta_keywords']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="catalog_producer_meta_description"><u>meta description</u></label>
                        <textarea class="form-control" id="catalog_producer_meta_description" name="catalog_producer_meta_description" rows="1"><?=$texts['catalog_producer_meta_description']?></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in " id="tab4">
                <div class="editContent">
                     <p class="alert-info text-center">Текстовый блок, следующий за списком товаров на странице каталога</p>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="catalog_default_text">Текстовка <u>по умолчанию</u></label>
                            <textarea class="form-control" id="catalog_default_text" name="catalog_default_text" rows="3"><?=$texts['catalog_all_text']?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="catalog_liquid_text">Текстовка Каталога с фильтром <u>по жидкостям</u></label>
                            <textarea class="form-control" id="catalog_liquid_text" name="catalog_liquid_text" rows="3"><?=$texts['catalog_liquid_text']?></textarea>
                        </div>
                    </div>
	                <div class="row">
		                <div class="form-group col-md-12">
			                <label for="catalog_nonicoliquid_text">Текстовка Каталога с фильтром <u>по БЕЗНИКОТИНОВЫМ жидкостям</u></label>
			                <textarea class="form-control" id="catalog_nonicoliquid_text" name="catalog_nonicoliquid_text" rows="3"><?=$texts['catalog_nonicoliquid_text']?></textarea>
		                </div>
	                </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="catalog_device_text">Текстовка Каталога с фильтром <u>по девайсам</u></label>
                            <textarea class="form-control" id="catalog_device_text" name="catalog_device_text" rows="3"><?=$texts['catalog_device_text']?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="catalog_producer_text">Текстовка Каталога с фильтром <u>по производителям</u></label>
                            <textarea class="form-control" id="catalog_producer_text" name="catalog_producer_text" rows="3"><?=$texts['catalog_producer_text']?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?/* foreach ($texts as $item) : */?><!--
            <div class="form-group">
                <label for="<?/*=$item['name']*/?>"><?/*=$textList[$item['name']]*/?></label>
                <textarea class="form-control" id="<?/*=$item['name']*/?>" name="<?/*=$item['name']*/?>" rows="2"><?/*=$item['text']*/?></textarea>
            </div>
        --><?/* endforeach;*/?>
        <div class="editContent text-center">
            <button type="submit" class="btn btn-lg btn-success">Сохранить</button>
        </div>
    </form>
</div>