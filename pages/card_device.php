<?
    $dev = $data['currentItem'];
?>

<script type="text/javascript">
    $(document).ready(function() {
        /**
         * сброс значений
         */
        $('#quantity-group #quantity').val('1');
    });
</script>

<!-- Start Shop 1-6 v1 -->
<section id="shop-1-6" class="content-block shop-1-6">
    <div class="container">
        <div class="row">
            <div class="col-md-9 pull-right" itemscope itemtype="http://schema.org/Product">
                <form class="form-inline col-md-12" id="item-form">

                    <input type="hidden" data-src="<?=$img?>" name="color" id="color" value="<?=$dev['color'][0]?>">
                    <input type="hidden" name="category" id="category" value="device">
                    <input type="hidden" name="id" id="id" value="<?=$dev['id']?>">
                    <input type="hidden" name="name" value="<?=$dev['name']?>">
                    <input type="hidden" name="url_name" value="<?=$dev['url_name']?>">
                    <input type="hidden" name="producer" value="<?=$dev['producer']?>">
                    <input type="hidden" name="weight" value="<?=$dev['weight']?>">
                    <input type="hidden" name="session_id" value="<?=$sessID?>">

                    <div class="row">
                        <div class="col-md-12 breadcrumbs">
                            <h4>
                                <ul class="filter">
                                    <li><a href="/">Главная</a></li>
                                    <li><a href="/catalog/<?=$alias['device']?>/"><?=$bAlias['device']?></a></li>
                                    <li><a href="/catalog/<?=$dev['producer']?>"><?=$catList[$dev['producer']]?></a></li>
                                    <li><?=$dev['name']?></li>
                                </ul>
                            </h4>
                        </div>
                        <div class="col-md-6 col-xs-8 col-xs-offset-2 col-md-offset-0 text-center">
                            <div class="ribbon exclusive"></div>
                            <div class="product-image">
                                <? $color = (empty($dev['color'][0])) ? '' : '_'.$dev['color'][0] ;?>
                                <? $img = safeFile("/images/devices/{$dev['url_name']}{$color}_big.jpg", 'big', 'device',$dev['producer']);?>
                                <a href="#"><img itemprop="image" src="<?=$img?>"  alt="<?=$dev['img_alt']?>"  class="img-responsive"/></a>
                            </div>
                            <div class="product-colors">
                                <? if (count($dev['color']) > 1) : ?>
                                    <ul>
                                        <?$i=0;?>
                                        <? foreach ($dev['color'] as $color) : ?>
                                            <li id="color-<?=$color?>" data-color="<?=$color?>" class="<?=($i==0)?'active':'';?>">
                                                <? $img = safeFile("/images/devices/{$dev['url_name']}_{$color}_small.jpg", 'small', 'device', $dev['producer']);?>
                                                <img src="<?=$img?>" alt="<?=$colors[$color]?>" class="img-thumbnail"/>
                                            </li>
                                            <?$i++;?>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif;?>
                            </div>
                        </div>

	                    <div class="col-md-6">
                            <div class="product-desc">
                                <div class="editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
                                    <h1 itemprop="name"><?=$dev['name']?></h1>
                                </div>
                                <div class="editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
                                    <h2><?=$catList[$dev['producer']]?></h2>
                                </div>
		                        <? printRating('device', $dev['url_name'], $dev['rating'] , $dev['ratingCnt']);?>
                                <div class="editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center">
                                    <? if ( ! empty($dev['volume'])) {
                                        echo "Мощность: {$dev['volume']} Вт";
                                    } ?>
                                </div>
                                <div class="row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	                                <? if ($promos['show_old_price_device'] == 1):?>
		                                <div id="old-price-block" class="editContent col-md-12 old-price">
			                                <span class="price-val" itemprop="price"><?=oldDevPrice($dev['price'])?></span>
			                                <span class="price-rub" itemprop="priceCurrency">RUB</span>
			                                <div class="del-line"></div>
		                                </div>
	                                <?endif;?>
	                                <div id="price-block" class="editContent col-md-12 col-xs-8 col-xs-offset-2 col-md-offset-0  text-center" data-price="<?=$dev['price']?>">
                                        <span class="price-val" itemprop="price"><?=$dev['price']?></span>
                                        <span class="price-rub" itemprop="priceCurrency">RUB</span>
                                        <input type="hidden" name="price" id="price" value="<?=$dev['price']?>">
                                    </div>
                                    <div class="form-group col-md-12 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center" id="quantity-group">
                                        <label class="sr-only" for="exampleInputAmount">Количество</label>
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-role="btn-minus" type="button">-</button>
                                        </span>
                                            <input class="form-control" id="quantity" name="quantity" placeholder="1" value='1' type="text">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-role="btn-plus" type="button">+</button>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center quantity-group">
                                        <i class="fa fa-calculator"></i>В наличии: <span class='fat' itemprop="availability"><?=$dev['quantity']?></span>
                                    </div>
                                    <div class="form-group col-md-12 col-md-offset-0 col-xs-4 col-xs-offset-4 text-center">
                                        <button type="button" data-id="<?=$dev['id']?>" data-category="device" class="btn btn-primary add-item-to-cart">В корзину</button>
                                    </div>
                                </div>
                                <div class="editContent">
                                    <h3 itemprop="description" class="text-left"><?=$dev['remark']?></h3>
                                </div>
                                <div class="editContent">
                                    <p><?=$dev['description']?></p>
                                </div>
                                <ul class="fa-ul" id="desc-row">
                                    <li><i class="fa-li fa fa-th-list"></i>Категория: <a href="/catalog/<?=$alias['device']?>/<?=$dev['category']?>"><?=$catList[$dev['category']]?></a></li>
                                    <?/*<li><i class="fa-li fa fa-tags"></i>Теги: <?=writeTagsLinks($liquid['tags'])?></li>*/?>
                                </ul>

                            </div><!-- /.product-desc -->
                        </div>
                    </div><!-- /.row -->
                </form>
                <div class="col-md-12">
                    <?php include "tpl/commentblock.php";?>
                </div>
            </div><!-- /.main-col -->
            <div class="sidebar col-md-3 col-sm-12 col-xs-12 pull-left">
                <?php include "tpl/widgets.php"; ?>
            </div><!-- /.sidebar -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!--// End Shop 1-6 v1 -->