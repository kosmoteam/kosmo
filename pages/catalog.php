<?php
	// получаем фильтры
	//$tag = ( !empty($_GET['tag'])) ? $_GET['tag'] : FALSE;

    // получаем тип итемов по которым отсортирован каталог
    $cItemType = FALSE;

    if($data['area'] AND isset($aliasBack[$data['area']])){
        $cItemType = $aliasBack[$data['area']];
    }

    // получаем категорию
	$category = FALSE;
    if ($data['filtredBy'] == 'category' AND !empty($data['category'])) {
		$category = $data['category'];
	}

	// получаем подмножества товаров
	if ($data['filtredBy'] == 'category' OR $data['filtredBy'] == 'area') {
		if ($cItemType == 'device' OR $cItemType == 'producer' OR $cItemType == 'all') {
			$devices = getDevices($category);
        }
		if ($cItemType == 'liquid' OR $cItemType == 'producer' OR $cItemType == 'all') {
			$liquids = getLiquids($category);
		}
        if ($cItemType == 'nonicoliquid') {
			$liquids = getNoNicoLiquids();
		}
	}

	// перемешиваем товары при нулевом фильтре
	if ($data['filtredBy'] == 'area') {
        if(!empty($devices)){
	        shuffle($devices);
        }
		if(!empty($liquids)){
			shuffle($liquids);
		}
    }
?>


<!-- Start Shop 1-5 v2 -->
<section id="shop-1-5" class="content-block shop-1-5">
    <div class="container">
        <div class="row">
            <div class="col-md-9 pull-right">
	            <? if ($promos['cat_head_banner_enabled'] == 1):?>
		            <div class="col-md-12 top-promo">
			            <a href="<?=$promos['cat_head_banner_url']?>" target="_blank">
				            <img src="/img/ban/<?=$promos['cat_head_banner_img']?>" class="ban-top col-md-12" alt=""/>
			            </a>
		            </div>
	            <?endif;?>
                <div class="col-md-12 breadcrumbs">
                    <h4>
                        <ul class="filter">
                            <li><a href="/">Главная</a></li>
                            <? if ($cItemType AND ! $category AND isset($bAlias[$cItemType])) :?>
                                <li><?=$bAlias[$cItemType]?></li>
                            <? elseif ($cItemType AND $category) :?>
                                <li><a href="/catalog/<?=$alias[$cItemType]?>"><?=$bAlias[$cItemType]?></a></li>
                                <li><?=$catList[$category]?></li>
                            <? endif; ?>
                        </ul>
                    </h4>
                </div>
                <h2 class="catalog-h1 text-center">Интернет-магазин премиальных и элитных жидкостей для электронных сигарет и испарителей с доставкой по Москве и России</h2>
                <? if ( !empty($category) ): ?>
                    <div class="row">
                        <div class="underlined-title">
                            <h2>Поиск по <?=$cAlias[$cItemType]?> "<?=$catList[$category]?>"</h2>
                            <hr>
                        </div>
                    </div>
                <? endif; ?>

                <?/* if ( !empty($tag) ): ?>
                    <div class="row">
                        <div class="underlined-title">
                            <div class="editContent">
                                <h2>Поиск по тегу <?=$tag?></h2>
                            </div>
                            <hr>
                        </div>
                    </div>
                <? endif; */?>
				 <? if (!empty($liquids)): ?>
                    <div class="row">
                        <? foreach ($liquids as $liquid) : ?>
                            <? $iurl = "/catalog/{$alias['liquid']}/{$liquid['producer']}/{$liquid['url_name']}";?>
	                        <? $img = safeFile("/images/liquids/{$liquid['url_name']}_small.jpg", 'small', 'liquid', $liquid['producer']);?>

                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                <div class="product clearfix">
                                    <div class="product-image">
                                        <a href="<?=$iurl?>"><img src="<?=$img?>" class="img-responsive" alt="<?=$liquid['img_alt']?>"/></a>
                                        <div class="product-overlay">
                                            <a href="<?=$iurl?>" class="gallery-zoom" data-lightbox="ajax"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-desc">
	                                    <div class="editContent product-name">
                                            <h5><a href="<?=$iurl?>"><?=$liquid['name']?></a></h5>
                                        </div>
                                        <div class="editContent producer-name">
                                            <h6><a href="/catalog/all/<?=trim($liquid['producer'])?>"><?=$catList[$liquid['producer']]?></a></h6>
                                        </div>
                                        <div class="editContent">
                                            Никотин: <?=str_replace(",",", ",$liquid['nicotine']);?> мг
                                        </div>
                                        <div class="editContent">
                                            Объем: <?=$liquid['volume1']?><?=(!empty($liquid['volume2'])) ? ' и '.$liquid['volume2'] : ''?> мл
                                        </div>
                                        <div class="editContent">
                                            <b>В наличии!</b>
                                        </div>
	                                    <? if ($promos['show_old_price_liquid'] == 1):?>
		                                    <div class="editContent catalog-old-price">
			                                    <h5>
				                                    <?=oldLiqPrice($liquid['price1'])?>
				                                    <span class="price-rub"></span>
			                                    </h5>
			                                    <div class="del-line"></div>
		                                    </div>
	                                    <?endif;?>
	                                    <div class="editContent catalog-price">
                                            <h5>
	                                            <?=$liquid['price1']?><?=(!empty($liquid['price2'])) ? ' - '.$liquid['price2'] : ''?><span class="price-rub"></span>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div><!-- /.row -->
                <? endif; ?>
	            <? if (!empty($devices)): ?>
		            <div class="row">
			            <? $i=0; ?>
			            <? foreach ($devices as $dev) : ?>
				            <? $iurl = "/catalog/{$alias['device']}/{$dev['producer']}/{$dev['url_name']}";?>
				            <? $color = (empty($dev['color'][0])) ? '' : '_'.$dev['color'][0] ;?>
				            <? $img = safeFile("/images/devices/{$dev['url_name']}{$color}_small.jpg", 'small', 'device', $dev['producer']);?>

				            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
					            <div class="product clearfix">
						            <div class="ribbon sale"></div><!-- Ribbon -->
						            <div class="product-image">
							            <a href="<?=$iurl?>"><img src="<?=$img?>" alt="<?=$dev['img_alt']?>" class="img-responsive"/></a>
							            <div class="product-overlay">
								            <a href="<?=$iurl?>" class="gallery-zoom" data-lightbox="ajax"><i class="fa fa-search"></i></a>
							            </div>
						            </div>
						            <div class="product-desc">
							            <div class="editContent product-name">
								            <h5><a href="<?=$iurl?>"><?=$dev['name']?></a></h5>
							            </div>
							            <div class="editContent">
								            <h6><a href="/catalog/all/<?=trim($dev['producer'])?>"><?=$catList[trim($dev['producer'])]?></a></h6>
							            </div>
							            <div class="editContent">
								            <? if ( ! empty($dev['volume'])) {
									            echo "Мощность: {$dev['volume']} Вт";
								            } else {
									            echo "-";
								            } ?>
							            </div>
							            <div class="editContent">
								            <b>В наличии!</b>
							            </div>
							            <? if ($promos['show_old_price_device'] == 1):?>
								            <div class="editContent catalog-old-price">
									            <h5>
										            <?=oldDevPrice($dev['price'])?>
										            <span class="price-rub"></span>
									            </h5>
									            <div class="del-line"></div>
								            </div>
							            <?endif;?>
							            <div class="editContent catalog-price">
								            <h5><?=$dev['price']?><span class="price-rub"></span></h5>
							            </div>

							            <div class="editContent">
								            <?//=( ! empty($dev['description'])) ? mb_substr($dev['description'],0,126,'UTF-8').'...' : '' ;?>
							            </div>

						            </div>
					            </div>
				            </div>
				            <? $i++; ?>
			            <? endforeach; ?>
		            </div><!-- /.row -->
	            <? endif; ?>
	            <div class="row seo-catalog">
		            <div class="col-md-12">
			            <div class="editContent">
                            <?=htmlspecialchars_decode($seoElements['meta_catalog_text'])?>
			            </div>
		            </div>
	            </div>
            </div><!-- /.main-col -->
            <div class="sidebar col-md-3 col-sm-12 col-xs-12 pull-left">
                <?php include "tpl/widgets.php"; ?>
            </div><!-- /.sidebar -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!--// End Shop 1-5 v2 -->
