	        <!-- Start Footer 1-1 -->
	        <section class="content-block-nopad bg-deepocean">
	            <div class="container footer-1-1">
	                <div class="row logo-footer">
	                    <div class="col-sm-5">
	                        <div class="row logo-area">
		                        <div class="col-sm-3">
			                        <img src="/images/brand/logo.png" class="" alt="Kosmos Vape Logo"/>
		                        </div>
		                        <div class="col-sm-9">
			                        <div class="editContent">
				                        <h3>Kosmos Vape</h3>
			                        </div>
			                        <div class="editContent">
				                        <h5>премиальные жидкости</h5>
			                        </div>
	                                <div class="editContent">
				                        <h5>для электронных испарителей</h5>
			                        </div>

		                        </div>
	                        </div>

	                    </div>
	                    <div class="col-sm-4">
		                    <div class="editContent">
			                    <ul class="contact-info">
				                    <li><span class="fa fa-map-marker"></span>ИП Антон Сергеевич Постюшков - Kosmos Vape, Пресненская набережная 6, стр. 2, Москва.</li>
				                    <li><span class="fa fa-phone"></span>+7 916 448 75 71</li>
				                    <li><span class="fa fa-envelope"></span><a href="mailto:kosmosvape@gmail.com">kosmosvape@gmail.com</a> </li>
			                    </ul>
		                    </div>
	                    </div>
	                    <div class="col-sm-2 col-sm-offset-1">
	                        <div class="row">
	                            <div class="col-md-12">
	                                <div class="editContent">
	                                    <h4>Ссылки</h4>
	                                </div>
	                                <div class="editContent">
	                                    <ul>
	                                        <li><a rel="nofollow" href="/">Главная</a></li>
	                                        <li><a rel="nofollow" href="/catalog">Каталог</a></li>
	                                        <li><a rel="nofollow" href="/order">Доставка и контакты</a></li>
	                                        <li><a rel="nofollow" href="/cart">Корзина</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                </div><!-- /.row -->
	            </div><!-- /.container -->
	        </section>
	        <!--// END Footer 1-1 -->
	        <?include "pages/tpl/modals.php";?>
        </div>

        <img src="//code.directadvert.ru/track/267991.gif" width="1" height="1" alt="directadvert" />
        <!-- Scripts at the end... you know the score! -->
        <!-- Core Scripts (Do not remove) -->
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap-hover-dropdown.min.js"></script>

        <!-- Vendor Scripts (Feel free to remove any that you aren't using in your final export) -->
        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="/js/headroom.js"></script><!-- Header 1 -->
        <script type="text/javascript" src="/js/jquery.headroom.js"></script><!-- Header 1 -->
        <script type="text/javascript" src="/js/general.js"></script>
        <script type="text/javascript" id="dsq-count-scr" src="//kosmosvape.disqus.com/count.js" async></script>
        <script type="text/javascript" src="http://platform.checkout.ru/cop/cop.js?ver=9.8"></script>

		<script type="text/javascript">
			var copFormId = 'orderForm';
		</script>
        <!--ЕСЛИ включено в админке, показываем диалог с предложением подписки-->
        <? if ( ! empty($promos['subscriptions_enabled'])) : ?>
			<script type="text/javascript">
				$(document).ready(function() {
					// кука, отвечающая за показ юзеру блока с предложением о подписке
					var hideSubs = getCookie('hide_subs');
					if (hideSubs === undefined) {
						setTimeout(function () {
							$('#subModal').modal('show');
						}, 5000);
					}
				});
			</script>
        <? endif; // subscriptions?>

        <? if ($mode != 'dev') : ?>
            <script type='text/javascript'>
                (function(){ var widget_id = '1IVUGlsJws';var d=document;var w=window;function l(){
                    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
            <!-- {/literal} END JIVOSITE CODE -->
        <? endif; // dev mode switching off scripts?>
    </body>
</html>

