<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<!--модальное окно подписки-->
<div class="modal fade" id="subModal" tabindex="-1" role="dialog" aria-labelledby="subModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="subModalLabel">Мы готовим полезную рассылку!</h4>
			</div>
			<div class="modal-body">
				<div class="editContent message">
					Акции, скидки, распродажи и другие полезные новости.
				</div>
				<div class="editContent text-center email-block">
					<input name="subEmail" id="subEmail" placeholder="Email" class="form-control" type="email" value="">
				</div>
				<div class="editContent text-center">
					<button class="btn btn-info" type="button" id="subscribeBtn">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						Подписаться!
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
