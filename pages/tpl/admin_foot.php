            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	            <div class="modal-dialog" role="document">
		            <div class="modal-content">
			            <div class="modal-header">
				            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				            <h4 class="modal-title" id="myModalLabel"></h4>
			            </div>
			            <div class="modal-body"></div>
		            </div>
	            </div>
            </div>
        </div>
		<!-- Scripts at the end... you know the score! -->
        <!-- Core Scripts (Do not remove) -->
        <!--<script type="text/javascript" src="/js/fileinput/plugins/canvas-to-blob.min.js"></script>-->
        <!--<script type="text/javascript" src="/js/fileinput/fileinput.js"></script>-->
        <!--<script type="text/javascript" src="/js/fileinput/themes/fa.js"></script>-->
        <!--<script type="text/javascript" src="/js/fileinput/locales/ru.js"></script>-->

        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap-hover-dropdown.min.js"></script>

        <!-- Vendor Scripts (Feel free to remove any that you aren't using in your final export) -->
        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="/js/headroom.js"></script><!-- Header 1 -->
        <script type="text/javascript" src="/js/jquery.headroom.js"></script>
        <script type="text/javascript" src="/js/sendmail.js"></script><!-- Contact Form -->
        <script type="text/javascript" src="/js/general.js"></script>
    </body>
</html>

