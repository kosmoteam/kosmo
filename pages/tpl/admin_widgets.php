<div class="widget admin-widget">
    <div class="editContent text-center">
        <img src="/images/brand/logo.png" class="brand-img img-responsive" alt="Kosmos Vape Logo"/>
    </div>
    <div class="editContent text-center">
        <h4>Админка</h4>
    </div>
    <hr>
    <ul class="nav nav-pills nav-stacked cat-list text-left">
        <li id=''  ><a href="/catalog">Вернуться к сайту</a></li>
        <li ><hr></li>
        <li id='meta' class="<?=($area == 'meta') ? 'active':'';?>"><a href="/admin/meta">Метаданные</a></li>
	    <li id='meta' class="<?=($area == 'promo') ? 'active':'';?>"><a href="/admin/promo">Промо</a></li>
	    <li id='goods' class="<?=($area == 'goods') ? 'active':'';?>"><a href="/admin/goods">Товары</a></li>
        <li id='orders' class="<?=($area == 'orders') ? 'active':'';?>"><a href="/admin/orders">Заказы</a></li>
        <li id='categories' class="<?=($area == 'categories') ? 'active':'';?>"><a href="/admin/categories">Категории</a></li>
    </ul>
</div>
