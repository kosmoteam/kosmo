<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title><?=$seoElements['meta_title']?></title>
	    <meta name="google-site-verification" content="2TalYCZ_uWEsbv7_3iWaHk19kzGr2AVod3Apul5t4M0" />
	    <meta name='yandex-verification' content='7082d8bdf8f0008c' />
	    <meta http-equiv= "keywords" content= "<?=$seoElements['meta_keywords']?>"/>
        <meta name="description" content="<?=$seoElements['meta_description']?>"/>
        <meta property="place:location:latitude" content="13.062616"/>
        <meta property="business:contact_data:locality" content="Москва"/>
        <meta property="business:contact_data:country_name" content="Россия"/>
        <meta property="business:contact_data:email" content="kosmosvape@gmail.com"/>
        <meta property="business:contact_data:phone_number" content="+7 916 448-75-71"/>
        <meta property="business:contact_data:website" content="http://kosmosvape.ru"/>
        <meta property="og:type"        content="website"/>
        <meta property="og:image"       content="<?=$seoElements['og_image']?>"/>
        <meta property="og:title"       content="<?=$seoElements['meta_title']?><?//=$seoElements['og_title']?>"/>
        <meta property="og:description" content="<?=$seoElements['meta_description']?><?//=$seoElements['og_description']?>"/>
        <meta property="og:url"         content="<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>"/>
        <meta property="og:site_name"   content="KosmosVape"/>


        <link rel="shortcut icon" href="/images/ico/favicon.png" type="image/x-icon"/>
        <link rel="icon" href="/images/ico/favicon.png" type="image/x-icon"/>

	    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/css/style.css" rel="stylesheet"/>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!--[if lt IE 9]>
	        <script src="/js/html5shiv.js"></script>
	        <script src="/js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
        <? if ($mode != 'dev') : ?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
                !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                        document,'script','https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '1017887848281196');
                fbq('track', "PageView");</script>
            <noscript><img height="1" width="1" style="display:none"
                           src="https://www.facebook.com/tr?id=1017887848281196&ev=PageView&noscript=1"
                /></noscript>
            <!-- End Facebook Pixel Code -->

            <!-- Put this script tag to the <head> of your page -->
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
        <? endif; // dev mode switching off scripts?>
    </head>
    <body>
        <? if ($mode != 'dev') : ?>
            <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KVGV6L"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <script type="text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-KVGV6L');</script>
            <!-- End Google Tag Manager -->

            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter35660425 = new Ya.Metrika({
                                id:35660425,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true,
                                trackHash:true,
                                ecommerce:"dataLayer"
                            });
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/35660425" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->

            <!-- Google Analytics -->
            <script type="text/javascript">
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-74325230-1', 'auto');
                ga('send', 'pageview');
            </script>
            <!-- End Google Analytics -->

            <div id="fb-root"></div>
            <script type="text/javascript">(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.6&appId=111758762566526";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div id="page" class="page">

            <script type="text/javascript">
                VK.init({apiId: 5444339, onlyWidgets: true});
            </script>
        <? endif; // dev mode switching off scripts?>