<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kosmos Vape: АДМИНКА</title>
        <meta name="description" content="Продажа премиальных, элитных и импортных жидкостей для электронных испарителей по доступным ценам в Москве с доставкой.">
        <meta property="place:location:latitude" content="13.062616"/>
        <meta property="business:contact_data:locality" content="Москва"/>
        <meta property="business:contact_data:country_name" content="Россия"/>
        <meta property="business:contact_data:email" content="kosmosvape@gmail.com"/>
        <meta property="business:contact_data:phone_number" content="+7 916 448-75-71"/>
        <meta property="business:contact_data:website" content="http://kosmosvape.ru"/>
        <meta name='yandex-verification' content='7082d8bdf8f0008c' />

        <link rel="shortcut icon" href="/images/ico/favicon.png" type="image/x-icon">
        <link rel="icon" href="/images/ico/favicon.png" type="image/x-icon">

	    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/admin.css" rel="stylesheet"/>
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	    <!--[if lt IE 9]>
		    <script src="/js/html5shiv.js"></script>
		    <script src="/js/respond.min.js"></script>
	    <![endif]-->
	    <!--<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>-->
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="page" class="page admin-page">
