<?
	$categories = getCategories();

    $data['activeWidItem'] = (empty($data['category'])) ? 'all' : $data['category']; //  отмечаем в виджете нужный элемент

	if ( $data['filtredBy'] == 'area'){
		$data['activeWidItem'] = $data['area'];
	}
?>
<!-- HEADER 1 -->
<header id="header-1">
	<!-- Navbar -->
	<nav class="main-nav navbar-fixed-top headroom headroom--pinned">
		<div class="container">
			<!-- Brand and toggle -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-desc">
                        <a rel="nofollow" href="/cart"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span id="cartCount"><?=$cartItemsCount;?></span></a>
                        <span class="icon-text">
                            КАТЕГОРИИ
                        </span>
                    </span>
					<span class="glyphicon glyphicon-menu-hamburger"></span>
				</button>
				<div class="logo-header">
					<img src="/images/brand/logo.png" class="brand-img img-responsive" alt="Kosmos Vape Logo"/>
					<div class="logo">
						<a href="/">Kosmos Vape</a>
						<a class="logo-link" href="/">премиальные жидкости<br/>&nbsp;для электронных сигарет</a>
					</div>
				</div>
			</div>
			<!-- Navigation -->
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li id='instagram' class="nav-item">
						<a class='instagram-link' target="_blank" href="https://www.instagram.com/kosmosvapeshop/"></a>
					</li><li id='home' class="nav-item <?=($data['activeMenuItem'] == 'home')?'active':'';?>">
						<a rel="nofollow" href="/">Главная</a>
					</li>
					<li id='catalog' class="nav-item <?=($data['activeMenuItem'] == 'catalog')?'active':'';?>">
						<a rel="nofollow" href="/catalog">Каталог</a>
					</li>
					<li class="nav-item dropdown mobile-only">
						<a rel="nofollow" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
							Производители<i class="glyphicon glyphicon-triangle-right"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a rel="nofollow" href="/catalog">Все производители</a></li>
							<li role="separator" class="divider"></li>
							<? foreach ($categories['producer'] as $key => $val ) : ?>
								<li id='<?=$key?>'><a rel="nofollow" href="/catalog/all/<?=$key?>"><?=$val?></a></li>
							<? endforeach; ?>
						</ul>
					</li>
					<li class="nav-item dropdown mobile-only">
						<a rel="nofollow" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
							Жидкости<i class="glyphicon glyphicon-triangle-right"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a rel="nofollow" href="/catalog/<?=$alias['liquid']?>/">Все жидкости</a></li>
							<li role="separator" class="divider"></li>
							<? foreach ($categories['liquid'] as $key => $val ) : ?>
								<li id='<?=$key?>' ><a href="/catalog/all/<?=$key?>"><?=$val?></a></li>
							<? endforeach; ?>
						</ul>
					</li>
					<li class="nav-item dropdown mobile-only">
						<a rel="nofollow" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
							Устройства и акссесуары<i class="glyphicon glyphicon-triangle-right"></i>
						</a>
						<ul class="dropdown-menu">
							<li> <a rel="nofollow" href="/catalog/<?=$alias['device']?>/">Все Устройства</a></li>
							<li role="separator" class="divider"></li>
							<? foreach ($categories['device'] as $key => $val ) : ?>
								<li id='<?=$key?>' class="<?=($data['activeWidItem'] == $key)?'active':'';?>"><a href="/catalog/all/<?=$key?>"><?=$val?></a></li>
							<? endforeach; ?>
						</ul>
					</li>

					<li id='order' class="nav-item <?=($data['activeMenuItem'] == 'order')?'active':'';?>">
						<a rel="nofollow" href="/order">Доставка заказа
						</a>
					</li>
					<li id='order-status' class="nav-item <?=($data['activeMenuItem'] == 'checkorder')?'active':'';?>">
						<a rel="nofollow" href="/checkorder">Статус</a>
					</li>
					<li id='cart' class="nav-item <?=($data['activeMenuItem'] == 'cart')?'active':'';?>">
						<a rel="nofollow" href="/cart"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span id="cartCount"><?=$cartItemsCount;?></span></a>
						<div id="done-arrow"></div>
					</li>

                    <li role="separator" class="divider"></li>

					<li  id='info' >
						<ul class="head-contact-info">
                            <li id='contact-phone'><span class="fa fa-phone"></span>+7 (916) 448 75 71</li>
                            <li>Москва</li>
                            <li>Пресненская набережная, д. 6, стр. 2</li>
                            <li>Пон. - Пт., с 10:00 до 20:00</li>
                        </ul>
					</li>
				</ul>
			</div>
			<!--// End Navigation -->
		</div>
		<!--// End Container -->
	</nav>
	<!--// End Navbar -->

</header>
<!--// END HEADER 1 -->


