<?
    // массив категорий
    $categories = getCategories();

    $data['activeWidItem'] = (empty($data['category'])) ? 'all' : $data['category']; //  отмечаем в виджете нужный элемент

    if ( $data['filtredBy'] == 'area'){
        $data['activeWidItem'] = $data['area'];
    }
?>
<div class="widget mobile-hide">
    <div class="editContent">
        <h4>Производители</h4>
    </div>
    <ul class="nav nav-pills nav-stacked cat-list">
        <? foreach ($categories['producer'] as $key => $val ) : ?>
            <li id='wid-<?=$key?>' class="<?=($data['activeWidItem'] == $key)?'active':'';?>"><a rel="nofollow" href="/catalog/<?=$alias['producer']?>/<?=$key?>"><?=$val?></a></li>
        <? endforeach; ?>
    </ul>
</div>

<div class="widget mobile-hide">
    <div class="editContent">
        <h4>Категории</h4>
    </div>
    <ul class="nav nav-pills nav-stacked cat-list">
        <li id='all' class="<?=($data['activeWidItem'] == 'all')?'active':'';?>"><a rel="nofollow" href="/catalog/all">Все</a></li>

        <? if( count($categories['liquid'] > 0)) : ?>
            <li id='liquids' class="<?=($data['activeWidItem'] == $alias['nonicoliquid']) ? 'active':'';?>">
                <a rel="nofollow" href="/catalog/<?=$alias['nonicoliquid']?>">Безникотиновые жидкости</a>
            </li>
            <li id='liquids' class="<?=($data['activeWidItem'] == $alias['liquid']) ? 'active':'';?>">
                <a rel="nofollow" href="/catalog/<?=$alias['liquid']?>">Жидкости</a>
                <ul class="nav nav-pills nav-stacked">
                    <? foreach ($categories['liquid'] as $key => $val ) : ?>
                        <li id='wid-<?=$key?>' class="<?=($data['activeWidItem'] == $key)?'active':'';?>"><a rel="nofollow"  href="/catalog/<?=$alias['liquid']?>/<?=$key?>"><?=$val?></a></li>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endif; ?>

        <? if( count($categories['device'] > 0)) : ?>
            <li id='devices' class="<?=($data['activeWidItem'] == $alias['device']) ? 'active':'';?>">
                <a rel="nofollow" href="/catalog/<?=$alias['device']?>">Устройства и акссесуары</a>
                <ul class="nav nav-pills nav-stacked">
                    <? foreach ($categories['device'] as $key => $val ) : ?>
                        <li id='wid-<?=$key?>' class="<?=($data['activeWidItem'] == $key)?'active':'';?>"><a rel="nofollow" href="/catalog/<?=$alias['device']?>/<?=$key?>"><?=$val?></a></li>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endif; ?>
    </ul>
</div>

<div class="widget clearfix">
    <h4>Популярные</h4>
    <div id="popular-items">
        <div class="sml-item">
            <div class="entry-image">
                <a href="/catalog/<?=$alias['device']?>/aeronaut/aeronaut_ss"><img src="/images/devices/aeronaut_ss_small.jpg" alt="Aeronaut SS"/></a>
            </div>
            <div class="editContent">
                <h4><a href="/catalog/<?=$alias['device']?>/aeronaut/aeronaut_ss">Aeronaut SS</a></h4>
            </div>
            <div class="editContent widget-price">
                <h5>4500<span class="price-rub"></span></h5>
            </div>
        </div>
        <div class="sml-item">
            <div class="entry-image">
                <a href="/catalog/<?=$alias['liquid']?>/lovela/the_bees_knees"><img src="/images/liquids/the_bees_knees_small.jpg" alt="The Bee's Knees"/></a>
            </div>
            <div class="editContent">
                <h4><a href="/catalog/<?=$alias['liquid']?>/lovela/the_bees_knees">The Bee's Knees</a></h4>
            </div>
            <div class="editContent widget-price">
                <h5>1800<span class="price-rub"></span></h5>
            </div>
        </div>
        <div class="sml-item">
            <div class="entry-image">
                <a href="/catalog/<?=$alias['liquid']?>/crft_labs/lime_cola"><img src="/images/liquids/lime_cola_small.jpg" alt="Lime Cola"/></a>
            </div>
            <div class="editContent">
                <h4><a href="/catalog/<?=$alias['liquid']?>/crft_labs/lime_cola">Lime Cola</a></h4>
            </div>
            <div class="editContent widget-price">
                <h5>1500<span class="price-rub"></span></h5>
            </div>
        </div>
    </div><!-- /.popular-items -->

</div><!-- /.widget -->