<? if ($mode != 'dev') : ?>
    <!--СОЦИАЛЬНЫЕ КНОПКИ-->
    <div class="col-md-12 text-center social-block">
        <script type="text/javascript">(function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();</script>
        <div class="pluso" data-background="transparent" data-options="big,square,line,horizontal,counter,theme=01" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
    </div>

    <!--ТАБЫ С ФОРМАМИ КОММЕНТИРОВАНИЯ-->
    <ul class="nav nav-tabs text-center" role="tablist" id="myTab">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">ВКонтакте</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Facebook</a></li>
        <li><a href="#tab3" role="tab" data-toggle="tab">Disqus</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
            <div class="row">
                <div class="col-md-12 vk-comment-form text-center">
                    <!-- Put this div tag to the place, where the Comments block will be -->
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                        VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                    </script>
                </div>
            </div>
        </div>
	    <div class="tab-pane fade in" id="tab2">
            <div class="row">
                <div class="col-md-12 fb-comment-form text-center">
                    <div class="fb-comments" data-href="http://kosmosvape.ru/" data-width="400" data-numposts="5"></div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade in" id="tab3">
            <div id="disqus_thread"></div>
            <script type="text/javascript">
                /**
                 * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                 * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                 */
                var disqus_config = function () {
                    this.page.url = '<?=$data['url'] ?>'; // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = '<?=$data['pageident']?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = '//kosmosvape.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
        </div>
    </div>
<? endif; // dev mode switching off scripts?>