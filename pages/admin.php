<?
    $categories = getCategories();
?>
<div class="page-container">
    <section id="shop-1-5" class="content-block shop-1-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 pull-right main-bar">
                    <?php include "admin/{$area}.php"; ?>
                </div>
                <div class="sidebar col-md-2 col-sm-12 col-xs-12 pull-left">
                    <?php include "tpl/admin_widgets.php"; ?>
                </div><!-- /.sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
</div>