<section id="shop-1-6" class="content-block shop-1-6">
    <div class="container">
        <div class="row">
            <div class="col-md-9 pull-right text-center">
                <?php
                    if ( !empty($_POST) ) {
                        // анализируем полученные от чекаута данные
                        $post = (!empty($_POST)) ? makeSafeArray($_POST) : FALSE;

                        // формируем поля для хранения ответа
                        //TODO подумать что из этих значений стоит поместить в отдельные поля
                        $orderId    = $data['orid'];    // ID заказа в нашей БД
                        $chOrderId  = $post['orderId']; // ID заказа в системе чекаута
                        $chOrderBody = ''; //текст, содержащий полный ответ
                        foreach ($post AS $k => $v){
                            $chOrderBody .= $k. ': '. $v . "\n";
                        }


                        // получаем текущий заказ
                        $orderCur = getOrder($orderId);

                        // обновляем заказ в нашей БД
                        $sql = "UPDATE orders SET ch_order = '{$chOrderId}', ch_answer = '{$chOrderBody}' WHERE id = '{$orderId}'";
                        mysql_query($sql);

                        echo "<h3>Заказ успешно сформирован и передан у службу доставки</h3>";
                        echo "<h4>Номер вашего заказа <b>{$chOrderId}</b>. Сохраните его для дальнейшего общения с менеджером. Проверить статус вашего заказа можно на странице <a href='/checkorder'>Статус заказа</a>.<br/>В самое ближайшее время с Вами свяжутся указанным Вами способом.<br/> Спасибо, что выбрали нас!</h4>";


                        /************* формируем сообщение для заказчика с самовывозом */
                        //TODO внести детали заказа
                        $message['user'] = "<h1>Новый заказ с сайта kosmosvape.ru</h1>";
                        $message['user'] .= "Ваш заказ успешно принят магазином и передан в службу доставки. В самое ближайшее время с Вами свяжется их представитель.<br/>";
                        $message['user'] .= "Номер вашего заказа <b>{$chOrderId}</b>. Сохраните его для дальнейшего общения с менеджером.<br/>";
                        $message['user'] .= "Проверить статус вашего заказа можно на странице <a href='http://kosmosvape.ru/checkorder'>http://kosmosvape.ru/checkorder</a><br/>";
                        $message['user'] .= "Если у Вас возникли вопросы, свяжитесь с менеджером магазина, мы будем рады помочь Вам";
                        $message['user'] .= "<br/><hr>";
                        //$message['user'] .= $orderCur['textform'];
                        $message['user'] .= "Спасибо что выбрали нас!<br/>";

                        $subject = $name . " - новый заказ с kosmosvape.ru";
                        $Reply = 'kosmosvape@gmail.com';
                        $from = 'kosmosvape@gmail.com';

                        // отправляем письмо юзеру
                        $headers = "from: $from <$Reply>\nReply-To: $Reply \nContent-type: text/html";
                        $to = $orderCur['email'];
                        $mail = mail($to, $subject, $message['user'], $headers);

                        // отправляем в checkout Данные нашего заказа
                        $data = array(
                                "apiKey" => $checkoutKey,
                                "order" => array(
                                        "shopOrderId" => $orderId
                                )
                        );

                        // подключаем httpful.phar
                        require("core/httpful.phar");
                        $url = "http://platform.checkout.ru/service/order/".$chOrderId;
                        $response = \Httpful\Request::post($url)
                                ->sendsJson()
                                ->body(json_encode($data))
                                ->sendIt();
                        if ($response->hasErrors()) {
                            $headers = explode("\r\n", $response->raw_headers);
                            $header = explode(" ", $headers[0]);
                            $response = str_replace($header[0], "", $headers[0]);
                        } else {
                            $response = $response->body;
                        }
                    } else {
                        echo '<div class="text-center">';
                            echo "<h1>Это системная страница</h1>";
                            echo "<h2>Вас тут быть не должно</h2>";
                            echo "<img height='200px' src='/images/hacker.png'/>";
                        echo '</div>';
                    }
                ?>
            </div><!-- /.main-col -->
            <div class="sidebar col-md-3 col-sm-12 col-xs-12 pull-left">
                <?include "tpl/widgets.php"; ?>
            </div><!-- /.sidebar -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!--// End Shop 1-6 v1 -->

