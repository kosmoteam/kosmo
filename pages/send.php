<?php
	$post = (!empty($_POST)) ? makeSafeArray($_POST) : false;
	if ($post) {
		// получаем списки товаров в корзине
		$items = getCartItems($post['cartId']);
		$subject = stripslashes($post['name']) . " - новый заказ с kosmosvape.ru";
		$name = stripslashes($post['name']);
		$cartTotal = $post['cartTotal'];
		$cartCount = $post['cartCount'];

		if (!filter_var(trim($post['email']), FILTER_VALIDATE_EMAIL)) {
			echo '<div class="alert alert-danger" role="alert">Неверный формат почтового ящика. Должен быть name@domain.com</div>';
			exit();
		}
		$email = trim($post['email']);
		$message = "<h1>Новый заказ с сайта kosmosvape.ru</h1>";
		$message .= "Заказчик: {$post['name']}<br/>";
		$message .= "Контакты: {$post['phone']} {$post['email']} <br/>";
		$message .= "Сообщение: {$post['comments']}<br/><hr>";
		$message .= "<h2>Содержимое заказа:</h2>";
		$message .= "<center><table border=1px cellspacing=0 cellpadding=4 style='padding:-1px;text-align:left'>";
			$message .= "<thead><tr>";
			$message .= "<th>ID</th>";
			$message .= "<th>ТИП</th>";
			$message .= "<th>ТОВАР</th>";
			$message .= "<th>ПРОИЗВОДИТЕЛЬ</th>";
			$message .= "<th>ЦВЕТ</th>";
			$message .= "<th>ЦЕНА</th>";
			$message .= "<th>КОЛИЧЕСТВО</th>";
			$message .= "<th>ВСЕГО</th>";
			$message .= "</tr></thead>";
			foreach ($items as $item) {
				$message .= "<tr>";
					$type = ($item['category'] == 'liquid') ? 'Жидкость' : "Устройство";
					$color = ($item['category'] == 'device' and isset($colors[$item['color']])) ? $colors[$item['color']] : "-";
					$message .= "<td>{$item['item_id']}</td>";
					$message .= "<td>{$type}</td>";
					$message .= "<td>{$item['name']}</td>";
					$message .= "<td>{$catList[$item['producer']]}</td>";
					$message .= "<td>{$color}</td>";
					$message .= "<td>{$item['price']}р.</td>";
					$message .= "<td>{$item['quantity']} шт.</td>";
					$message .= "<td>".$item['quantity']*$item['price']."р.</td>";
				$message .= "</tr>";
			}
			$message .= "<tr><td colspan=6>общее кол-во товаров:</td><td align=center colspan=2><b>".$cartCount.' шт.</b></td></tr>';
			$message .= "<tr><td colspan=6>общая сумма заказа:</td><td align=center colspan=2><b>".$cartTotal.' р.</b></td></tr>';
			$message .= "<tr><td colspan=6>Вид доставки:</td><td align=center colspan=2><b>".$delRus[$post['deliveryType']].'</b></td></tr>';
		$message .= "</table></center>";
		$Reply = $email;
		$from = $name;

		//echo '<pre>'.$message.'</pre>';
		// сохраняем информацию о заказе в БД
		$mes = htmlspecialchars($message, ENT_QUOTES);
		$sql = "INSERT INTO orders (date, fullname, phone, email, comment, textform, delivery) VALUES ";
		$sql .= "( NOW(),'{$post['name']}', '{$post['phone']}', '{$post['email']}', '{$post['comments']}', '{$mes}', '{$post['deliveryType']}')";

		//echo $sql;
		$res = mysql_query($sql);
		//echo '$res >>'.$res;
		//die;
		// Let's send the email.
		$headers = "from: $from <$Reply>\nReply-To: $Reply \nContent-type: text/html";

		$to = 'medvedev095@gmail.com';
		$mail = mail($to, $subject, $message, $headers);

		$to = 'kosmosvape@gmail.com';
		$mail = mail($to, $subject, $message, $headers);

		// тестовая отправка мне
		$to = 'kemosabhay@gmail.com';
		$mail = mail($to, $subject, $message, $headers);

		if ($mail) {
			// в случае успешной отправкивычищаем из БД заказ
			deleteOrderFromCart($post['cartId']);
			echo '<div class="alert alert-success" role="alert">Заказ отправлен!</div>';
		} else {
			echo '<div class="alert alert-danger" role="alert">Письмо не было отправлено. Попробуйте связаться с намшим менеджером по телефону</div>';
		}
	} else {
		echo '<div class="alert alert-danger" role="alert">Ошибка сервера!</div>';
	}
?>