<?php
    $item = $_GET;

    // безопасность
    foreach ($item AS $field) {
        $field = trim(strip_tags($field));
    }

    // подготовка полей
    $item['item_id'] = $item['id'];
    unset($item['id']);

    if ($item['category'] == 'liquid'){
        $item['color'] = '';
        $item['nicotine'] = substr($item['nicotine'], 0, 1);
        $item['volume'] = substr($item['volume'], 0, 2);
    } else {
        $item['nicotine'] = '';
        $item['volume'] = '';
    }

    // проверяем наличие данного товара в карточке заказа
    $existed = findItemInCart($item);

    if (count($existed) > 0) {
        // если данный товар в карточке есть - инкрементируем его количество
        $result = changeItemCountInCart($existed['id'], $existed['quantity'] + $item['quantity']);
    } else {
        // вставка в БД
        $result = addItemToCart($item);
    }

    if ( ! $result) {
        $res = array(
            'res'   => FALSE,
            'msg'   => 'Ошибка добавления товара в корзину!'
        );
    } else {
        $res = array(
            'res'   => TRUE,
            'msg'   => 'Товар успешно добавлен в корзину'
        );
    }

    echo json_encode($res);
