<?php
    /**********************************************************
     * списки и справочники
    ***********************************************************/
	// справочник цветов
	$colors = array(
		'white'     => 'Белый',
		'black'     => 'Чёрный',
		'gold'      => 'Золотой',
		'red'       => 'Красный',
		'silver'    => 'Серебрянный',
		'cyan'      => 'Бирюзовый',
		'grey'      => 'Серый'
	);

	// справочник типов доставки
	$delRus = array(
		'courier' => 'Курьерская доставка',
		'self'    => 'Самовывоз'
	);

	// справочник состояний заказа
	$stateRus = array(
		'created'   => 'Создан',
		'paid'      => 'Оплачен',
		'rejected'  => 'Отклонён'
	);

	// справочниук алиасов для ЧПУ
	$alias = array(
		'all'           => 'all',
		'liquid'        => 'premialniejidkosti',
		'nonicoliquid'  => 'beznikotinoviejidkosti',
		'device'        => 'boksmodi',
		'producer'      => 'proizvoditeli'

	);

	//обратный массив ЧПУ
	$aliasBack = array();
	foreach($alias as $k => $v){
		$aliasBack[$v] = $k;
	}

	// фразы для заголовка "ПОИСК ПО"
	$cAlias = array(
		'liquid'        => 'жидкостям',
		'nonicoliquid'  => 'безникотиновым жидкостям',
		'device'        => 'устройствам',
		'producer'      => 'производителю'
	);

	// фразы для хлебных крошек
	$bAlias = array(
		'liquid'        => 'Жидкости',
		'nonicoliquid'  => 'Безникотиновые жидкости',
		'device'        => 'Устройства и акссесуары',
		'producer'      => 'Производители'
	);

	// Задаём id сессии/корзины
	$sessID = FALSE;

	$uri_parts= array();

	// page / area / category / itemID
	//	каталог / ТИп товара(суперкатегория) / категория / товраИД
	// массив параметров для старниц
	$data = array(
		'page'              => 'catalog', // страница
		'area'              => 'all', // суперкатегория (тип) товаров (девайсы, жидкости, производители)
		'category'          => '', // имя категории для фильтрования каталога
		'filtredBy'         => 'area', // фильтр каталога
		'pageType'          => 'full', // тип страницы (home/admin/full )
		'activeWidItem'     => '',// активный пункт меню категорий
		'activeMenuItem'    => ''// активный пункт меню
	);

	// справочник категорий
	$catList = getCategoriesList();

	//промо-настройки
	$promos = getSitePromos();