<?php

/**********************************************************
 * жидкости
***********************************************************/

/**
 * получаем список жидкостей из БД
 * @return array массив товаров
 */
function getLiquids($category=FALSE,$tag=FALSE){
    $liquids = array();
    //добавляем фильтры
    $where = '';
    $where .= (!empty($category)) ? "WHERE category = '{$category}' OR producer = '{$category}' " : '';
//        if ($tag) {
//            if ($where == ''){
//                $where .= "WHERE tags LIKE '%{$tag}%'";
//            } else {
//                $where .= "AND tags LIKE '%{$tag}%'";
//            }
//        }
    $where .= ( $where != '') ? ' AND quantity > 0' : 'WHERE quantity > 0';
    $q = mysql_query("SELECT * FROM liquids {$where}");
    if ($q) {
        for ($c=0; $c < mysql_num_rows($q); $c++){
            $liquids[] = mysql_fetch_array($q);
        }
    }
    return $liquids;
}
 /**
 * получаем список безникотиновых жидкостей из БД
 * @return array массив товаров
 */
function getNoNicoLiquids(){
    $liquids = array();
    $q = mysql_query("SELECT * FROM  `liquids` WHERE (nicotine = '0' OR nicotine LIKE '0%') AND  quantity > 0");
    if ($q) {
        for ($c=0; $c < mysql_num_rows($q); $c++){
            $liquids[] = mysql_fetch_array($q);
        }
    }
    return $liquids;
}



/**
 * Получаем жидкость по заданному id
 * @param $id
 * @return array - масси полей товара
 */
function getLiquidByName($name){
    $q = mysql_query("SELECT * FROM liquids where url_name ='{$name}'");
    $liquid[] = mysql_fetch_array($q);
    $result = $liquid[0];
    list($result['rating'], $result['ratingCnt']) = getaItemRate('liquid', $result['url_name']);

    return $result;
}

/**********************************************************
 * категории
 ***********************************************************/

/**
 * получаем справочник категорий
 * @return array массив справочник
 */
function getCategoriesList(){
    $categories = array();
    $q = mysql_query("SELECT * FROM category");
    for ($c=0; $c < mysql_num_rows($q); $c++){
        $tmp = mysql_fetch_array($q);
        $categories[trim($tmp['url_name'])] = trim($tmp['name']);
    }
    return $categories;
}
/**
 * получаем список категорий из БД
 * @return array массив категорий
 */
function getCategories($sortBy='parent'){
    $categories = array();
    $q = mysql_query("SELECT * FROM category ORDER BY parent");
    for ($c=0; $c < mysql_num_rows($q); $c++){
        $tmp[] = mysql_fetch_array($q);
    }

    if ($sortBy == 'parent'){
	    $categories['device'] = array();
	    $categories['liquid'] = array();
	    $categories['producer'] = array();
	    foreach ($tmp as $item) {

		    $categories[$item['parent']][$item['url_name']] = $item['name'];
	    }
    }
	if ($sortBy=='url_name'){
	    foreach ($tmp as $item) {
		    $categories[$item['url_name']]  = $item;
	    }
    }
    return $categories;
}

/**********************************************************
 * девайсы
 ***********************************************************/
/**
 * получаем список девайсов из БД
 * @return array массив товаров
 */
function getDevices($category=FALSE) {
    $devices = array();
    $where = '';
    $where .= (!empty($category)) ? "WHERE category = '{$category}' OR producer = '{$category}' " : '';
    $where .= ( $where != '') ? ' AND quantity > 0' : 'WHERE quantity > 0';
    $q = mysql_query("SELECT * FROM devices ".$where);
    if ($q) {
        for ($c = 0; $c < mysql_num_rows($q); $c++) {
	        $devices[] = mysql_fetch_array($q);
        }
        foreach ($devices as &$dev) {
	        $dev['color'] = explode(',', $dev['color']);
	        $dev['producer'] = trim($dev['producer']);
	        $dev['url_name'] = trim($dev['url_name']);
        }
    }
    return $devices;
}

/**
 * Получаем девайс по заданному url_name
 * @param $name
 * @return array - масси полей товара
 */
function getDeviceByName($name){
	$q = mysql_query("SELECT * FROM devices where url_name ='{$name}'");
	$device[] = mysql_fetch_array($q);
    if ( ! empty($device[0])){
		$device[0]['color'] = explode(',', $device[0]['color']);
	    $device[0]['producer'] = trim($device[0]['producer']);
	    $device[0]['url_name'] = trim($device[0]['url_name']);
	}
    $result = $device[0];
    list($result['rating'], $result['ratingCnt']) = getaItemRate('device', $result['url_name']);
    return $result;
}

/**
 * создаёт из строки тегов ссылки для поиска по тегам
 * @param $tagLine - строка тегов из БД
 * @return строка содержащая набор ссылок
 */
function writeTagsLinks($tagLine){
    $result = '';
    //удаляем лишние пробелы
    //$tagLine = preg_replace('/\s+/', '', $tagLine);
    $tagLine = trim($tagLine);

    //получаем массив
    $tags = explode( ',', $tagLine);
    $i=0;
    foreach ($tags as $tag){
        $sem = ( $i != count($tags)-1 ) ? ', ': '';
        //$result .= "<a href='/catalog.php?tag=$tag'>$tag$sem</a>";
        $result .= "$tag$sem";
        $i++;
    }

    return $result;
}

/**********************************************************
 * корзиан заказа
 ***********************************************************/

/**
 * добавляет запись в таблицу хранения заказа
 * @param $item массив полей вставляемого итема
 * @return boolean результат вставки
 */
function addItemToCart($item){
    $sql = "INSERT INTO cart (session_id, item_id, category, `name`, producer, weight, url_name, nicotine, volume, quantity, price, color) VALUES ";
    $sql .= "('{$item['session_id']}','{$item['item_id']}', '{$item['category']}', '{$item['name']}',
            '{$item['producer']}', '{$item['weight']}', '{$item['url_name']}', '{$item['nicotine']}', '{$item['volume']}',
            '{$item['quantity']}', '{$item['price']}', '{$item['color']}')";
    return mysql_query($sql);
}
/***/
function getCartItems($kosmosessid){
    $items = array();
    $q = mysql_query("SELECT * FROM cart WHERE session_id = '{$kosmosessid}'");
    for ($c=0; $c < mysql_num_rows($q); $c++){
        $items[] = mysql_fetch_array($q);
    }
    return $items;
}

function getCartCount($kosmosessid){
    $items = getCartItems($kosmosessid);
    $quantity = 0;
    foreach ($items as $item) {
        $quantity +=$item['quantity'];
    }
    return $quantity;
}



/***/
function deleteOrderFromCart($kosmosessid){
    $sql = mysql_query("DELETE FROM cart WHERE session_id = '{$kosmosessid}'");
    return mysql_query($sql);
}

/**
 * удаляем заказ из корзины
 * @param $id
 * @return bool результат выполнения запроса
 */
function removeItemFromCart($id){
    $sql = "DELETE FROM cart   WHERE id = '{$id}'";
    return mysql_query($sql);
}


/**
 * меняем количество товара в корзине
 * @param $id
 * @param $quantity
 * @return bool результат выполнения запроса
 */
function changeItemCountInCart($id, $quantity){
    //* елси вручную задано нулевое или отрицательнео количество
    if ($quantity <= 0 ){
	    $sql = "DELETE FROM cart WHERE id = '{$id}'";
    } else {
	    $sql = "UPDATE cart  SET quantity = '{$quantity}' WHERE id = '{$id}'";
    }
    return mysql_query($sql);
}

/**
 * поиск записи в корзине
 * @param $item массив полей для поиска
 * @return array
 */
function findItemInCart($item){
    // поле количества товаров не нужно для поиска
    unset($item['quantity']);

    //задаём переменные
    $items = array();
    $i = 0;
    $where = 'WHERE ';
    foreach ($item AS $key => $val) {
        $sem = ($i > 0) ? ' AND ' : '';
        $where .= $sem. "{$key} = '{$val}'";
        $i++;
    }
    $sql = "SELECT * FROM cart {$where}";
    //echo $sql; die;
    $q = mysql_query($sql);
    if ($q) {
        for ($c=0; $c < mysql_num_rows($q); $c++){
            $items[] = mysql_fetch_array($q);
        }
    }

    return isset($items[0]) ? $items[0] : $items;
}

/**********************************************************
 * разное
 ***********************************************************/
/**
 * генерирует уникальный id для сессии
 * @return string
 */
function generateId() {
    return strtoupper(md5(uniqid(rand(), TRUE)));
}

function escapeIdent($value) {
    return "`".str_replace("`","``",$value)."`";
}

function makeSafeArray($array) {
    $resArray = array();
    foreach ($array as $key => $val) {
        if (is_array($val)) {
            $resArray[$key] = makeSafeArray($val);
        } else {
            $resArray[$key] = htmlspecialchars(trim($val), ENT_QUOTES);
        }
    }
    return $resArray;
}

/**
 * получение реальной ссылки на картинку товара.
 * Если картинка отсутствет - пробуем подставить дефолтную картинку производителя
 * либо просто дефолтную картинку товара
 * @param $filename имя файла
 * @param $size размер картинки (big,small)
 * @param $type - категория товара
 * @param $producer - производитель товара
 * @return string - путь к картинке
 */
function safeFile($filename, $size, $type, $producer) {
    if ( ! file_exists($_SERVER["DOCUMENT_ROOT"].$filename)) {
        // девайсы
        if ($type == 'device') {
            if ( file_exists($_SERVER["DOCUMENT_ROOT"]."/images/devices/{$producer}_default_{$size}.jpg")) {
                $filename = "/images/devices/{$producer}_default_{$size}.jpg";
            } else {
                $filename = "/images/devices/blank_device_{$size}.jpg";
            }
        }
        // жижи
        if ($type == 'liquid') {
            if ( file_exists($_SERVER["DOCUMENT_ROOT"]."/images/liquids/{$producer}_default_{$size}.jpg")) {
                $filename = "/images/liquids/{$producer}_default_{$size}.jpg";
            } else {
                $filename = "/images/liquids/blank_liquid_{$size}.jpg";
            }
        }
    }
    return $filename;
}

/**********************************************************
 * админка
 ***********************************************************/

/**
 * получает текстовки
*/
function getSiteTexts(){
    $items = array();
    $q = mysql_query("SELECT * FROM site_texts");
    for ($c=0; $c < mysql_num_rows($q); $c++){
        $items[] = mysql_fetch_array($q);
    }
    return $items;
}
/**
 * получает текстовки в виде пар значений
*/
function getSiteTextsPairs(){
    $items = array();
    $res = array();
    $q = mysql_query("SELECT * FROM site_texts");
    for ($c=0; $c < mysql_num_rows($q); $c++){
        $items[] = mysql_fetch_array($q);
    }
    // преобразую в пары значений
    foreach ($items as $item) {
	    $res[$item['name']] = $item['text'];
    }
    return $res;
}
 function getSitePromos(){
    $items = array();
    $res = array();
    $q = mysql_query("SELECT * FROM site_promo");
    for ($c=0; $c < mysql_num_rows($q); $c++){
        $items[] = mysql_fetch_array($q);
    }
    // преобразую в пары значений
    foreach ($items as $item) {
	    $res[$item['name']] = $item['text'];
    }
    return $res;
}


/**
 * На основании массива данных старницы получает её метадтеги
 * @param $data
 * @return array
 */
function getSeoElements($data){
    //todo добавить catalog_ХХХ_meta_text
    // получаем текстовые настройки сайта
    $textls = getSiteTextsPairs();

	// формируем массив сео-текстовок
    $seoArr = array(
		// основной набор тегов
		'main' => array(
				'meta_title' => $textls['meta_title'],
				'meta_keywords' => $textls['meta_keywords'],
				'meta_description' => $textls['meta_description'],
				'meta_catalog_text' => $textls['catalog_all_text']
		),
		// набор тегов для области (жидкости, производители, девайсы...)
		'area' => array(
				'meta_title' => '',
				'meta_keywords' => '',
				'meta_description' => '',
				'meta_catalog_text' => ''
		),
		// набор тегов для категории
		'category' => array(
				'meta_title' => '',
				'meta_keywords' => '',
				'meta_description' => '',
				'meta_catalog_text' => ''
		),
		// набор тегов для товара
		'item' => array(
				'meta_title' => '',
				'meta_keywords' => '',
				'meta_description' => '',
				'meta_catalog_text' => ''
		)
	);

	// последовательно, начиная с дефолтных для всех страниц метаданных, заполняем уровни метаданных
	// основной набор тегов
	$seoArr['main'] = array(
		'meta_title' => $textls['meta_title'],
		'meta_keywords' => $textls['meta_keywords'],
		'meta_description' => $textls['meta_description'],
		'meta_catalog_text' => $textls['catalog_all_text']
	);

	// набор тегов для области (жидкости, производители, девайсы...)
	if (($data['page'] == 'card_device' OR $data['page'] == 'card_liquid' OR $data['page'] == 'catalog') AND ! empty($data['area'])){
		$aliasBack = $data['aliasBack'];
		$seoArr['area'] = array(
				'meta_title' => $textls["catalog_{$aliasBack[$data['area']]}_meta_title"],
				'meta_keywords' => $textls["catalog_{$aliasBack[$data['area']]}_meta_keywords"],
				'meta_description' => $textls["catalog_{$aliasBack[$data['area']]}_meta_description"],
				'meta_catalog_text' => $textls["catalog_{$aliasBack[$data['area']]}_text"]
		);
	}

	// набор тегов для категории
    if (($data['page'] == 'card_device' OR $data['page'] == 'card_liquid' OR $data['page'] == 'catalog') AND ! empty($data['area']) AND ! empty($data['category'])){
	    $cat = getCategoryByName($data['category']);
	    $seoArr['category'] = array(
			    'meta_title' => $cat["meta_title"],
			    'meta_keywords' => $cat["meta_keywords"],
			    'meta_description' => $cat["meta_description"],
			    'meta_catalog_text' => ''
	    );
    }

	// набор тегов для товара
    if (($data['page'] == 'card_device' OR $data['page'] == 'card_liquid' ) AND ! empty($data['area']) AND ! empty($data['itemUrlName'])){
	    $itm = $data['currentItem'];
	    $seoArr['item'] = array(
			    'meta_title' => $itm["meta_title"],
			    'meta_keywords' => $itm["meta_keywords"],
			    'meta_description' => $itm["meta_description"],
			    'meta_catalog_text' => ''
	    );
    }
	// формируем финальный массив
	$metaArray = array(
			'meta_title'        => getMetaChain($seoArr, 'title'),
			'meta_keywords'     => getMetaChain($seoArr, 'title'),
			'meta_description'  => getMetaChain($seoArr, 'title'),
			'meta_catalog_text' => getMetaChain($seoArr, 'title'),
			'og_image'          => $textls['og_image'],
			'og_title'          => $textls['og_title'],
			'og_description'    => $textls['og_description'],
			'data'              => $data
	);
    return $metaArray;
}

/**
 * полседовательно проверяет уровни наследования мета-данных
 * и возвращает первое поавшееся непустое значение
 *
 * @param $arr - массив метаданных
 * @param $field - суффикс мета-тега
 * @return mixed - текст мета-тега
 */
function getMetaChain($arr, $field){

	if ($arr['item']['meta_'.$field] != '' ){
		return $arr['item']['meta_'.$field];
	} elseif ($arr['category']['meta_'.$field] != '' ){
		return $arr['category']['meta_'.$field];
	} elseif ($arr['area']['meta_'.$field] != '' ){
		return $arr['area']['meta_'.$field];
	} else {
		return $arr['main']['meta_'.$field];
	}
}

/**
 * сохраняет текстовки
*/
function saveSiteTexts($post){
    foreach ($post as $key => $val){
	    $sql = "UPDATE site_texts SET text = '{$val}' WHERE name = '{$key}';";
	    //echo $sql;
	    mysql_query($sql);
    }
    return TRUE;
}

/**
 * сохраняет промо-настройки
*/
function savePromos($post){
    foreach ($post as $key => $val){
	    $sql = "UPDATE site_promo SET text = '{$val}' WHERE name = '{$key}';";
	    //echo $sql;
	    mysql_query($sql);
    }
    return TRUE;
}

/**
 * получает список заказов
 */
function getOrders(){
	$items = array();
	$q = mysql_query("SELECT * FROM orders");
	for ($c=0; $c < mysql_num_rows($q); $c++){
		$items[] = mysql_fetch_array($q);
	}
	return $items;
}


/**
 * получает заказ
 */
function getOrder($id){
	$items = array();
	$q = mysql_query("SELECT * FROM orders WHERE id='{$id}'");
	for ($c=0; $c < mysql_num_rows($q); $c++){
		$items[] = mysql_fetch_array($q);
	}
	$result = (!empty($items[0])) ? $items[0] : FALSE ;
	return $result;
}

/**
 * получает последнее занчение id ииз таблицы заказов
 */
function getLastOrderId(){
	$q = mysql_query("SELECT MAX(id) FROM orders");
	$max[] = mysql_fetch_array($q);
	return $max[0][0];
}


/**
 * создаёт или сохраняет запись в таблице
 */
function saveRow($post, $table){
    if ($table == 'liquids'){
	    // преобразуем значениия крепости
	    if (!isset($post['nicotine'])){
		    $post['nicotine'] = '0';
	    } else {
		    $post['nicotine'] = implode(',', array_keys($post['nicotine']));
	    }
    }
    if ($table == 'devices'){
	    // преобразуем значениия крепости
	    if (isset($post['color'])){
		    $post['color'] = implode(',', array_keys($post['color']));
	    }
    }

    if ($post['id'] != ''){
	    // редактирование
	    $values = '';

	    foreach ( $post as $key => $val) {
		    if($key != 'id'){
			    $values .= "`{$key}` = '{$val}'".',';
		    }
	    }

	    //уладяем лишнюю запятую
	    $values = substr($values, 0, -1);

	    $sql = "UPDATE {$table}  SET {$values} WHERE id = '{$post['id']}'";

//		    echo 'edit<br/>';
//		    echo $sql;
	    return mysql_query($sql);
    } else {
	    // создание
	    unset($post['id']);
	    $keys = '';
	    $values = '';
	    foreach ($post as $key => $val){
		    $keys .= "`".$key."`,";
		    $values .= "'".$val."'".',';
	    }

	    //уладяем лишнюю запятую
	    $keys = substr($keys, 0, -1);
	    $values = substr($values, 0, -1);

	    $sql = "INSERT INTO {$table} ({$keys}) VALUES ({$values})";
//		    echo 'create<br/>';
//		    echo $sql;
	    return mysql_query($sql);
    }
}

/***/
function getCategoryByName($name){
	$q = mysql_query("SELECT * FROM category where url_name ='{$name}'");
	$cat[] = mysql_fetch_array($q);
	return $cat[0];
}


function removeItem($fields){
    $table = '';
    $id = $fields['id'];
    $type = $fields['itemType'];
    switch ($type) {
	    case 'liquid':
		    $table = 'liquids';
		    break;
	    case 'device':
		    $table = 'devices';
		    break;
	    case 'categoryl':
	    case 'categoryd':
	    case 'producer':
			// TODO вставить проверку на использование категорий
	        $table = 'category';
		    break;
    }
    if ($table != '' AND $id != ''){
		$sql = "DELETE FROM {$table} WHERE url_name = '{$id}'";
	    return mysql_query($sql);
    }
    return FALSE;
}

function printRating($itemType, $itemId, $rate, $rateCnt){
	// проверяем, не голосовал ли юзер ранее за этот товар
	$canRate = ( ! empty($_COOKIE["rate_" . $itemId])) ? '' : 'can-rate';
	//role='button'
	echo "<div class='product-rating' tabindex='0' data-toggle='popover'  data-placement='bottom' data-trigger='focus' itemprop='aggregateRating' itemscope itemtype='http://schema.org/AggregateRating' data-content='Благодарим за Ваше участие в нашем рейтинге!'>";
	echo "<meta itemprop=ratingValue content = '{$rate}'/>";
	echo "<div class='product-rating-stars  {$canRate} text-center'  data-toggle='popover' data-placement='bottom' data-content='Поставь свою оценку!'>";
	for ($i=1; $i<=5; $i++) {
		if ($i <= $rate) {
			$class = 'fa fa-star fill';
		} else {
			$class = 'fa fa-star-o empty';
		}
		echo "<i data-weight='{$i}' data-type='{$itemType}' data-id='{$itemId}' class='{$class}'></i>";
	}
	echo '</div>';
	echo '<div class="editContent text-center">';
	echo '<i class="fa fa-user" aria-hidden="true"></i>'."&nbsp;<span data-count='{$rateCnt}' class='rating-count' itemprop='ratingCount'>{$rateCnt}</span>&nbsp;голоса(ов)";
	echo '</div>';
	echo '</div>';
}
function getaItemRate($itemType, $itemId){
	$items = array();
	$rating = 0;
	$i = 0;
	$q = mysql_query("SELECT rate FROM item_rating WHERE item_type = '{$itemType}' AND item_id = '{$itemId}'");
	for ($c=0; $c < mysql_num_rows($q); $c++){
		$items[] = mysql_fetch_array($q);
	}

	//вычисляем рейтинг
	if ( ! empty($items[0]) ) {
		$i=0;
		$rate=0;
		foreach ($items as $row) {
			$rate += $row['rate'];
			$i++;
		}
		$rating = round($rate/$i);
	}
	return array($rating, $i);
}

/**
 * сохраняет факт голосования юзером за товар
 * @param $item
 * @return resource
 */
function saveRate($item) {
	$item['user'] = 'user';
	$sql =  "INSERT INTO item_rating (user, item_type, item_id, rate) VALUES ".
			"('{$item['user']}','{$item['type']}','{$item['id']}','{$item['rate']}')";
	return mysql_query($sql);
}

	/**
 * дебажная распечатка данных
 * @param $array
 *
 */
function pp($array){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

/**
 * поиск email в спсике ранее подпсиавшихся
 * @param $email
 * @return array
 */
function findEmailInSubs($email){
	$items = array();
	$q = mysql_query("SELECT * FROM subscriptions WHERE email = '{$email}'");
	if ($q) {
		for ($c=0; $c < mysql_num_rows($q); $c++){
			$items[] = mysql_fetch_array($q);
		}
	}
	$res = count($items);
	//print_r($items);
	//print_r(count($items));
	//print_r($res);
	return $res;
}

/**
 * добавляет запись в таблицу подписок
 * @param $email
 * @return boolean результат вставки
 */
function addEmailToSubs($email){
	// проверяем наличие данного email среди ранее подписанных
	$existed = findEmailInSubs($email);

	// ставим куку на месяц
	$exp = 24 * 60 * 60 * 30;

	if ($existed == 0) {
		// вставка в БД
		$sql = "INSERT INTO subscriptions (email) VALUES ('{$email}')";
		$result = mysql_query($sql);

		if ( ! $result) {
			// ставим куку на день
			$exp = 24 * 60 * 60;
		}
	}
	return $exp;
}

/**
 * получает список подписчиков
 */
function getSubscribers(){
	$items = array();
	$q = mysql_query("SELECT * FROM subscriptions");
	for ($c=0; $c < mysql_num_rows($q); $c++){
		$items[] = mysql_fetch_array($q);
	}
	return $items;
}

/**
 * подсчитывает "старую цену" для блока скидок
 *
 * @param $price
 * @return mixed
 */
function oldLiqPrice($price) {
	$promo = getSitePromos();
	return $price+($price / 100 * $promo['old_price_liquid_percent']);
}

/**
 * подсчитывает "старую цену" для блока скидок
 *
 * @param $price
 * @return mixed
 */
function oldDevPrice($price) {
	$promo = getSitePromos();
	return $price + ($price / 100 * $promo['old_price_device_percent']);
}

