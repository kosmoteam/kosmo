<!-- Start Contact 1 -->
<section class="content-block contact-1">
    <div class="container text-left">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="col-md-12 breadcrumbs">
                <h4>
                    <ul class="filter">
                        <li><a href="/">Главная</a></li>
                        <li>Статус заказа</li>
                    </ul>
                </h4>
            </div>
            <div class="editContent text-center col-sm-12">
                <h1>Статус заказа</h1>
            </div>
            <div class="col-sm-10 col-sm-offset-1 text-center ">
                <?php include($_SERVER["DOCUMENT_ROOT"] . "/checkout.ru/deliveryhistory/index.php"); ?>
            </div>
        </div><!-- /.col-sm-10 -->
    </div><!-- /.container -->
</section><!-- /.content-block -->
<!--// END Contact 1 -->
