<?php
    // получаем списки товаров в корзине
    $items = getCartItems($sessID);
    $total = 0;
    $deliveryPrice = 350;
    $count = 0;
    foreach ($items as $item) {
        $total += $item['quantity']*$item['price'];
        $count += $item['quantity'];
    }
    $isCartEmpty  = (count($items ) == 0) ? TRUE : FALSE ;

    // инициализация checkout
    $apiKey  = 'oQZMgKL7m75NUVb1W3O5LR0L';
    $tuCurl = curl_init();
    curl_setopt($tuCurl, CURLOPT_URL,
            "http://platform.checkout.ru/service/login/ticket/" . $apiKey);
    curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
    curl_setopt($tuCurl, CURLOPT_HEADER, 0);
    curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
    $tuData = curl_exec($tuCurl);
    if(!curl_errno($tuCurl)){
        $info = curl_getinfo($tuCurl);
    } else {
        //TODO  подумать что тут показать посетителю
        echo 'Curl error: ' . curl_error($tuCurl);
    }
    curl_close($tuCurl);
    $responce =  json_decode($tuData,true);
    $ticket = $responce["ticket"];
?>

<script type="text/javascript">
    var deliveryPrice = <?=$deliveryPrice?>;
    var totalPrice = <?=$total?>;

    $(document).ready(function() {
        $("input[name='deliveryType']").change(function() {
            var val = $("input[type='radio'][name='deliveryType']:checked").val();
            if (val == 'self'){
                $('#delivery-note').hide();
                $('#order-price').html(totalPrice);
            } else {
                $('#delivery-note').show();
                $('#order-price').html(totalPrice+deliveryPrice);
            }
        });
    });
</script>
<!-- Start Contact 1 -->
<section class="content-block contact-1">
    <div class="container text-left">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="col-md-12 breadcrumbs">
                <h4>
                    <h4>
                        <ul class="filter">
                            <li><a href="/">Главная</a></li>
                            <li>Заказ и доставка</li>
                        </ul>
                    </h4>
                </h4>
            </div>
            <div class="editContent text-center">
                <h1>Заказ и доставка</h1>
            </div>
            <? if ($isCartEmpty) : ?>
                <div class="editContent text-center">
                    <h3>Ваша корзина пока пуста.</h3>
                    <h4><a href="/catalog">Перейти к выбору товаров</a></h4>
                </div>
            <? else :?>
                <div id="contact" class="form-container text-center">
                    <h4>Заполните форму и отправьте нам. <br/>Наш менеджер свяжется с Вами в ближайшее время</h4>
                    <div class="editContent alert alert-info">
                        <p>Вами заказно <b><?=$count?></b> товар(а/ов) на сумму <b><span id="order-price"><?=$total+$deliveryPrice?></span><span class="price-rub"></span></b>
                            <span id="delivery-note"> (включая курьерскую доставку <b>350<span class="price-rub"></span></b>)</span></b></p>
                        <p>(<a href="/cart">просмотреть ваш заказ в корзине</a>)</p>
                    </div>
                    <div id="message"></div>
                    <form method="post" action="/send" name="contactform" id="contactform">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input name="name" id="name" placeholder="Имя" class="form-control" type="text" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input name="mobilephone" id="mobilephone" placeholder="Телефон" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input name="email" id="email" placeholder="Email" class="form-control" type="text" value="">
                                    <input type="hidden" id="cartId" name="cartId" value="<?=$sessID?>">
                                    <input type="hidden" id="cartItCount" name="cartItCount" value="<?=$count?>">
                                    <input type="hidden" id="cartTotal" name="cartTotal" value="<?=$total?>">
                                </div>
                            </div>
                        </div><!-- /.row -->

                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group text-left">
                                    Другие способы связи с вами (вКонтакте, Telegram, WhatsApp и т.д.) и комментарии к заказу:
                                    <textarea name="comments" id="comments" class="form-control" rows="3" placeholder="Другие способы связи с вами (вКонтакте, Telegram, WhatsApp и т.д.) и комментарии к заказу:"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-5">
                                <div class="form-group text-left">
                                    <h6>Способ доставки</h6>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="deliveryType" id="deliveryType1" value="courier" checked>
                                            Курьерская доставка <span class="small text-muted">(по Москве в пределах МКАД)</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="deliveryType" id="deliveryType2" value="self">
                                            Самовывоз
                                            <span class="small text-muted">(по адресу: Пресненская набережная 6, стр. 2, Москва. +7 916 448 75 71)</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="editContent">
                            <p class="small text-muted">После отправки сообщения мы с вами свяжемся в течении 30 минут в рабочее время по Москве - с 10 утра до 19 вечера в будние дни. Заказы отправленные в другое время будут обработаны на следующий рабочий день</p>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit" id="cf-submit" name="submit">Оформить заказ</button>
                        </div>
                    </form>
                </div><!-- /.form-container -->
            <? endif; ?>
            <hr>
            <div class="editContent row">
                <div class="col-sm-6">
                    <h6>Оплата</h6>
                    <p>Оплата производится только наличным способом в момент получения Вами вашего заказа.</p>
                    <h6>Доставка</h6>
                    <p>Стоимость доставки по Москве в пределах МКАД составляет 350 рублей и осуществляется нашей курьерской службой. Заказы доставляются на следующий рабочий день после оформления. Условия и сроки доставки за МКАД обговариваются отдельно с менеджером по телефону или электронной почте.</p>
                    <h6>Самовывоз</h6>
                    <p>Самовывоз доступен в день заказа в случае, если товар в наличии. Вы можете самостоятельно забрать свой заказ по адресу Пресненская набережная 6, стр. 2, башня «Империя», Южное Лобби., с 10 утра до 19 вечера в будние рабочие дни. В выходные дни самовывоз обговаривается отдельно с менеджером по телефону или электронной почте.</p>
                </div>
                <div class="col-sm-6 text-center">
                    <ul class="contact-info">
                        <li><span class="fa fa-user"></span>ИП Антон Сергеевич Постюшков - Kosmos Vape,</li>
                        <li><span class="fa fa-map-marker"></span>Пресненская набережная 6, стр. 2, Москва.</li>
                        <li><span class="fa fa-phone"></span>+7 916 448 75 71 <a href="mailto:kosmosvape@gmail.com">kosmosvape@gmail.com</a> </li>
                    </ul>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.590468280113!2d37.53830451609125!3d55.748244299810935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54bdcf5c58c73%3A0x92d5b50611b605cc!2z0J_RgNC10YHQvdC10L3RgdC60LDRjyDQvdCw0LEuLCA2INGB0YLRgNC-0LXQvdC40LUgMiwg0JzQvtGB0LrQstCwLCAxMjMzMTc!5e0!3m2!1sru!2sru!4v1460043146666" width="450" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>

                </div>
            </div>
        </div><!-- /.col-sm-10 -->

    </div><!-- /.container -->
</section><!-- /.content-block -->
<!--// END Contact 1 -->
