<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kosmos Vape: премиальные элитные жидкости со всего мира</title>
    <meta name="description" content="Продажа премиальных, элитных и импортных жидкостей для электронных испарителей по доступным ценам в Москве с доставкой.">
    <meta property="place:location:latitude" content="13.062616"/>
    <meta property="business:contact_data:locality" content="Москва"/>
    <meta property="business:contact_data:country_name" content="Россия"/>
    <meta property="business:contact_data:email" content="kosmosvape@gmail.com"/>
    <meta property="business:contact_data:phone_number" content="+7 916 448-75-71"/>
    <meta property="business:contact_data:website" content="http://kosmosvape.ru"/>

    <meta name="google-site-verification" content="HqOcKEPB_D1yWu42ggdiQNjxu-SKcqqOI60HuoMdwvI" />
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/styles.css" media="screen" />
    <link rel="stylesheet" href="/css/magnific-popup.css" media="screen" />
    <link rel="stylesheet" href="/css/retina.css" media="screen" />
    <link rel="stylesheet" href="/css/media.css" media="screen" />
    <link rel="stylesheet" href="/css/animated.min.css" media="screen" />
    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
    <link href="/css/footer-1.css" rel="stylesheet">
    <script type="text/javascript" src='/js/modernizr.min.js'></script>
</head>
<body >
<div class="loader"><div class="bar"><strong>KOSMOS VAPE</strong><span class="progress color-2"><span>загружаем...</span></span></div></div>
<header class="alt color-2"  data-animation="fadeIn fast" data-delay="0">
    <div class="container">
        <div class="row">
            <div class="col-md-3 for-mobile-header-1">
                <h1><a href="#pagetop">Kosmos Vape</a></h1>
                <p style="max-width:500px; margin:0 auto; padding-bottom:63px; color: white !important;"    data-animation="fadeInUp slow" data-delay="0.2">premium e-liquids |
                    +7 916 448-75-71</p>
            </div>
            <div class="col-md-9 for-mobile-header-2 for-one-page"><div class="mobile-wrapper clearfix">
                    <nav class="main-menu clearfix navbar navbar-default">
                        <div class="navbar-header" data-toggle="collapse" data-target="#top-main-menu">
                            <button type="button" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Меню</a>
                        </div>
                    </nav>
                    <div class="collapse navbar-collapse m-menu" id="top-main-menu">
                        <ul class="sf-menu superfish-li-relative color-2">
                            <li class="current-menu-item">
                                <a href="#welcome">главная</a>
                            </li>
                            <li><a href="#services">предлагаем</a></li>
                            <!--
							<li class="dropdown">
								<a href="#about">about us</a>
								<ul class="submenu">
									<li><a href="#about">About shopster</a></li>
									<li><a href="#meet_team">Meet the team</a></li>
									<li><a href="#skills">Our skills</a></li>
									<li><a href="#awards_block">Awards</a></li>
								</ul>
							</li>
							<li><a href="#clients">clients</a></li>
							<li><a href="#portfolio">portfolio</a></li>
							<li><a href="#blog">blog</a></li>
							<li><a href="#contacts">contacts</a></li>
							<li class="menu-item-border">
								<a href="http://themeforest.net/item/family-multipurpose-responsive-one-page-template/8819554?ref=olegnax">Purchase</a>
							</li> -->
                        </ul>
                    </div>
                </div></div>
        </div>
    </div>
</header>
<!--Welcome section-->
<section class="waypoint video-block fullscreen" data-hash="welcome">
    <video width="1920" height="1080" autoplay="autoplay" muted="muted" loop="loop" poster="http://placehold.it/1920x1080" class="background-video" >
        <source src="video/video.webm" type="video/webm">
        <source src="video/video.mp4" type="video/mp4">
    </video>
    <div class="text-inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <ul class="features"   data-animation="fadeIn" data-delay="0.4">
                        <li>the best premium e-liquids from around the world</li>
                    </ul>
                    <h6><a href="#services" class="to-text" data-animation="flipInX" data-delay="0">KOSMOS VAPE</a></h6>
					<span class="learn-more"   data-animation="fadeInUp" data-delay="0.6">-  site under construction  -
					<br/>
					For cooperation and commercial issues kosmosvape@gmail.com
					</span>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay color-2" style="opacity:0.5;"></div>
</section>
<!--Services section-->
<section class="alt-bg-1 color-2 waypoint" data-hash="services">
    <div class="container">
        <div class="spacer"  style="height:82px;"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="tAc">
                    <h2 class="big-title white color-2" data-animation="fadeInUp slow" data-delay="0">что мы предлагаем</h2>
                    <p style="max-width:500px; margin:0 auto; padding-bottom:63px;"    data-animation="fadeInUp slow" data-delay="0.2">Мы рады предложить вам лучшие жидкости для электронных сигарет от зарубежных производителей.</p>
                </div>
            </div>
            <div class="col-sm-4 top-features color-2 content-element" >
                <i class="fa fa-globe feature"  data-animation="fadeInUp " data-delay="0.3"></i>
                <h3 data-animation="fadeInUp " data-delay="0.5" class="white">Доставка</h3>
                <p  data-animation="fadeInUp " data-delay="0.7">Доставка осуществляется по Москве в пределах МКАД и ближайшему Подмосковью. Доставка оплачивается курьеру и в стоимость заказа не входит.</p>
            </div>
            <div class="col-sm-4 top-features color-2 content-element">
                <i class="fa fa-fire feature"  data-animation="fadeInUp " data-delay="0.3"></i>
                <h3 data-animation="fadeInUp " data-delay="0.5" class="white">Предзаказ и наличие</h3>
                <p data-animation="fadeInUp " data-delay="0.7" >Временно принимаем заказы только через социальные сети, электронную почту и по телефону.</p>
            </div>
            <div class="col-sm-4 top-features color-2 content-element" >
                <i class="fa fa-twitter feature"  data-animation="fadeInUp " data-delay="0.3"></i>
                <h3 data-animation="fadeInUp " data-delay="0.5" class="white">Ассортимент</h3>
                <p data-animation="fadeInUp " data-delay="0.7">В нашем ассортименты представлены только импортные жидкости, в том числе те, продажу которых не осуществляет в РФ никто, кроме нас.</p>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="tAc">
                <a href="/catalog"><h2 class="big-title white color-2" data-animation="fadeInUp slow" data-delay="0">перейти в каталог</h2></a>

            </div>
        </div>
</section>

<? include('tpl/map.php');?>

<!-- Start Footer 1-1 -->
<section class="content-block-nopad bg-deepocean">
    <div class="container footer-1-1">
        <div class="row">

            <div class="col-sm-5">
                <img src="images/brand/bskit-logo-white.png" class="brand-img img-responsive"/>
                <div class="editContent">
                    <h3>Over <strong>10,000</strong> downloads last month…</h3>
                </div>
                <div class="editContent">
                    <p class="lead">We've hand-crafted <strong>bskit</strong> in great detail to make your life easier… enjoy!</p>
                </div>
            </div>

            <div class="col-sm-6 col-sm-offset-1">
                <div class="row">
                    <div class="col-md-4">
                        <div class="editContent">
                            <h4>Shortcuts</h4>
                        </div>
                        <div class="editContent">
                            <ul>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Portfolio</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="editContent">
                            <h4>Legal Stuff</h4>
                        </div>
                        <div class="editContent">
                            <ul>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms &amp; Conditions</a></li>
                                <li><a href="#">Cookie Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="editContent">
                            <h4>Seen Enough?</h4>
                        </div>
                        <a href="#" class="btn btn-block btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!--// END Footer 1-1 -->
<script type="text/javascript" src='/js/jquery.min.js'></script>
<script type="text/javascript" src='/js/script.js'></script>
</body>
</html>