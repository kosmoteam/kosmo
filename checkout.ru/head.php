<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/checkout.ru/config.php"); ?>
<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/checkout.ru/include.php"); ?>

<link rel="stylesheet" type="text/css" href="<?php print $checkoutServerUrl; ?>/checkout/css/checkout-external.css?<?php print time(); ?>" />
<link rel="stylesheet" type="text/css" href="<?php print $checkoutServerUrl; ?>/checkout/css/jquery-ui.min.css?<?php print time(); ?>" />
<link rel="stylesheet" type="text/css" href="<?php print $checkoutServerUrl; ?>/checkout/css/jquery.modal.css?<?php print time(); ?>" />
<link rel="stylesheet" type="text/css" href="<?php print $checkoutServerUrl; ?>/checkout/css/jquery.autocomplete.css?<?php print time(); ?>" />
<link rel="stylesheet" type="text/css" href="/checkout.ru/css/custom.css?<?php print time(); ?>" />

<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/jquery.min.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/jquery-ui.min.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/jquery.timing.min.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/jquery.modal.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/jquery.notification.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/jquery.autocomplete.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/geo.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/common.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/phonemask/jquery.bind-first-0.1.min.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/phonemask/phones-ru.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/phonemask/jquery.inputmask-multi.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/phonemask/jquery.inputmask.js?<?php print time(); ?>" type="text/javascript"></script>
<script src="<?php print $checkoutServerUrl; ?>/checkout/js/checkout-external.js?<?php print time(); ?>" type="text/javascript"></script>