﻿<?php
ini_set('display_errors', 1);
header("Content-Type: text/html; charset=utf-8");
require_once($_SERVER["DOCUMENT_ROOT"] . "/checkout.ru/deliveryhistory/config.php");

$shopOrderNumber = $_POST["shopOrderNumber"];

$tuCurl = curl_init();

curl_setopt($tuCurl, CURLOPT_URL, $checkoutServerUrl."/service/order/statushistory/".$shopOrderNumber."?apiKey=".$apiKey);
curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
curl_setopt($tuCurl, CURLOPT_HEADER, 0);
curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);

$tuData = curl_exec($tuCurl);

if(!curl_errno($tuCurl)){
$info = curl_getinfo($tuCurl);
} else {
echo 'Curl error: ' . curl_error($tuCurl);
}

curl_close($tuCurl);

$responce =  json_decode($tuData,true);

if ($info['http_code'] != 200) {
    echo $info['http_code']; exit();
}
if (count($responce["deliveryHistory"]) < 1) {
    echo 'Заказ не отправлен'; exit();
}

$str_result = "<div style=\" padding: 0 0 20px 26px; color: rgb(97, 137, 166);\">Заказ №<b>".$responce["order"]["id"]."</b> создан <b>".$responce["order"]["date"]."</b> на сумму <b>".$responce["order"]["totalCost"]."</b> р.</div>";
$str_result .= "<div style=\" padding: 0 0 20px 26px; color: rgb(97, 137, 166);\">Доставка в <b>".$responce["user"]["address"]["place"]."</b> с помощью службы доставки <b>".$responce["deliveryName"]."</b></div>";
$str_result .= "<div style=\" padding: 0 0 20px 26px; color: rgb(97, 137, 166);\">Ориентировочная дата доставки <b>".$responce["order"]["approximateDeliveryDate"]."</b></div>";
$str_result .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";

$str_result .= "<tr>" .
                "<td></td><td class=\"blocktitle\">Статус заказа</td>".
                "<td class=\"blocktitle\">Время</td>".
            	"</tr><tr><td height=\"20\"></td></tr>";

foreach ($responce["deliveryHistory"] as $v1) {
    if ($v1["status"] === "Посылка доставлена" || $v1["status"] === "Получен") {
        $str_result .="<tr><td class=\"status_icon_complete\"><img src=\"/checkout.ru/deliveryhistory/status_icon_complete.gif\" border=\"0\"></td><td width=\"570\" class=\"status_complete\">".$v1["status"]."</td><td class=\"date_complete\">".$v1["date"]."</td></tr>";
    } else {
        $str_result .="<tr><td class=\"status_icon\"><img src=\"/checkout.ru/deliveryhistory/status_icon.gif\" border=\"0\"></td><td width=\"570\" class=\"status\">".$v1["status"]."</td><td class=\"date\">".$v1["date"]."</td></tr>";
    }
}
$str_result .= "</table>";
echo $str_result;