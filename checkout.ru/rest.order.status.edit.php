<?php
    require("config.php");
    
    $jsonBody = @file_get_contents('php://input');
    $data = json_decode($jsonBody);
        
    if ($data->ticket != $checkoutShopId) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Invalid ticket', true, 400);
        print 'Invalid ticket';
        exit();
    } else {
        $newValues = Array(
            "CHECKOUT_ORDER_ID" => $orderId,
            "STATUS" => $data->status,
            "STATUS_DATE" => $data->statusChangeDate,
            "LAST_CHANGES_SOURCE" => "Checkout"
        );
        
        // update order in your CMS using array with values
    }    
 ?>Ok