<?php
    require("config.php");
    
    $jsonBody = @file_get_contents('php://input');
    $data = json_decode($jsonBody);
    
    if ($data->ticket != $checkoutShopId) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Invalid ticket', true, 400);
        print 'Invalid ticket';
        exit();
    } else {
        $newValues = Array(
            "CHECKOUT_ORDER_ID" => $orderId,    
            "ZIP" => $data->order->delivery->postindex,
            "CITY" => $data->order->delivery->place,
            "PLACE_FIAS_ID" => $data->order->delivery->placeFiasId,
            "STREET_FIAS_ID" => $data->order->delivery->streetFiasId,
            "ADDRESS" => $personalAddress,
            "PP_ADDRESS" => $data->order->delivery->address,
            "DELIVERY_TYPE" => $data->order->delivery->type,
            "DELIVERY_SERVICE" => $data->order->delivery->deliveryId,
            "DELIVERY_SERVICE_NAME" => $data->order->delivery->serviceName,
            "DELIVERY_PRICE" => $data->order->delivery->cost,
            "DELIVERY_MIN_TERM" => $data->order->delivery->minTerm,
            "DELIVERY_MAX_TERM" => $data->order->delivery->maxTerm,
            "CUSTOMER_FULL_NAME" => $data->order->user->fullname,
            "EMAIL" => $data->order->user->email,
            "PHONE" => $data->order->user->phone,
            "STATUS" => $data->order->status,
            "STATUS_DATE" => $data->order->statusChangeDate,
            "LAST_CHANGES_SOURCE" => "Checkout"
        );
        
        // update order in your CMS using array with values
    }    
 ?>Ok