<div id="checkout-div">
    <div class="header-title"><span>Получатель</span></div>

    <div class="customer-info-row">
        <div class="customer-info-label">
            ФИО&nbsp;<span class="required">*</span>
        </div>
        <div class="customer-info-input">
            <input id="fullname" name="cFullName" type="text" value="" maxlength="255" autocomplete="off" />
        </div>
    </div>

    <div class="customer-info-row">
        <div class="customer-info-label">
            Мобильный телефон &nbsp;<span class="required">*</span>
        </div>
        <div class="customer-info-input">
            <input id="phone" name="cPhone" type="text" value="" maxlength="25" autocomplete="off" />
        </div>
        <div id="phone-info" class="check-phone-info">Формат: +7XXXXXXXXXX или 8XXXXXXXXXX.</div>
		<div class="customer-info-error" style="margin-top:-2px;">
			<div id="phone-error" class="check-error" style="display: none;">
				- невалидный номер.</div>
			<div id="phone-warning" class="check-phone-warning"
				style="display: none;">Не мобильный номер. Без смс о доставке.</div>
		</div>
    </div>

    <div class="customer-info-row-last">
        <div class="customer-info-label">
            E-mail&nbsp;<span class="required">*</span>
        </div>
        <div class="customer-info-input">
            <input id="email" name="cEmail" type="text" value="" maxlength="75" autocomplete="off" />
        </div>
        <div class="customer-info-error">	
            <div id="email-error" class="check-error" style="display: none;">
                Неверный формат E-mail
            </div>
        </div>
    </div>

    <div class="header-title"><span>Адрес и способ доставки</span></div>

    <div class="container">
        <div class="place-container">
            <div class="place-label">
                Населенный пункт &nbsp;<span class="required">*</span>
            </div>
            <div class="place-input">
                <input id="place" name="cPlace" type="text" value="" autocomplete="off" />
            </div>
            <div class="place-hint">
                <div id="place-choosing-hint" class="" style="display: none;">
                    Для выбора способа доставки введите населённый пункт
                </div>
                <div id="place-error" class="check-error" style="display: none;">
                    Не найден
                </div>
                <div id="place-loader" class="loader" style="display: none;">
                    &nbsp;
                </div>	
            </div>
        </div>
        <div class="postindex-container">
            <div class="postindex-label">
                Почтовый индекс
            </div>
            <div class="postindex-input">
                <input id="postindex" name="cPostIndex" type="text" value="" maxlength="6" autocomplete="off" />
            </div>
            <div class="postindex-hint">	
                <div id="postindex-error" class="check-error" style="display: none;">
                    Несуществующий индекс
                </div>
                <div id="postindex-loader" class="loader" style="display: none;">
                    &nbsp;
                </div>	
            </div>
        </div>
    </div>

    <div id="delivery">
        <div id="deliveryMethods" class="container methods">
            <div class="methods-mail methods-disabled-mail" id="deliveryTypeMail">
                <div class="container">
                    <span id="deliveryTypeMailSpan" class="float-left">Почтой</span>
                    <span class="methods-icon"><img title="Требуется описание" src="<?php print $checkoutServerUrl; ?>/checkout/img/tip.png" class="hint" /></span>
                </div>
                <div class="methods-legend">
                    <span id="deliveryTypeMailLegend"></span>
                </div>
            </div>

            <div class="methods-express methods-disabled-express" id="deliveryTypeExpress">
                <div class="container">
                    <span id="deliveryTypeExpressSpan" class="float-left">Курьером</span>
                    <span class="methods-icon"><img title="Требуется описание" src="<?php print $checkoutServerUrl; ?>/checkout/img/tip.png" class="hint" /></span>
                </div>
                <div class="methods-legend">		
                    <span id="deliveryTypeExpressLegend"></span>
                </div>
            </div>

            <div class="methods-postamat methods-disabled-postamat" id="deliveryTypePostamat">
                <div class="container">
                    <span id="deliveryTypePostamatSpan" class="float-left">В постамат</span>
                    <span class="methods-icon"><img title="Требуется описание" src="<?php print $checkoutServerUrl; ?>/checkout/img/tip.png" class="hint" /></span>
                </div>

                <div class="methods-legend">	
                    <span id="deliveryTypePostamatLegend"></span>
                </div>	
            </div>

            <div class="methods-pvz methods-disabled-pvz" id="deliveryTypePVZ">
                <div class="container">
                    <span id="deliveryTypePVZSpan" class="float-left">В пункт выдачи</span>
                    <span class="methods-icon"><img title="Требуется описание" src="<?php print $checkoutServerUrl; ?>/checkout/img/tip.png" class="hint" /></span>
                </div>
                <div class="methods-legend">	
                    <span id="deliveryTypePVZLegend"></span>
                </div>	
            </div>
        </div>

        <div id="noDelivery" style="display: none;" class="msg-error">
            К сожалению, доставка в ваш населенный пункт невозможна
        </div>

        <div id="deliveryAddress" style="display: none;">
            <div class="commentcontainer">
                <div class="label">
                    Комментарий <img align="bottom" title="Требуется описание" class="hint" src="<?php print $checkoutServerUrl; ?>/checkout/img/tip.png">
                </div>
                <div class="comment">
                    <textarea rows="3" cols="5" id="comment" name="cComment"></textarea>
                </div>
            </div>
            <div class="container">
                <div class="street-check">	
                    <div id="street-error" class="check-warning" style="display: none;">
                        Не найдено
                    </div>
                    <div id="street-loader" class="loader" style="display: none;">
                        &nbsp;
                    </div>	
                </div>
                <div class="label">
                    Улица <span class="required">*</span>
                </div>
                <div class="street">
                    <input id="street" name="cStreet" type="text" value="" maxlength="100" autocomplete="off" />
                </div>
            </div>

            <div class="container">	
                <div class="label">
                    Дом <span class="required">*</span>
                </div>
                <div class="house">
                    <input id="house" name="cHouse" type="text" value="" maxlength="10" autocomplete="off" />
                </div>
                <div class="house-check">	
                    <div id="house-error" class="check-error" style="display: none;">
                        Неверный формат
                    </div>
                </div>
            </div>

            <div class="container">	
                <div class="label">
                    Корпус
                </div>
                <div class="housing">
                    <input id="housing" name="cHousing" type="text" value="" maxlength="10" autocomplete="off" />
                </div>
                <div class="housing-check">	
                    <div id="housing-error" class="check-error" style="display: none;">
                        Неверный формат
                    </div>
                </div>
            </div>

            <div class="container">	
                <div class="label">
                    Строение
                </div>
                <div class="building">
                    <input id="building" name="cBuilding" type="text" value="" maxlength="10" autocomplete="off" />
                </div>
                <div class="building-check">	
                    <div id="building-error" class="check-error" style="display: none;">
                        Неверный формат
                    </div>
                </div>
            </div>

            <div class="container">	
                <div class="label">
                    Квартира/офис
                </div>
                <div class="appartment">
                    <input id="appartment" name="cAppartment" type="text" value="" maxlength="10" autocomplete="off" />
                </div>
                <div class="appartment-check">	
                    <div id="appartment-error" class="check-error" style="display: none;">
                        Неверный формат
                    </div>
                </div>
            </div>
        </div>

        <div id="deliveryPVZAddress" class="container" style="display: none;">
            <div class="chooseinmap"><a href="#" onclick="showPvzMap();">Выбрать на карте</a></div>
            <div class="label">
                Расположение пункта выдачи <span class="required">*</span>
            </div>
            <div class="terminal">
                <select id="pvzAddress"></select>
            </div>
        </div>

        <div id="deliveryPostamatAddress" class="container" style="display: none;">
            <div class="chooseinmap"><a href="#" onclick="showPostamatMap();">Выбрать на карте</a></div>
            <div class="label">
                Расположение постамата <span class="required">*</span>
            </div>
            <div class="terminal">
                <select id="postamatAddress"></select>
            </div>
        </div>
		
		<div class="additionalInfoCnt" id="additionalInfo"
			style="display: none;">
			<div class="additionalInfoImg">
				<img src="<?php print $checkoutServerUrl; ?>/checkout/img/tip.png">
			</div>
			<div class="additionalInfo" id="additionalDeliveryInfo"></div>
		</div>
    </div>
</div>

<!-- hidden data in printable format -->
<div id="deliveryTotals">
    <div style="display: none;" id="deliveryCostShow"></div>
    <div style="display: none;" id="deliveryTermShow"></div>
    <div style="display: none;" id="deliveryNameShow"></div>
    <div style="display: none;" id="totalCostShow"></div>
</div>

<!-- pvz/postamat map -->
<div id="mapPage">
    <div class="shadow"></div>
    <div class="mapcontent">
        <span class="closemap" onclick="closeTerminalMap();"></span>
        <div class="main">
            <div class="main-left">
                <div class="map-header" id="mapLeftHeader"></div>
                <div class="map-body" id="mapBody"></div>
            </div>
            <div class="main-right">
                <div class="order-header" id="mapRightHeader"></div>

                <div class="terminals" id="mapTerminalList"></div>

                <div class="totals2" id="mapTerminalInfo">&nbsp;</div>

                <div class="container finish">
                    <input type="image" onclick="return false;" class="float-right closemap-btn" src="<?php print $checkoutServerUrl; ?>/checkout/img/order-btn-closemap.png" width="154" height="42" id="mapCloseMapBtn" alt="Закрыть карту" />
                    <input type="image" onclick="return false;" class="float-right select-btn" src="<?php print $checkoutServerUrl; ?>/checkout/img/order-btn-select.png" width="119" height="42" id="mapSelectBtn" disabled="disabled" alt="Выбрать" />					
                </div>
            </div>
        </div>
    </div>
</div>    

<input type="hidden" id="ticket" name="ticket" value="<?php print CheckoutUtils::getTicket(); ?>" />

<input type="hidden" id="sum" value="" />
<input type="hidden" id="assessedSum" value="" />
<input type="hidden" id="totalWeight" value="" />
<input type="hidden" id="placeId" name="cPlaceId" value="" />
<input type="hidden" id="streetId" name="cStreetId" value="" />
<input type="hidden" id="streetType" name="cStreetType" value="" />
<input type="hidden" id="country" name="cCountry" value="" />
<input type="hidden" id="address" name="cAddress" value="" />
<input type="hidden" id="deliveryId" name="cDeliveryId" value="0" />
<input type="hidden" id="deliveryType" name="cDeliveryType" value="" />
<input type="hidden" id="deliveryCost" name="cDeliveryCost" value="0" />
<input type="hidden" id="deliveryMinTerm" name="cDeliveryMinTerm" value="0" />
<input type="hidden" id="deliveryMaxTerm" name="cDeliveryMaxTerm" value="0" />

<input type="hidden" id="basketPlaceId" value="" />
<input type="hidden" id="basketPlace" value="" />
<input type="hidden" id="basketStreetId" value="" />
<input type="hidden" id="basketStreet" value="" />
<input type="hidden" id="basketStreetType" value="" />
<input type="hidden" id="basketHouse" value="" />
<input type="hidden" id="basketHousing" value="" />
<input type="hidden" id="basketBuilding" value="" />
<input type="hidden" id="basketAppartment" value="" />
<input type="hidden" id="basketPostindex" value="" />