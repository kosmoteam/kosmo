<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/checkout.ru/httpful.phar");

    class CheckoutUtils {
        public static function getTicket() {
            global $checkoutShopId, $checkoutServerUrl;
            $tuCurl = curl_init();

            curl_setopt($tuCurl, CURLOPT_URL, "$checkoutServerUrl/service/login/ticket/$checkoutShopId");
            curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
            curl_setopt($tuCurl, CURLOPT_HEADER, 0);
            curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);

            $tuData = curl_exec($tuCurl);

            if (!curl_errno($tuCurl)) {
                $info = curl_getinfo($tuCurl);
            } else {
                echo 'Curl error: ' . curl_error($tuCurl);
            }

            curl_close($tuCurl);
            $response =  json_decode($tuData, true);

            $ticket = $response["ticket"];

            return $ticket;
        }

        public static function createOrder($data) {
            global $checkoutShopId, $checkoutServerUrl;
            
            $response = \Httpful\Request::post($checkoutServerUrl . "/service/order/create")
                ->sendsJson()                               
                ->body(json_encode($data))
                ->sendIt();
            
            if ($response->hasErrors()) {
                $headers = explode("\r\n", $response->raw_headers);
                $header = explode(" ", $headers[0]);
                $response = str_replace($header[0], "", $headers[0]);
            } else
                $response = $response->body;
            
            return $response;
        }
        
        public static function modifyOrder($checkoutOrderId, $data) {

        }

        public static function buildFullAddress($street, $house, $housing, $building, $appartment) {
            $addr = "";

            if ($street != "" && $house != "") {
                $addr = $street . ", д." . $house;

                if ($housing != "")
                    $addr .= " корп." . $housing;
                if ($building != "")
                    $addr .= " стр." . $building;
                if ($appartment != "")
                    $addr .= " кв." . $appartment;
            }

            return $addr;
        }

        public static function parseFullAddress($address) {
            $data = array();

            mb_internal_encoding("UTF-8");

            if ($address != "") {
                $addrParts = explode(", ", $address);
                $data["street"] = $addrParts[0];

                if (mb_eregi("д\.([а-яА-ЯёЁ0-9\-]+)(\s|$)", $addrParts[1], $matches))
                    $data["house"] = $matches[1];

                if (mb_eregi("корп\.([а-яА-ЯёЁ0-9\-]+)(\s|$)", $addrParts[1], $matches))
                    $data["housing"] = $matches[1];
                else
                    $data["housing"] = "";

                if (mb_eregi("стр\.([а-яА-ЯёЁ0-9\-]+)(\s|$)", $addrParts[1], $matches))
                    $data["building"] = $matches[1];
                else
                    $data["building"] = "";

                if (mb_eregi("кв\.([а-яА-ЯёЁ0-9\-]+)(\s|$)", $addrParts[1], $matches))
                    $data["appartment"] = $matches[1];
                else
                    $data["appartment"] = "";
            }

            return $data;
        }
    }
?>