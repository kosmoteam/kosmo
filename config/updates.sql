CREATE TABLE IF NOT EXISTS `item_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(256) NOT NULL,
  `item_type` varchar(128) NOT NULL,
  `item_id` varchar(256) NOT NULL,
  `rate` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `item_rating`
--

INSERT INTO `item_rating` (`id`, `user`, `item_type`, `item_id`, `rate`) VALUES
(1, 'user', 'liquid', 'white_tiger', 3),
(2, 'user', 'liquid', 'white_tiger', 4),
(3, 'user', 'liquid', 'white_tiger', 4),
(4, 'user', 'liquid', 'white_tiger', 4),
(5, 'user', 'liquid', 'white_tiger', 4),
(6, 'user', 'liquid', 'white_tiger', 4),
(7, 'user', 'liquid', 'white_tiger', 4),
(8, 'user', 'liquid', 'white_tiger', 4),
(9, 'user', 'liquid', 'white_tiger', 4),
(10, 'user', 'liquid', 'white_tiger', 4),
(11, 'user', 'liquid', 'white_tiger', 3),
(12, 'user', 'liquid', 'white_tiger', 4),
(13, 'user', 'liquid', 'white_tiger', 3),
(14, 'user', 'liquid', 'white_tiger', 3),
(15, 'user', 'liquid', 'white_tiger', 3),
(16, 'user', 'liquid', 'white_tiger', 4),
(17, 'user', 'liquid', 'white_tiger', 4),
(18, 'user', 'liquid', 'white_tiger', 4),
(19, 'user', 'liquid', 'white_tiger', 4),
(20, 'user', 'liquid', 'white_tiger', 4);


--
-- Структура таблицы `site_promo`
--
CREATE TABLE IF NOT EXISTS `site_promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

--
-- Дамп данных таблицы `site_promo`
--
INSERT INTO `site_promo` (`id`, `name`, `text`) VALUES
  (23, 'cat_head_banner_img', 'banner2.png'),
  (24, 'cat_head_banner_url', 'http://waka.waka1'),
  (25, 'cat_head_banner_enabled', '0'),
  (26, 'subscriptions_enabled', '0');

--
-- Структура таблицы `subscriptions`
--
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`(255)),
  KEY `email_2` (`email`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;


INSERT INTO `site_texts` (`name`, `text`) VALUES
  ('catalog_nonicoliquid_meta_title', ''),
  ('catalog_nonicoliquid_meta_keywords', ''),
  ('catalog_nonicoliquid_meta_description', ''),
  ('catalog_nonicoliquid_text', '');