--
-- База данных: `kosmo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(32) NOT NULL,
  `url_name` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `producer` varchar(256) NOT NULL,
  `category` enum('liquid','device') NOT NULL DEFAULT 'liquid',
  `item_id` int(100) NOT NULL,
  `nicotine` int(1) NOT NULL,
  `volume` int(2) NOT NULL,
  `quantity` int(2) NOT NULL,
  `price` int(5) NOT NULL,
  `color` varchar(256) NOT NULL,
  `weight` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `url_name` varchar(256) NOT NULL,
  `parent` enum('liquid','device','producer') NOT NULL DEFAULT 'liquid',
  `name` varchar(256) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `catalog_text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `url_name`, `parent`, `name`, `meta_title`, `meta_keywords`, `meta_description`, `catalog_text`) VALUES
  (1, 'oakshire_trade', 'producer', 'Oakshire Trade Co.', '', '', 'sdfsdffsdfddf', 'dsfsdffff'),
  (2, 'cafe_racer', 'producer', 'Cafe Racer Vape', '', '', '', ''),
  (3, 'crft_labs', 'producer', 'CRFT Labs Inc.', '', '', '', ''),
  (4, 'lovela', 'liquid', 'Lovela', '', '', '', ''),
  (5, 'wismec', 'producer', 'Wismec', '', '', '', ''),
  (6, 'joyetech', 'producer', 'Joyetech', '', '', '', ''),
  (7, 'smoktech', 'producer', 'SmokTech', '', '', '', ''),
  (8, 'clon', 'producer', 'Clon', 'clon title', 'clon keywords', 'clon desc', ''),
  (9, 'aeronaut', 'producer', 'Aeronaut', '', '', '', ''),
  (10, 'classified', 'liquid', 'Classified', '', '', '', ''),
  (11, 'traditional_juice', 'liquid', 'Traditional Juice Co.', '', '', '', ''),
  (12, 'teardrip', 'liquid', 'Teardrip', '', '', '', ''),
  (13, 'cafe_racer', 'liquid', 'Cafe Racer Vape', '', '', '', ''),
  (14, 'blck', 'liquid', 'BLCK', '', '', '', ''),
  (15, 'aesop', 'liquid', 'AESOP', '', '', '', ''),
  (16, 'crft', 'liquid', 'CRFT', '', '', '', ''),
  (17, 'popbars', 'liquid', 'POPBARS', '', '', '', ''),
  (18, 'cwrl', 'liquid', 'CWRL', '', '', '', ''),
  (19, 'whip_it', 'liquid', 'WHIP IT', '', '', '', ''),
  (20, 'lovela', 'liquid', 'Lovela', '', '', '', ''),
  (21, 'mod', 'device', 'Мод', '', '', '', ''),
  (22, 'tank', 'device', 'Бак', '', '', '', ''),
  (23, 'drip', 'device', 'Дрипка', '', '', '', ''),
  (24, 'uncle_junks_genius', 'liquid', 'Uncle Junk''s Genius Juice', '', '', '', ''),
  (25, 'uncle_junks', 'producer', 'Uncle Junk&#039;s Genius Juice', 'ntu', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `url_name` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `img_alt` varchar(256) NOT NULL,
  `category` varchar(256) NOT NULL,
  `producer` varchar(256) NOT NULL,
  `volume` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `color` varchar(256) NOT NULL DEFAULT 'black,white,gold',
  `price` int(5) NOT NULL,
  `quantity` int(100) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `weight` int(5) NOT NULL DEFAULT '300',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `devices`
--

INSERT INTO `devices` (`id`, `url_name`, `name`, `img_alt`, `category`, `producer`, `volume`, `description`, `remark`, `color`, `price`, `quantity`, `meta_title`, `meta_keywords`, `meta_description`, `weight`) VALUES
  (1, 'reuleaux_rx200', 'Reuleaux RX200', 'Reuleaux RX200', 'mod', 'wismec', '200', '', '', 'white,black', 5000, 10, 'Reuleaux RX200', 'Reuleaux RX200', 'Reuleaux RX200', 300),
  (3, 'evic_vtc_mini_full_kit', 'eVic VTC Mini Full Kit With TRON-S', 'eVic VTC Mini Full Kit With TRON-S', 'mod', 'joyetech', '75', '', '', 'grey,black,cyan,gold,red,white', 5500, 10, '', '', '', 300),
  (4, 'evic_vtc_mini_box_mod', 'eVic VTC Mini Box Mod Black', 'eVic VTC Mini Box Mod Black', 'mod', 'joyetech', '75', '', '', 'white,black,cyan', 4500, 10, '', '', '', 300),
  (5, 'cuboid', 'Cuboid', 'Cuboid', 'mod', 'joyetech', '200', '', '', 'black,grey', 4900, 10, '', '', '', 300),
  (6, 'tfv4_full_kit', 'TFV4 Full Kit', 'TFV4 Full Kit', 'tank', 'smoktech', '', '', '', 'silver,black', 3000, 10, '', '', '', 300),
  (7, 'velocity_rda', 'Velocity RDA', 'Velocity RDA', 'drip', 'clon', '', '', '', 'silver,black,cyan', 1200, 10, '', '', '', 300),
  (8, 'kennedy_v2', 'KENNEDY V2 Silver', 'KENNEDY V2 Silver', 'drip', 'clon', '', '', '', 'silver', 1700, 10, '', '', '', 300),
  (9, 'royal_hunter_rda', 'Royal Hunter RDA', 'Royal Hunter RDA', 'drip', 'clon', '', '', '', '', 1200, 10, '', '', '', 300),
  (10, 'aeronaut_gunmetal', 'Aeronaut Gunmetal', 'Aeronaut Gunmetal', 'drip', 'aeronaut', '', '', '', '', 4800, 10, 'Aeronaut RDA Gunmetal фото, цена, обзор', '', '', 300),
  (11, 'aeronaut_ss', 'Aeronaut SS', 'Aeronaut SS', 'drip', 'aeronaut', '', 'Дрипка Aeronaut SS', '', '', 4500, 10, '', '', '', 300),
  (12, 'aeronaut_afc_kit', 'Aeronaut AFC Kit', 'Aeronaut AFC Kit', 'drip', 'aeronaut', '', '', '', '', 1300, 10, '', '', '', 300);

-- --------------------------------------------------------

--
-- Структура таблицы `liquids`
--

CREATE TABLE IF NOT EXISTS `liquids` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `url_name` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `img_alt` varchar(256) NOT NULL,
  `producer` varchar(256) NOT NULL,
  `category` varchar(256) NOT NULL,
  `volume1` int(2) NOT NULL DEFAULT '15',
  `volume2` int(2) DEFAULT NULL,
  `price1` int(5) NOT NULL,
  `price2` int(5) DEFAULT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tags` varchar(256) NOT NULL,
  `nicotine` varchar(32) NOT NULL DEFAULT '3',
  `quantity` int(100) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `weight` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `liquids`
--

INSERT INTO `liquids` (`id`, `url_name`, `name`, `img_alt`, `producer`, `category`, `volume1`, `volume2`, `price1`, `price2`, `remark`, `description`, `tags`, `nicotine`, `quantity`, `meta_title`, `meta_keywords`, `meta_description`, `weight`) VALUES
  (1, 'crisp', 'Crisp', 'Crisp', 'oakshire_trade', 'classified', 30, 0, 1500, 0, 'Премиальная жидкость для электронных сигарет Crisp', '', '', '3', 100, '', '', '', 100),
  (2, 'delight', 'Delight', 'Delight', 'oakshire_trade', 'classified', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Delight', '', '', '3', 10, '', '', '', 100),
  (3, 'fusion', 'Fusion', 'Fusion', 'oakshire_trade', 'classified', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Fusion', '', '', '3', 10, '', '', '', 100),
  (4, 'indulge', 'Indulge', 'Indulge', 'oakshire_trade', 'classified', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Indulge', '', '', '3', 10, '', '', '', 100),
  (5, 'keen', 'Keen', 'Keen', 'classified', 'classified', 30, 0, 1500, 0, 'Премиальная жидкость для электронных сигарет Keen', '', 'keen, liquid', '3,6', 10, '', '', '', 100),
  (6, 'plush', 'Plush', 'Plush', 'oakshire_trade', 'classified', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Plush', '', '', '3', 10, '', '', '', 100),
  (7, 'lola', 'Lola', 'Lola', 'oakshire_trade', 'traditional_juice', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Lola', '', '', '0,3', 10, '', '', '', 100),
  (8, 'black_and_blue', 'Black and Blue', 'Black and Blue', 'oakshire_trade', 'traditional_juice', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Black and Blue', '', '', '0,3', 10, '', '', '', 100),
  (9, 'indian_giver', 'Indian Giver', 'Indian Giver', 'oakshire_trade', 'traditional_juice', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Indian Giver', '', '', '0,3', 10, '', '', '', 100),
  (10, 'white_tiger', 'White Tiger', 'White Tiger', 'oakshire_trade', 'traditional_juice', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет White Tiger', '', '', '0,3', 10, '', '', '', 100),
  (11, 'cinapps', 'Cinapps', 'Cinapps', 'oakshire_trade', 'teardrip', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Cinapps', '', '', '0,3', 10, '', '', '', 100),
  (12, 'pearamel', 'Pearamel', 'Pearamel', 'oakshire_trade', 'teardrip', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Pearamel', '', '', '0,3', 10, '', '', '', 100),
  (13, 'revenge_of_the_geeks', 'Revenge of the Geeks', 'Revenge of the Geeks', 'oakshire_trade', 'teardrip', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Revenge of the Geeks', '', '', '0,3', 10, '', '', '', 100),
  (14, 'lucky_13', 'Lucky 13', 'Lucky 13', 'cafe_racer', 'cafe_racer', 30, NULL, 1550, NULL, 'Премиальная жидкость для электронных сигарет Lucky 13', '', '', '0,3', 10, '', '', '', 100),
  (15, 'yogurt_bomb', 'Yogurt Bomb', 'Yogurt Bomb', 'cafe_racer', 'cafe_racer', 30, NULL, 1550, NULL, 'Премиальная жидкость для электронных сигарет Yogurt Bomb', '', '', '0,3', 10, '', '', '', 100),
  (16, 'peach_guzzi', 'Peach Guzzi', 'Peach Guzzi', 'cafe_racer', 'cafe_racer', 30, NULL, 1550, NULL, 'Премиальная жидкость для электронных сигарет Peach Guzzi', '', '', '0,3', 10, '', '', '', 100),
  (17, 'citron', 'Citron', 'Citron', 'crft_labs', 'blck', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Citron', '', '', '0,3', 10, '', '', '', 100),
  (18, 'rosso', 'Rosso', 'Rosso', 'crft_labs', 'blck', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Rosso', '', '', '0,3', 10, '', '', '', 100),
  (19, 's_palmer', 'S.Palmer', 'S.Palmer', 'crft_labs', 'aesop', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет S.Palmer', '', '', '0,3', 10, '', '', '', 100),
  (20, 'gravel_pit', 'Gravel Pit', 'Gravel Pit', 'crft_labs', 'crft', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Gravel Pit', '', '', '0,3', 10, '', '', '', 100),
  (21, 'lime_cola', 'Lime Cola', 'Lime Cola', 'crft_labs', 'crft', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Lime Col', '', '', '0,3,6', 10, '', '', '', 100),
  (22, 'strawberry_blonde', 'Strawberry Blonde', 'Strawberry Blonde', 'crft_labs', 'crft', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет ', '', '', '0,3', 10, '', '', '', 100),
  (23, 'trail_mix', 'Trail Mix', 'Trail Mix', 'crft_labs', 'crft', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Strawberry Blonde', '', '', '0,3', 10, '', '', '', 100),
  (24, 'cocodew', 'Cocodew', 'Cocodew', 'crft_labs', 'popbars', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Cocodew', '', '', '0,3', 10, '', '', '', 100),
  (25, 'strawberry_cinnamon_roll', 'Strawberry Cinnamon Roll', 'Strawberry Cinnamon Roll', 'crft_labs', 'cwrl', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Strawberry Cinnamon Roll', '', '', '0,3', 10, '', '', '', 100),
  (26, 'frozen_yogurt_pinapple', 'Frozen Yogurt Pinapple', 'Frozen Yogurt Pinapple', 'crft_labs', 'whip_it', 30, NULL, 1500, NULL, 'Премиальная жидкость для электронных сигарет Frozen Yogurt Pinapple', '', '', '0,3', 10, '', '', '', 100),
  (27, 'the_battenberg', 'The Battenberg', 'The Battenberg', 'lovela', 'lovela', 30, NULL, 1800, NULL, 'Премиальная жидкость для электронных сигарет The Battenberg', '', '', '0,3,6', 10, '', '', '', 100),
  (28, 'the_bees_knees', 'The Bee`s Knees', 'The Bee`s Knees', 'lovela', 'lovela', 30, NULL, 1800, NULL, 'Премиальная жидкость для электронных сигарет The Bee`s Knees', '', '', '0,3,6', 10, '', '', '', 100),
  (29, 'the_julep', 'The Julep', 'The Julep', 'lovela', 'lovela', 30, NULL, 1800, NULL, 'Премиальная жидкость для электронных сигарет The Julep', '', '', '0,3,6', 10, '', '', '', 100),
  (30, 'jon_wayne', 'Jon Wayne', 'Jon Wayne', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет Jon Wayne', '', '', '0,3,6', 10, '', '', '', 65),
  (31, 'junkyard_scotch', 'Junkyard Scotch', 'Junkyard Scotch', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет Junkyard Scotch', '', '', '3,6', 10, '', '', '', 65),
  (32, 'pink_meadow', 'Pink Meadow', 'Pink Meadow', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет Pink Meadow', '', '', '3,6', 10, '', '', '', 65),
  (33, 'badd_nana', 'Badd Nana', 'Badd Nana', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет Badd Nana', '', '', '3', 10, '', '', '', 65),
  (34, 'key_stone', 'Key Stone', 'Key Stone', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет Key Stone', '', '', '3', 10, '', '', '', 65),
  (35, 'new_dawn', 'A New Dawn', 'A New Dawn', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет A New Dawn', '', '', '3', 10, '', '', '', 65),
  (36, 'savannah', 'Savannah', 'Savannah', 'uncle_junks', 'uncle_junks_genius', 15, NULL, 850, NULL, 'Премиальная жидкость для электронных сигарет Savannah', '', '', '3', 10, '', '', '', 65);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(32) NOT NULL,
  `cartid` varchar(32) NOT NULL,
  `date` datetime NOT NULL,
  `fullname` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `comment` text NOT NULL,
  `textform` text NOT NULL,
  `delivery` varchar(256) NOT NULL DEFAULT 'courier',
  `state` enum('created','paid','rejected') NOT NULL DEFAULT 'created',
  `ch_order` varchar(128) NOT NULL,
  `ch_answer` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `site_texts`
--

CREATE TABLE IF NOT EXISTS `site_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `site_texts`
--

INSERT INTO `site_texts` (`id`, `name`, `text`) VALUES
  (1, 'meta_title', 'Kosmos Vape: премиальные элитные жидкости со всего мира'),
  (2, 'meta_keywords', 'vapes, personal vaporizer, mechanical mods, bling vapes, custom vape wraps, vape kits'),
  (3, 'meta_description', 'Продажа премиальных, элитных и импортных жидкостей для электронных испарителей по доступным ценам в Москве с доставкой.'),
  (4, 'catalog_liquid_text', '&lt;h1&gt;Онлайн магазин премиальных элитных жидкостей&lt;/h1&gt;&lt;p&gt;У нас много жидкостей на складе Москве.&lt;/p&gt;'),
  (5, 'catalog_all_text', '&lt;h1&gt;Онлайн магазин премиальных элитных жидкостей из США и Европы&lt;/h1&gt;&lt;p&gt;Добро пожаловать в интернет-магазин премиальных импортных жидкостей для электронных испарителей (сигарет) Kosmos Vape. Мы отобрали для вас самые элитные вкусы из США и Европы. Все жижи, что вы видите в каталоге в наличии на складе Москве.&lt;/p&gt;'),
  (6, 'catalog_device_text', '&lt;h1&gt;Онлайн магазин премиальных элитных девайсов&lt;/h1&gt;&lt;p&gt;У нас много девайсов на складе Москве.&lt;/p&gt;'),
  (7, 'catalog_all_meta_title', 'Каталог добра'),
  (8, 'catalog_all_meta_keywords', 'каталог, слова'),
  (9, 'catalog_all_meta_description', 'страница каталога'),
  (10, 'catalog_device_meta_title', 'каталог девайсов'),
  (11, 'catalog_device_meta_keywords', 'девайсы, покупай'),
  (12, 'catalog_device_meta_description', 'страница каталога девайсов'),
  (13, 'catalog_liquid_meta_title', 'Каталог жидкости'),
  (14, 'catalog_liquid_meta_keywords', 'жидости, покупай,быстро'),
  (15, 'catalog_liquid_meta_description', 'страница каталога жидкостей'),
  (16, 'catalog_producer_text', ''),
  (17, 'catalog_producer_meta_title', ''),
  (18, 'catalog_producer_meta_description', ''),
  (19, 'catalog_producer_meta_keywords', ''),
  (20, 'og_image', 'http://kosmosvape.ru/img/social/logosmm.png'),
  (21, 'og_title', 'Kosmos Vape - премиальные элитные жидкости со всего мира'),
  (22, 'og_description', 'Продажа премиальных, элитных и импортных жидкостей для электронных испарителей по доступным ценам в Москве с доставкой.');
